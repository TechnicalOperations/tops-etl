# import os
activate_this = '/home/cbeecher/venv/tops-etl/bin/activate_this.py'  # os.environ['TOPS_ETL_VENV']
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))

import sys
sys.stdout = sys.stderr
sys.path.insert(0, '/var/www/html/tops-etl/')

from etl.http_nodes.run_server import complete_app as application

