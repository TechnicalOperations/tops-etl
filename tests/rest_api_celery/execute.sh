celery -A etl.job_processor worker --loglevel=info &
export celery_pid=$!
python3 -m etl.http_nodes.run_server &
export flask_pid=$!
sleep 3s

# curl -H "Content-Type: application/json" -X POST -d '{"job_name": "nodsp_filters", "parameters": {"pub_ids": ["61303"], "site_ids": ["63453"], "notifier": {"type": "email", "to": "cbeecher@rhythmone.com", "cc": []}}}' http://127.0.0.1:5000/submit/
curl -X POST http://127.0.0.1:5000/reports/all/
kill $flask_pid
unset flask_pid
sleep 10s

kill $celery_pid
unset celery_pid