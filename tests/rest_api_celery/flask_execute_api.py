import flask
from etl.job_processor import run_job
from jobs.main import get_all_scripts_names


all_scripts = get_all_scripts_names()
app = flask.Flask('celery_flask_test')


@app.route('/', methods=['POST'])
def make_ticket():
    data = flask.request.get_json()
    # The line below is how to set the job to run while simultaneously returning a response back to the client
    validator = all_scripts[data['job_name']].param_validator(**data['parameters'])
    if not validator[0]:
        return flask.jsonify({'failure': 'Invalid parameters.  Required parameters are {0}'.format(validator[1])})
    run_job.delay(data['job_name'], **data['parameters'])
    return flask.jsonify({'success': 'Successfully submitted request for job {0}'.format(data['job_name'])})


if __name__ == '__main__':
    app.run('127.0.0.1', 9898)
