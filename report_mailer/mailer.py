import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.base import MIMEBase
from email import encoders
from . import parse
import numpy as np
import locale
import json


class Mailer:
    """
    SMTP email helper class
    """

    def __init__(self, authentication, template, default='HTML can not be rendered', subs={}):
        """
        Mailer instantation
        :param authentication: File to email credentials.  File must be a JSON with format {"email":"","username":"","password":"","con":{"host":"","port":<int>}}
        :param template: HTML template for the email
        :param default: Default message to be shown if HTML is not supported by email client
        :param subs: Additional substitutions to be made in template.  Used for dynamically creating a template to extend the base template (ex. iterating over a list of unknown size)
        """
        self.parse = parse.TemplateParse()
        with open(authentication, 'r') as f:
            self.auth = json.loads(f.read())
        self.default = default
        self.template = template
        self.graphs = {}
        self.colors = []
        self.files = []
        self.formatter = {}
        self.cids = None
        self.subs = subs

    def set_graph_colors(self, colors):
        """
        Set list of color options as ['#<6 digit hex value>', ...]
        :param colors:
        :return:
        """
        self.colors = colors

    def set_formatter(self, formatter={}):
        """
        Similar to sub except these are all one off values (example: date)
        :param formatter: Dictionary of items formatted as {<template_key>: <value>}
        :return:
        """
        self.formatter = formatter

    def make_html(self):
        """
        Makes the final substitutions in the template to create the final HTML
        :return:
        """
        self.template = self.parse.replace_tags(self.template).format(**self.formatter)

    def make_graph(self, cid, data, *args, **kwargs):
        """
        Parses out the cid from the template and creates the appropriate graph type
        :param cid: cid to process from the template
        :param data: pandas data frame to plot (First column will be the x axis)
        :param args: Additional arguments which are just passed off to the parse graph method
        :param kwargs: Additional optional keyword arguments including title and colors.  Remaining arguments are just passed off to the parse graph method
        :return: List of tuples with where the tuple has structure (value, color used,)
        """
        if self.cids is None:
            with open(self.template, 'r') as f:
                self.template = f.read()
            for k, v in self.subs.items():
                self.template = self.template.replace('{'+k.replace(' ', '')+'}', v)
            self.cids = self.parse.get_cids(self.template)
        meta = self.cids.get(cid, None)
        if meta is None:
            raise Exception("CID '{0}' does not exist in template".format(cid))
        method = self.parse.methods[meta['kind']]
        meta['title'] = kwargs.pop('title') if 'title' in kwargs else meta['title']
        colors = kwargs.pop('colors') if 'colors' in kwargs else None
        colors = colors if colors is not None else self.colors if len(self.colors) > 0 else parse.TemplateParse.DEFAULT_COLORS
        if 'multi' in meta['kind']:
            self.graphs[cid] = method(
                data,
                args[0],
                meta['title'] if len(args) < 2 else args[1],
                colors=colors,
                **kwargs
            )
        else:
            self.graphs[cid] = method(
                data,
                None,
                meta['title'],
                colors=colors,
                **kwargs
            )
        return zip(list(data[list(data.columns)[0]]), colors)

    def attach_file(self, file, *file_name):
        """
        Attach a file to the email.  Only reads in binary data
        :param file: String to file path
        :return: Returns attachment payload
        """
        if type(file) == str:
            with open(file, 'rb') as f:
                data = f.read()
        else:
            file.seek(0)
            data = file.read()
            file = file_name[0]
        attch = MIMEBase('application', 'octet-stream')
        attch.set_payload(data)
        encoders.encode_base64(attch)
        attch.add_header('Content-Disposition', 'attachment; filename="{0}"'.format(file))
        self.files.append(attch)
        return attch

    def send_mail(self, subject, to, cc=[], bcc=[], **kwargs):
        """
        Send the email
        :param subject: Email subject
        :param to: Primary receiver
        :param cc: Any CC
        :param bcc: Any BCC
        :param kwargs: This does nothing, honestly
        :return:
        """
        if self.cids is None:
            with open(self.template, 'r') as f:
                self.template = f.read()
        self.make_html()
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = self.auth['email']
        msg['To'] = to
        msg['CC'] = ', '.join(cc)

        msg.attach(MIMEText(self.default, 'plain'))
        msg.attach(MIMEText(self.template, 'html'))

        for cid, graph in self.graphs.items():
            img = MIMEImage(graph, 'png')
            img.add_header('Content-Disposition', 'attachement; filename={0}.png'.format(cid))
            img.add_header('Content-ID', '<{0}>'.format(cid))
            msg.attach(img)

        for file in self.files:
            msg.attach(file)

        s = smtplib.SMTP()
        s.connect(**self.auth['con'])
        s.ehlo()
        s.starttls()
        s.ehlo()
        s.login(self.auth['email'], self.auth['password'])
        s.sendmail(self.auth['email'],
                   cc+bcc+[to],
                   msg.as_string())

    def format_float(self, x, round=0):
        try:
            locale.setlocale(locale.LC_NUMERIC, 'en_US')
        except locale.Error:
            locale.setlocale(locale.LC_NUMERIC, 'EN-US')
        x = np.round(x, round)
        output = locale.format("%d", x, grouping=True)
        locale.setlocale(locale.LC_NUMERIC, None)
        return output
