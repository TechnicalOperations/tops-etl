import os
import re
import locale
import matplotlib
matplotlib.use('Agg')
import pandas
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.ticker import FuncFormatter
from io import BytesIO


def contract(x, pos):
    """
    Contract numeric value to have a suffix
    :param x: The float value to be contracted
    :param pos: No clue.  The second argument is necessary to satisfy the matploblib formatter, but I don't use it
    :return: Returns a string that is the formatted float
    """
    inv = 0
    while x * (10 ** inv) > 1:
        inv -= 1
    inv += 1
    if inv <= -6:
        return '%0.0fM' % (x / 1000000)
    elif inv <= -3:
        return '%0.0fK' % (x / 1000)
    else:
        return '%0.0f' % x


class TemplateParse:

    # The permitted chart types
    CHART_TYPES = {
        'pie': None,
        'line': None,
        'bar': None
    }

    DEFAULT_COLORS = ['#48d1f0', '#e780ac', '#8be8f8', '#a780c7', '#80d5c2', '#fcee80', '#bebdb7']

    def __init__(self):
        """
        Instantiates the parser and associates the chart types to their appropriate methods
        """
        self.cids_reg = re.compile('\{\{.+?\}\}')
        self.methods = {
            'pie': self.make_pie,
            'bar': self.make_bar,
            'line': self.make_line,
            'multiline': self.multi_axes_line
        }

    def replace_tags(self, template):
        """
        Finds the CID in templates and replaces it with the actual path that will be used in the email HTML
        :param template: The email template
        :return: The formatted email template
        """
        cids = self.get_cids(template)
        for cid, meta in cids.items():
            template = template.replace(meta['tag'], 'cid:{0}'.format(cid))
        return template

    def get_cids(self, template):
        """
        Finds the CIDs in the template and classifies them
        :param template: The email template
        :return: Returns the CID tags
        """
        tags = self.cids_reg.findall(template)
        tags = [(i, i.replace('{', '').replace('}', '').strip().split(':'),) for i in tags]
        tags = {
            i[1][1]: {
                'kind': i[1][0],
                'title': i[1][1],
                'legend': i[1][2] if len(i[1]) > 2 else '',
                'tag': i[0]
            }
            for i in tags
            if len(i) > 1
        }
        return tags

    def make_image(self):
        """
        Converts the matploblib plt object into a byte array for email ingestion
        :return: The image byte array
        """
        buffer = BytesIO()
        plt.savefig(buffer, format='png')
        buffer.seek(0)
        data = buffer.read()
        plt.close()
        return data

    def make_pie(
            self,
            data,
            legend,
            title,
            colors=DEFAULT_COLORS,
            autopct=lambda x: '{p:.2f}%'.format(p=x)
    ):
        """
        Method for making a pie chart
        :param data: pandas dataframe to plot
        :param legend: The legend to be used if not None
        :param title: Graph title
        :param colors: List of color options
        :param autopct: Number format.  Default: lambda x: '{p:.2f}%'.format(p=x)
        :return: Byte array of the image created
        """
        fig, ax = plt.subplots()
        ax.pie(data[list(data.columns)[1]], autopct=autopct, colors=colors)
        if title is not None:
            ax.set_title(title)
        if legend is not None:
            ax.legend(legend)
        return self.make_image()

    def make_bar(self,
                 data,
                 legend,
                 title,
                 top_labels=True,
                 colors=DEFAULT_COLORS,
                 width=0.5,
                 fontsize=12,
                 rot=None,
                 suffix=True):
        """
        Method for making a bar chart
        :param data: pandas dataframe to plot
        :param legend: Legend to be used if not None, otherwise dataframe columns are used as labels
        :param title: Graph title
        :param top_labels: Boolean to display numeric value at the top of each bar
        :param colors: List of colors to use
        :param width: Bar width (not sure on units)
        :param fontsize: Font size
        :param rot: Angle to rotate labels
        :param suffix: True to contract values to suffix (ex. 6100 -> 6K), False to use raw value on axis
        :return: Returns byte array of image
        """
        l = len(list(data.columns)[1:])
        width = width / 2
        keys = list(data.columns)[1:]
        m = max([max(data[col]) for col in keys]) if len(data) > 0 else 0
        shift = 0
        try:
            ax = data.plot(
                x=list(data.columns)[0],
                y=list(data.columns[1:]),
                kind='bar',
                color=colors[:l],
                fontsize=fontsize
            )
        except TypeError as err:
            columns = list(data.columns)
            data = {c: [0.0] for c in columns[1:]}
            data[columns[0]] = ['']
            data = pandas.DataFrame(data)
            data = data[columns]
            ax = data.plot(
                x=list(data.columns)[0],
                y=list(data.columns[1:]),
                kind='bar',
                color=colors[:l],
                fontsize=fontsize
            )
        if top_labels:
            try:
                locale.setlocale(locale.LC_NUMERIC, 'en_US')
            except locale.Error:
                locale.setlocale(locale.LC_NUMERIC, 'EN-US')
            for key in keys:
                for i, v in enumerate(data[key]):
                    ax.text(
                        i - width + (shift * width),
                        v + m * 0.01,
                        locale.format("%d", int(np.round(v, 0)), grouping=True),
                        color='black',
                        fontsize='x-small'
                    )
                shift += 1
            locale.setlocale(locale.LC_NUMERIC, None)
        ax.grid('on', axis='y', alpha=0.4)
        if title is not None:
            ax.set_title(title)
        ax.set_xlabel('')
        plt.xticks(plt.xticks()[0], plt.xticks()[1], rotation=rot)
        plt.yticks(list(plt.yticks()[0])+[max(plt.yticks()[0]) * 1])
        if legend is not None:
            ax.legend(legend)
        else:
            ax.legend(list(data.columns)[1:])
        if suffix:
            formatter = FuncFormatter(contract)
            ax.yaxis.set_major_formatter(formatter)
        return self.make_image()

    def make_line(self,
                  data,
                  legend,
                  title,
                  colors=DEFAULT_COLORS,
                  y_lim=None,
                  suffix=True):
        """
        Method for making a line chart
        :param data: pandas dataframe to plot
        :param legend: Legend to be used if not None, otherwise dataframe columns are used as labels
        :param title: Graph title
        :param colors: Colors to be used
        :param y_lim: If not None, this value is used as a hard max limit on the y axis
        :param suffix: True to contract values to suffix (ex. 6100 -> 6K), False to use raw value on axis
        :return: Returns byte array of image
        """
        l = len(list(data.columns)[1:])
        ax = data.plot(x=list(data.columns)[0], y=list(data.columns)[1:], kind='line', color=colors[:l])
        ax.grid('on', axis='y', alpha=0.4)
        if title is not None:
            ax.set_title(title)
        ax.set_xlabel('')
        plt.xticks(plt.xticks()[0], plt.xticks()[1], rotation=0)
        plt.yticks(list(plt.yticks()[0])+[max(plt.yticks()[0]) * 1])
        if y_lim is not None:
            ax.set_ylim(bottom=0, top=y_lim)
        if legend is not None:
            ax.legend(legend)
        else:
            ax.legend(list(data.columns)[1:])
        if suffix:
            formatter = FuncFormatter(contract)
            ax.yaxis.set_major_formatter(formatter)
        return self.make_image()

    def multi_axes_line(self,
                        left,
                        right,
                        title,
                        colors=DEFAULT_COLORS,
                        shade_right=True,
                        left_y_lim=None,
                        right_y_lim=None,
                        suffix=True):
        """
        Method for generating two graphs on a single plot (each vertical side of the area is a y axis)
        :param left: pandas dataframe to plot, uses left side for y-axis
        :param right: pandas dataframe to plot, uses right side for y-axis
        :param title: Title of the graph
        :param colors: Colors to use
        :param shade_right: Shade to use on right graph
        :param left_y_lim: If not None, hard limit to use on left y axis
        :param right_y_lim: If not None, hard limit to use on right y axis
        :param suffix: If True, contracts values to use suffix (ex. 6100 -> 6.1K), False to use raw value on axis
        :return: Returns byte array of image
        """
        ax = left.plot(x=list(left.columns)[0], y=list(left.columns)[1:], legend=False, color=colors[1:len(left.columns)-1])
        ax2 = ax.twinx()
        ax2 = right.plot(x=list(right.columns)[0],
                         y=list(right.columns)[1:],
                         ax=ax2,
                         legend=False,
                         color=colors[len(left.columns):])
        if shade_right:
            ax2.fill_between(list(right[list(right.columns)[0]]), 0, list(right[list(right.columns)[1]]), alpha=0.1)
        if left_y_lim is not None:
                if type(left_y_lim) == tuple:
                        ax.set_ylim(bottom=left_y_lim[0], top=left_y_lim[1])
                else:
                        ax.set_ylim(bottom=0, top=left_y_lim)
        if right_y_lim is not None:
                if type(right_y_lim) == tuple:
                        ax2.set_ylim(bottom=right_y_lim[0], top=right_y_lim[1])
                else:
                        ax2.set_ylim(bottom=0, top=right_y_lim)
        ax.legend(list(left.columns)[1:], loc='upper left')
        ax2.legend(list(right.columns)[1:], loc='upper right')
        if suffix:
            formatter = FuncFormatter(contract)
            ax.yaxis.set_major_formatter(formatter)
            ax2.yaxis.set_major_formatter(formatter)
        plt.title(title)
        return self.make_image()

