from jobs import decorators
from jobs.monitoring.rmp_monitor import monitor as rmp_monitor


@decorators.validator_common
def param_validator(**kwargs):
    pass


@decorators.job_wrapper
def run(**kwargs):
    rmp_monitor()


if __name__ == '__main__':
    run()
