import os
from jobs import settings


MAIN = """from jobs import decorators


@decorators.validator_common
def param_validator(**kwargs):
    return True, kwargs,


@decorators.job_wrapper
def run(**kwargs):
    pass
"""


if os.name == 'nt':
    PATH = os.path.dirname(os.path.realpath(__file__)).split('\\')
    PATH = '\\'.join(PATH[:len(PATH) - 1])
else:
    PATH = os.path.dirname(os.path.realpath(__file__)).split('/')
    PATH = '/'.join(PATH[:len(PATH) - 1])


def make_dirs(job_name):
    queries = PATH + '/queries/{0}'.format(job_name)
    module_dir = PATH + '/jobs/{0}'.format(job_name)
    os.mkdir(queries)
    os.mkdir(module_dir)


def make_files(job_name):
    with open(PATH+'/jobs/{0}/__init__.py'.format(job_name), 'w') as f:
        f.write('')
    with open(PATH+'/jobs/{0}/main.py'.format(job_name), 'w') as f:
        f.write(MAIN)
    attributes = list(dir(settings))
    attributes = [i for i in attributes if i.upper() == i]
    with open(PATH+'/jobs/{0}/settings.py'.format(job_name), 'w') as f:
        out = []
        for attr in attributes:
            item = getattr(settings, attr)
            if item == 'SCRIPT_NAME':
                out.append('SCRIPT_NAME = \'{0}\''.format(job_name))
            elif type(item) == str:
                out.append('{0} = \'{1}\''.format(attr, item))
            else:
                out.append('{0} = {1}'.format(attr, item))
        out = '\n\n'.join(out) + '\n'
        f.write(out)


def run(job_name):
    make_dirs(job_name)
    make_files(job_name)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('job_name', type=str)
    args = parser.parse_args()
    run(args.job_name)
