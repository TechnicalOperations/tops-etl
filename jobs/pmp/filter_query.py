import json
import pandas
import numpy as np
from etl.util import db_conn
from etl.util import io
from datetime import datetime
from datetime import timedelta
from etl import logger as job_logger
from jobs.pmp import settings


REDIS_KEY_CHUNK = 'pmp/filter_data_{0}'
REDIS_KEY_CHUNK_NAMES = 'pmp/filter_data_names'
REDIS_KEY_COLUMN_NAMES = 'pmp/filter_column_names'

JSON_STR = ['segments', 'dsps']
EMPTY_STR = ['adstxt', 'ad_size', 'video_size', 'device_type', 'imp_type', 'media_type', 'country', 'ssp_id']
FLOAT_FILL = {'ias_rate': 100.0, 'dv_rate': 100.0, 'viewability': 0.0}


class _Attributes:

    def __init__(self):
        self.ias_rate = None
        self.dv_rate = None
        self.segments = None
        self.viewability=None
        self.dsps = None


def build_attributes_query(deals):
    attributes = _Attributes()
    deals = list(deals.values())
    adsizes = [i.adsize_list for i in deals if i.adsize_list is not None]
    adsizes = set([z for i in adsizes for z in i])
    adsizes = "', '".join(adsizes)
    # floors = list(set([i.bidfloor for i in deals]))
    # floors.sort(key=lambda x: 0 - x)
    # floors = '\n      '.join(['WHEN bid_floor >= {0} THEN {0}'.format(i) for i in floors])
    ias_rate = set(
        [
            i.brand_safety['IAS'] for i in deals
            if i.brand_safety is not None and i.brand_safety.get('IAS', None) is not None
        ]
    )
    ias_rate = sorted(list(ias_rate))
    attributes.ias_rate = ias_rate
    ias_rate = '\n      '.join(['WHEN ias_rate <= {0} THEN {0}'.format(i) for i in ias_rate])
    dv_rate = set(
        [
            i.brand_safety['DV'] for i in deals
            if i.brand_safety is not None and i.brand_safety.get('DV', None) is not None
        ]
    )
    dv_rate = sorted(list(dv_rate))
    attributes.dv_rate = dv_rate
    dv_rate = '\n      '.join(['WHEN dv_rate <= {0} THEN {0}'.format(i) for i in dv_rate])
    countries = [i.country_list for i in deals if i.country_list is not None]
    countries = set([z for i in countries for z in i])
    countries = "', '".join(countries)
    segments = [v for i in deals for k, v in i.segment.items() if i.segment is not None]
    segments = [v for i in segments for k, v in i.items()]
    segments = [v for i in segments for k, v in i.items()]
    attributes.segments = set([z for i in segments for z in i])
    segments = list(set([z.replace("'", "\\'") for i in segments for z in i]))
    segments = ',\n          '.join(
        [
            "IFF(ARRAY_CONTAINS(LOWER('{0}')::variant, PARSE_JSON(segments)), '{0}', NULL)".format(i)
            for i in segments
        ]
    )
    viewability = set(
        [
            i.viewability_percentage['IAS'] for i in deals
            if i.viewability_percentage is not None and i.viewability_percentage.get('IAS', None) is not None
        ]
    )
    viewability = list(viewability)
    viewability.sort(key=lambda x: 0 - x)
    attributes.viewability = viewability
    viewability = '\n        '.join([
        '''
        WHEN GREATEST(
            ZEROIFNULL(GET(PARSE_JSON(ias_viewability), 'adsize-country-domain')::decimal(15, 5)),
            ZEROIFNULL(GET(PARSE_JSON(ias_viewability), 'adsize-country-pubid-siteid-appid')::decimal(15, 5)),
            ZEROIFNULL(GET(PARSE_JSON(ias_viewability), 'pubid-adsize')::decimal(15, 5)),
            ZEROIFNULL(GET(PARSE_JSON(ias_viewability), 'score')::decimal(15, 5)),
            ZEROIFNULL(GET(PARSE_JSON(ias_viewability), 'sspid-pubid-siteid-appid')::decimal(15, 5))
        ) >= {0} THEN {0}'''.format(i)
        for i in viewability
    ])
    dsps = list(set([z for i in deals for z in i.dsp_list]))
    attributes.dsps = set(dsps)
    dsps = '.*|.*'.join(dsps)
    end_date = datetime.now()
    end_date = end_date.replace(hour=0, minute=0, second=0, microsecond=0)
    start_date = end_date - timedelta(days=1)
    query = io.get_query('pmp/filter_query')
    return query.format(
        start_date=start_date,
        end_date=end_date,
        # viewability=viewability,
        # segments=segments,
        countries=countries,
        # dv_rate=dv_rate,
        # ias_rate=ias_rate,
        # floors=floors,
        adsizes=adsizes,
        dsps=dsps
    ), attributes,


def check_values(value, dump):
    if value not in dump:
        dump[value] = value
    return dump[value]


def compress_data(chunk, attributes):
    columns = list(chunk.columns)
    columns = [column for column in columns]
    chunk = chunk.to_dict(orient='records')
    deal_segments = attributes.segments
    out = {}
    for row in chunk:
        row['segments'] = set(row['segments']) & deal_segments
        row['viewability'] = [i for i in attributes.viewability if row['viewability'] >= i]
        row['viewability'] = None if len(row['viewability']) == 0 else row['viewability'][0]
        row['dv_rate'] = [i for i in attributes.dv_rate if i >= row['dv_rate']]
        row['dv_rate'] = None if len(row['dv_rate']) == 0 else row['dv_rate'][0]
        row['ias_rate'] = [i for i in attributes.ias_rate if i >= row['ias_rate']]
        row['ias_rate'] = None if len(row['ias_rate']) == 0 else row['ias_rate'][0]
        row['dsps'] = set(row['dsps']) & attributes.dsps
        row['requests'] = 0 if row['requests'] is None or np.isnan(row['requests']) else int(row['requests'])
        for column in JSON_STR:
            row[column] = tuple(sorted(list(row[column]))) if type(row[column]) == set else tuple([])
        for column in EMPTY_STR:
            row[column] = '' if row[column] is None else row[column]
        for column, value in FLOAT_FILL.items():
            row[column] = value if row[column] is None or np.isnan(row[column]) else row[column]
        tmp = [(k, v,) for k, v in row.items()]
        tmp.sort(key=lambda x: columns.index(x[0]))
        tmp = tuple([i[1] for i in tmp if i[0] != 'requests'])
        if tmp not in out:
            out[tmp] = 0
        out[tmp] += row['requests']
    out = [k+tuple([v]) for k, v in out.items()]
    return out


def chunk_iterator(names):
    for name in names:
        data = io.get_redis(name)
        if data is None:
            break
        yield data


def get_filter_data(deals):
    logger = job_logger.get_logger(settings.SCRIPT_NAME)
    logger.info('Checking redis for existing filter data')
    chunk_names = io.get_redis(REDIS_KEY_CHUNK_NAMES)
    if chunk_names is not None:
        logger.info('Found existing filter data')
        return chunk_iterator(chunk_names)
    logger.info('No existing data found')
    logger.info('Building out query to fetch filter data')
    query, attributes, = build_attributes_query(deals)
    con = db_conn.connect('snowflake')
    logger.info('Fetching filter data')
    iter = pandas.read_sql_query(query, con, chunksize=1000000)
    chunk_id = 0
    chunk_names = []
    logger.info('Iterating over chunks')
    columns = {}
    for chunk in iter:
        logger.info('Formatting filter data')
        chunk = chunk.rename(columns={i: i.lower() for i in chunk.columns})
        segments = list(set(chunk['segments']))
        segments = {i: json.loads(i) for i in segments}
        chunk['segments'] = chunk['segments'].apply(lambda x: segments[x])
        dsps = list(set(chunk['dsps']))
        dsps = {i: json.loads(i) for i in dsps}
        chunk['dsps'] = chunk['dsps'].apply(lambda x: dsps[x])
        columns = list(chunk.columns)
        columns = {columns[i]: i for i in range(len(columns))}
        logger.info('Compressing data further')
        chunk = compress_data(chunk, attributes)
        logger.info('Storing filter data in redis for 24 hours')
        chunk_names.append(REDIS_KEY_CHUNK.format(chunk_id))
        io.to_redis(86400, **{REDIS_KEY_CHUNK.format(chunk_id): chunk})
        chunk_id += 1
    io.to_redis(86400, **{REDIS_KEY_CHUNK_NAMES: chunk_names})
    io.to_redis(86400, **{REDIS_KEY_COLUMN_NAMES: columns})
    db_conn.LIVE_CONNECTIONS['snowflake']['con'].close()
    del db_conn.LIVE_CONNECTIONS['snowflake']
    return chunk_iterator(chunk_names)
