import os
import queue
import pandas
import numpy as np
import multiprocessing
from time import sleep
from etl.util import io
from etl import logger as job_logger
from etl.util import db_conn
from jobs.pmp import deal_config
from jobs.pmp import filter_query
from jobs.pmp import settings
from datetime import datetime
from datetime import timedelta


COLUMN_NAMES = None
logger = job_logger.get_logger(settings.SCRIPT_NAME)


def ssp_filter(deal, data):
    if deal.ssp_list is None or len(deal.ssp_list) == 0:
        return data
    return [
        i for i in data
        if (deal.include_ssp and i[COLUMN_NAMES['ssp_id']] in deal.ssp_list)
        or (not deal.include_ssp and i[COLUMN_NAMES['ssp_id']] not in deal.ssp_list)
    ]


def dsps_filter(deal, data):
    if deal.dsp_list is None or len(deal.dsp_list) == 0:
        return data
    return [
        i for i in data
        if i[COLUMN_NAMES['dsps']] in deal.dsp_list
    ]


# def bid_floor_filter(deal, data):
#     data = data[(data['bid_floor'] >= deal.bidfloor)]
#     return data


def media_type_filter(deal, data):
    media_types = set()
    if deal.include_site:
        media_types.add('site')
    if deal.include_app:
        media_types.add('app')
    return [i for i in data if i[COLUMN_NAMES['media_type']] in media_types]


def imp_type_filter(deal, data):
    imp_types = set()
    if deal.include_native:
        imp_types.add('native')
    if deal.include_video:
        imp_types.add('video')
    if deal.include_banner:
        imp_types.add('banner')
    return [i for i in data if i[COLUMN_NAMES['imp_type']] in imp_types]


def device_type_filter(deal, data):
    devices = set()
    if deal.include_desktop:
        devices.add('desktop')
    if deal.include_mobile:
        devices.add('mobile')
    if deal.include_ctv:
        devices.add('ctv')
    if len(devices) == 3:
        devices.add('unknown')
    return [i for i in data if i[COLUMN_NAMES['device_type']] in devices]


def country_filter(deal, data):
    if deal.country_list is None or len(deal.country_list) == 0:
        return data
    return [
        i for i in data
        if (deal.include_country and i[COLUMN_NAMES['country']].upper() in deal.country_list)
        or (not deal.include_country and i[COLUMN_NAMES['country']].upper() not in deal.country_list)
    ]


def brand_safety_filter(deal, data):
    if deal.brand_safety is None:
        return data
    return [
        i for i in data
        if deal.brand_safety.get('IAS', 100.0) < i[COLUMN_NAMES['ias_rate']]
        and deal.brand_safety.get('DV', 100.0) < i[COLUMN_NAMES['dv_rate']]
    ]


def size_filter(deal, data):
    return [
        i for i in data
        if (
            i[COLUMN_NAMES['imp_type']] == 'banner'
            and deal.include_adsize
            and i[COLUMN_NAMES['ad_size']] in deal.adsize_list
        )
        or (
            i[COLUMN_NAMES['imp_type']] == 'banner'
            and not deal.include_adsize
            and i[COLUMN_NAMES['ad_size']] not in deal.adsize_list
        )
        or (
            i[COLUMN_NAMES['imp_type']] == 'video'
            and deal.include_videosize
            and i[COLUMN_NAMES['ad_size']] in deal.adsize_list
        )
        or (
            i[COLUMN_NAMES['imp_type']] == 'video'
            and not deal.include_videosize
            and i[COLUMN_NAMES['video_size']] not in deal.videosize_list
        )
    ]


def adstxt_filter(deal, data):
    if deal.ads_txt != 'has_r1' and deal.ads_txt != 'has_r1_or_missing':
        return data
    if deal.ads_txt == 'has_r1':
        return data[(data['adstxt'] == 'has_r1')]
    if deal.ads_txt == 'has_r1_or_missing':
        return data[data['adstxt'].isin(['has_r1', 'missing'])]
    return [
        i for i in data
        if (deal.ads_txt == 'has_r1' and i[COLUMN_NAMES['adstxt']] == 'has_r1')
        or (deal.ads_txt == 'has_r1_or_missing' and i[COLUMN_NAMES['adstxt']] in {'has_r1', 'missing'})
    ]


def viewability_filter(deal, data):
    if deal.viewability_percentage is None:
        return data
    return [
        i for i in data
        if (deal.viewability_percentage.get('IAS', 0.0) <= i[COLUMN_NAMES['viewability']])
        or (deal.viewability_percentage.get('DV', 0.0) <= i[COLUMN_NAMES['viewability']])
    ]


def segments_filter(deal, data):
    if deal.segment is None or len(deal.segment) == 0:
        return data
    segments = {
        media: {
            inc_type: [v for k, v in group.items()]
            for inc_type, group in items.items()
        }
        for media, items in deal.segment.items()
    }
    s_col = COLUMN_NAMES['segments']
    m_col = COLUMN_NAMES['media_type']
    tmp = []
    for i in data:
        if i[m_col] not in segments:
            tmp.append(i)
            continue
        m_type = segments[m_col]
        s = set(i[s_col])
        if 'include' in m_type:
            if True not in [len(s & set(i)) > 0 for i in m_type['include']]:
                continue
        if 'exclude' in m_type:
            if len(m_type['exclude']) > 0 and len(set(s & set(m_type['exclude'][0]))) > 0:
                continue
        tmp.append(i)
    return tmp


EVAL_ORDER = [
    ('ssp_id', ssp_filter,),
    ('segments', segments_filter,),
    ('viewability', viewability_filter,),
    ('imp_type', imp_type_filter,),
    # ('bid_floor', bid_floor_filter,),
    ('media_type', media_type_filter,),
    ('device_type', device_type_filter,),
    ('country', country_filter,),
    ('brand_safety', brand_safety_filter,),
    ('size', size_filter,),
    ('adstxt', adstxt_filter,),
    ('dsps', dsps_filter,),
]


def evaluate_funnel_thread(data, deals, output_queue):
    total_length = len(deals)
    pid = os.getpid()
    logger = job_logger.get_logger(settings.SCRIPT_NAME)
    count = 0
    mod = int(total_length / 10)
    data = [data[i]+(i,) for i in range(len(data))]
    l = len(data[0])-1
    for deal_id, deal_config in deals.items():
        row = {
            'deal_id': deal_id
        }
        filt = data
        for column, func in EVAL_ORDER:
            if len(filt) > 0:
                filt = func(deal_config, filt)
            if len(filt) > 0:
                row[column] = sum([i[COLUMN_NAMES['requests']] for i in filt])
            else:
                row[column] = 0
                break
        row['dsp_list'] = set([z for i in filt for z in i[COLUMN_NAMES['dsps']]]) & deal_config.dsp_list
        row['idx_list'] = {
            '{0}_{1}'.format(pid, filt[i][l]): filt[i][COLUMN_NAMES['requests']]
            for i in range(len(filt))
        }
        output_queue.put(row)
        count += 1
        if mod > 0 and count % mod == 0:
            logger.info('PID {0} {1}% Complete'.format(pid, count * 100.0 / total_length))
    logger.info('PID {0} Complete Exiting Now'.format(pid))


def evaluate_competition(output, select=7):
    tmp = output[['deal_id', 'dsp_list', 'idx_list']].to_dict(orient='records')
    comp = []
    for row in tmp:
        for dsp in row['dsp_list']:
            for idx, requests in row['idx_list'].items():
                comp.append(
                    {
                        'deal_id': row['deal_id'],
                        'dsp': dsp,
                        'idx': idx,
                        'requests': requests
                    }
                )
    del tmp
    cols = ['deal_id', 'total'] + [i[0] for i in EVAL_ORDER] + ['final_estimate']
    if len(comp) == 0:
        output['final_estimate'] = 0
    else:
        comp = pandas.DataFrame(comp)[['deal_id', 'dsp', 'idx', 'requests']]
        comp = comp.groupby(['idx', 'dsp', 'requests']).agg(lambda x: set(x)).reset_index()
        comp['probability'] = comp['deal_id'].apply(lambda x: 1 if len(x) < select else select / len(x))
        comp = comp[['idx', 'deal_id', 'requests', 'probability']].to_dict(orient='records')
        tmp = []
        for row in comp:
            for deal in row['deal_id']:
                tmp.append(
                    {
                        'idx': row['idx'],
                        'deal_id': deal,
                        'requests': row['requests'],
                        'probability': row['probability']
                    }
                )
        if len(tmp) == 0:
            comp = pandas.DataFrame(columns=['idx', 'deal_id', 'requests', 'probability'])
        else:
            comp = pandas.DataFrame(tmp)
        comp['requests'] = comp['requests'] * comp['probability']
        comp['requests'] = comp['requests'].fillna(0).astype(int)
        del comp['probability']
        comp = comp.groupby(['idx', 'deal_id']).mean().reset_index()
        del comp['idx']
        comp = comp.groupby(['deal_id']).sum().reset_index()
        comp['requests'] = comp['requests'].fillna(0).astype(int)
        comp = comp.rename(columns={'requests': 'final_estimate'})
        output = output.merge(comp, on='deal_id', how='left')
        output['final_estimate'] = output['final_estimate'].fillna(0).astype(int)
    return output[cols]


def get_actual():
    end_date = datetime.now()
    end_date = end_date.replace(hour=0, minute=0, second=0, microsecond=0)
    start_date = end_date - timedelta(days=1)
    conn = db_conn.DbConnection('snowflake')
    actual = conn.get_frame('pmp/actual_requests', start_date=start_date, end_date=end_date)
    return actual


def evaluate_funnel(deals, data, threads=4):
    output = []
    if threads > 4:
        threads = 4
    thread_list = []
    output_queue = multiprocessing.JoinableQueue()
    logger.info('Creating and starting processes')
    logger.info('Iterating over chunks')
    chunk = next(data)
    total = 0
    while chunk is not None:
        logger.info('Creating {0} threads'.format(threads-len(thread_list)))
        total += sum([i[COLUMN_NAMES['requests']] for i in chunk])
        for i in range(threads-len(thread_list)):
            t = multiprocessing.Process(
                target=evaluate_funnel_thread,
                args=(chunk, deals, output_queue,)
            )
            t.start()
            logger.info('Started PID {0}'.format(t.pid))
            thread_list.append(t)
            logger.info('Getting next data chunk for processing')
            try:
                chunk = next(data)
            except StopIteration as err:
                chunk = None
                logger.info('Fetched all data.  Waiting for processes to complete')
                break
        sleep(3)
        while len(thread_list) == threads:
            for t in thread_list:
                t.join(3)
            while not output_queue.empty():
                try:
                    output.append(output_queue.get(timeout=10))
                except queue.Empty as err:
                    continue
                output_queue.task_done()
            thread_list = [t for t in thread_list if t.is_alive()]
            logger.info('Live threads remaining : {0}'.format(len(thread_list)))
    logger.info('Retrieving output from processes')
    while not output_queue.empty():
        try:
            output.append(output_queue.get(timeout=10))
        except queue.Empty as err:
            continue
        output_queue.task_done()
    logger.info('Estimating competitive reductions')
    output = pandas.DataFrame(output)
    agg_funcs = {
        i[0]: np.sum
        for i in EVAL_ORDER
    }
    for i in EVAL_ORDER:
        if i[0] not in output:
            output[i[0]] = 0
        else:
            output[i[0]] = output[i[0]].fillna(0).astype(int)
    agg_funcs['dsp_list'] = lambda x: set([z for i in x for z in i])
    agg_funcs['idx_list'] = lambda x: {k: v for i in x for k, v in i.items()}
    # agg_funcs['total'] = np.sum
    output = output.groupby('deal_id').agg(agg_funcs).reset_index()
    output['total'] = total
    # output = output[columns[:len(columns) - 2]]
    output = evaluate_competition(output)
    actual = get_actual()
    actual = actual.rename(columns={'requests': 'actual_requests'})
    output = output.merge(actual, on='deal_id', how='left')
    output['actual_requests'] = output['actual_requests'].fillna(0).astype(int)
    return output


def build_funnel(deals, threads=4):
    global COLUMN_NAMES
    deal_ids = list(deals) if deals is not None else []
    deals = io.get_redis(deal_config.REDIS_KEY)
    deals = deals if len(deal_ids) == 0 else {k: v for k, v in deals.items() if k in deal_ids}
    data = filter_query.get_filter_data(deals)
    COLUMN_NAMES = io.get_redis(filter_query.REDIS_KEY_COLUMN_NAMES)
    funnel = evaluate_funnel(deals, data, threads=threads)
    return funnel


if __name__ == '__main__':
    import pickle
    pandas.set_option('display.width', 5000)
    deals = deal_config.get_deal_setup(None)
    outer_data = filter_query.get_filter_data(deals)
    funnel = evaluate_funnel(deals, outer_data)
    with open(os.path.expanduser('~')+'/Documents/data/funnel.bin', 'wb') as f:
        f.write(pickle.dumps(funnel))
