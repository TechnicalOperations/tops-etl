import numpy as np
from jobs import decorators
from jobs.pmp import funnel
from jobs.pmp import settings
from jobs.pmp import deal_config
from etl.util import io
from etl import logger as job_logger


@decorators.validator_common
def param_validator(**kwargs):
    if type(kwargs['deal_ids']) == str:
        kwargs['deal_ids'] = kwargs['deal_ids'].split(',')
    return kwargs, True,


@decorators.job_wrapper
def run(**kwargs):
    logger = job_logger.get_logger(settings.SCRIPT_NAME)
    logger.info('Getting deal setups')
    deals = deal_config.get_deal_setup(None)
    logger.info('Evaluating/creating funnel')
    funnel_data = funnel.build_funnel(deals)
    diff = funnel_data['actual_requests'] - funnel_data['final_estimate']
    diff = diff.apply(np.abs)
    funnel_data['difference'] = diff * 100.0 / funnel_data['actual_requests']
    # print(funnel_data[(funnel_data['actual_requests'] >= 20)])
    return funnel_data


if __name__ == '__main__':
    funnel_output = run()
    io.to_redis(3600, **{'pmp/funnel_output': funnel_output})
