import json
from etl.util import io
from etl.util import db_conn


REDIS_KEY = 'pmp/deal_config'


class Deal:

    def __init__(self, row):
        self.deal_id = row[0]
        self.description = row[1]
        self.deal_type = row[2]
        self.dsp_list = set([i.strip() for i in row[3].split(',')])
        if '' in self.dsp_list:
            self.dsp_list.remove('')
        if row[4] is not None:
            self.ssp_list = set([i.strip() for i in row[4].split(',')])
            if '' in self.ssp_list:
                self.ssp_list.remove('')
        else:
            self.ssp_list = None
        if row[5] is not None:
            self.viewability_percentage = [z.split(';;;') for z in row[5].split(',')]
            self.viewability_percentage = {z[0]: float(z[1]) for z in self.viewability_percentage if len(z) == 2}
        else:
            self.viewability_percentage = None
        if row[6] is not None:
            self.brand_safety = [z.split(';;;') for z in row[6].split(',')]
            self.brand_safety = {z[0]: float(z[1]) for z in self.brand_safety if len(z) == 2}
        else:
            self.brand_safety = None
        if row[7] is not None:
            temp_segment = [z.split(';;;') for z in row[7].split(',')]
            self.segment = {}
            for segment_row in temp_segment:
                if len(segment_row) < 4:
                    continue
                if segment_row[1] not in self.segment:
                    self.segment[segment_row[1]] = {}
                media = self.segment[segment_row[1]]
                if segment_row[2] not in media:
                    media[segment_row[2]] = {}
                filter_type = media[segment_row[2]]
                if segment_row[3] not in filter_type:
                    filter_type[segment_row[3]] = {segment_row[0]}
                else:
                    filter_type[segment_row[3]].add(segment_row[0])
        else:
            self.segment = None
        self.bidfloor = float(row[8]) if row[8] is not None else None
        self.include_ssp = False if row[9] == 'exclude' else True
        self.include_country = True if row[10] == 'include' else False
        self.country_list = None if row[11] is None else set(
            [k for k, v in json.loads(row[11]).items() if v == 1]
        )
        self.include_adsize = True if row[12] == 'include' else False
        self.adsize_list = None if row[13] is None else set(
            [k for k, v in json.loads(row[13]).items() if v == 1]
        )
        self.include_videosize = True if row[14] == 'include' else False
        self.videosize_list = None if row[15] is None else set(
            [k for k, v in json.loads(row[15]).items() if v == 1]
        )
        self.include_banner = True if row[16] == 1 else False
        self.include_video = True if row[17] == 1 else False
        self.include_native = True if row[18] else False
        self.include_desktop = True if row[19] else False
        self.include_mobile = True if row[20] else False
        self.include_site = True if row[21] else False
        self.include_app = True if row[22] else False
        self.ads_txt = row[23]
        self.wseat = row[24]
        self.include_inviewpercent = int(row[25]) if row[25] is not None else None
        self.include_ctv = True if row[26] else False


def get_deal_setup(deal_ids):
    """
    :param deal_ids: String array of deals
    :return: Deal class dictionary
    """
    deals = io.get_redis(REDIS_KEY)
    if deal_ids is not None and deals is not None and len(set(deals.keys()) & set(deal_ids)) == 0:
            return {k: v for k, v in deals.items() if k in set(deal_ids)}
    elif deals is not None:
        return deals
    con = db_conn.connect('rx_mysql')
    cur = con.cursor()
    if deal_ids is None or len(deal_ids) == 0:
        f = ''
    else:
        f = """
        AND A.external_deal_id IN (
            '{0}'
         )""".format("',\n'".join(deal_ids))
    query = io.get_query(REDIS_KEY).format(f)
    cur.execute(query)
    deals = cur.fetchall()
    if db_conn.LIVE_CONNECTIONS['rx_mysql']['count'] == 1:
        con.close()
        del db_conn.LIVE_CONNECTIONS['rx_mysql']
    else:
        db_conn.LIVE_CONNECTIONS['rx_mysql']['count'] -= 1
    deals = {i[0]: Deal(i) for i in deals}
    io.to_redis(28800, **{REDIS_KEY: deals})
    return deals
