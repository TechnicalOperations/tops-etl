DEFAULT_PARAMS = {
    'to': 'cbeecher@rhythmone.com',
    'cc': ['mvillaseca@rhythmone.com']
}

DEPENDENCIES = []

DESCRIPTION = 'Ads.txt request volume for last 7 days'

DISPLAY_NAME = 'Ads.txt Request Volume'

FIELD_DESCRIPTORS = [
    {
        'name': 'to',
        'type': 'str',
        'required': True,
        'descriptor': 'Who to send the results to'
    },
    {
        'name': 'cc',
        'type': 'str_array',
        'required': False,
        'descriptor': 'Anyone to CC on the results'
    }
]

FREQUENCY = 'MANUALLY'

IS_PUBLIC = False

SCRIPT_NAME = 'adstxt_7days'

TYPE = 'data_tools'
