from io import BytesIO
from jobs import decorators
from etl.util import io
from etl.util import db_conn
from etl.notification import email_notifier
from datetime import datetime
from datetime import timedelta


@decorators.validator_common
def param_validator(**kwargs):
    return True, kwargs,


@decorators.job_wrapper
def run(**kwargs):
    conn = db_conn.DbConnection('snowflake')
    end_date = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    start_date = end_date - timedelta(days=7)
    excel = BytesIO()
    data = conn.get_frame(
        'adstxt_7dayreqs/get_request_volumes',
        start_date=start_date.strftime('%Y-%m-%d'),
        end_date=end_date.strftime('%Y-%m-%d')
    )
    data = data.sort_values(by='requests', ascending=False).reset_index(drop=True)
    io.to_excel(excel, **{'Adstxt Volume': data})
    excel.seek(0)
    notifier = email_notifier.get_mailer_object(
        'adstxt_7dayreqs/mailer',
        subs={
            'start_date': start_date.strftime('%Y-%m-%d'),
            'end_date': (end_date - timedelta(days=1)).strftime('%Y-%m-%d')
        }
    )
    notifier.attach_file(excel, 'Adstxt7DayRequests_{0}-{1}.xlsx'.format(
        start_date.strftime('%Y-%m-%d'),
        (end_date - timedelta(days=1)).strftime('%Y-%m-%d')
    ))
    notifier.set_formatter(
        {
            'start_date': start_date.strftime('%Y-%m-%d'),
            'end_date': (end_date - timedelta(days=1)).strftime('%Y-%m-%d')
        }
    )
    notifier.send_mail(
        'Ads.txt Request Volume (7 Days)',
        kwargs['to'],
        cc=kwargs['cc']
    )


if __name__ == '__main__':
    from jobs import arguments
    from jobs.adstxt_7dayreqs import settings
    kwargs = arguments.create_arguments(settings)
    run(**kwargs)
