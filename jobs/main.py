import os
import pandas
import multiprocessing
from etl.util import io
from etl.util import db_conn
from etl import monitoring
from etl import logger as job_logger
from time import sleep
from jobs import settings
from datetime import datetime
from datetime import timedelta


RUN_EXPIRE = 1209600
LAST_RUN_KEY = 'etl_runs/{0}'
ETL_BLOCK_SET = 'etl_block_set'
logger = job_logger.get_logger('job_container')


def get_all_scripts_names():
    path = os.path.dirname(os.path.realpath(__file__))
    packages = [i for i in os.listdir(path) if '.py' not in i and '__' not in i]
    all_names = {}
    for i in packages:
        if '__init__.py' not in os.listdir(path+'/' + i + '/'):
            continue
        exec('from jobs.{0} import settings as {0}_settings'.format(i))
        exec('from jobs.{0} import main as {0}_main'.format(i))
        all_names[eval('{0}_settings.SCRIPT_NAME'.format(i))] = eval('{0}_main'.format(i))
    return all_names


def get_all_job_descriptions():
    path = os.path.dirname(os.path.realpath(__file__))
    packages = [i for i in os.listdir(path) if '.py' not in i and '__' not in i]
    descriptions = []
    for i in packages:
        if '__init__.py' not in os.listdir(path+'/' + i + '/'):
            continue
        exec('from jobs.{0} import settings as {0}_settings'.format(i))
        fields = eval('{0}_settings.FIELD_DESCRIPTORS'.format(i))
        description = {
            'type': eval('getattr({0}_settings, \'TYPE\', None)'.format(i)),
            'frequency': eval('{0}_settings.FREQUENCY'.format(i)),
            'is_public': eval('{0}_settings.IS_PUBLIC'.format(i)),
            'name': eval('{0}_settings.SCRIPT_NAME'.format(i)),
            'description': eval('{0}_settings.DESCRIPTION'.format(i)),
            'fields': fields,
            'display_name': eval('{0}_settings.DISPLAY_NAME'.format(i))
        }
        descriptions.append(description)
    return descriptions


def get_settings(package):
    exec('from jobs.{0} import settings as {0}_settings'.format(package))
    handle = eval('{0}_settings'.format(package))
    items = [i for i in dir(settings) if i.upper() == i]
    dir_items = [i for i in dir(handle) if i.upper() == i]
    for item in items:
        if item not in dir_items:
            setattr(handle, item, getattr(settings, item))
        if item in {
            'SCRIPT_NAME', 'DESCRIPTION', 'DISPLAY_NAME', 'TYPE'
        } and getattr(handle, item, None) is None:
            raise ValueError('Missing required settings field: {0}'.format(item))
    return handle


def resolve_dependencies(handles):
    parents = set([k for k, v in handles.items() if len(v['dependencies']) == 0])
    params = {
        k: (v['params'], v['name'], v['latest'],)
        for k, v in handles.items() if k in parents
    }
    out = []
    handles = {k: [i for i in v if i not in parents] for k, v in handles.items() if k not in parents}
    if len(handles) > 0:
        out += resolve_dependencies(handles)
    parents = [(k,) + tuple(v) for k, v in params.items()]
    out.append(parents)
    return out


def get_jobs(**kwargs):
    logger.info('Retrieving jobs to be run')
    path = os.path.dirname(os.path.realpath(__file__))
    packages = [i for i in os.listdir(path) if '.py' not in i and '__' not in i]
    handles = {}
    logger.info('Determining which jobs are up to be run')
    for i in packages:
        if len(kwargs) > 0 and i not in kwargs:
            continue
        job_settings = get_settings(i)
        current_run = datetime.utcnow().replace(second=0, microsecond=0)
        last_run = io.get_redis(LAST_RUN_KEY.format(getattr(job_settings, 'SCRIPT_NAME', 'unknown')))
        if job_settings.FREQUENCY == 'MANUALLY':
            continue
        elif job_settings.FREQUENCY == 'HOURLY':
            delta = timedelta(hours=1)
            buffer = timedelta(minutes=15)
        elif job_settings.FREQUENCY == 'DAILY':
            current_run = current_run.replace(minute=0)
            delta = timedelta(days=1)
            buffer = timedelta(hours=10)
        else:
            current_run = current_run.replace(minute=0)
            delta = timedelta(days=7)
            buffer = timedelta(hours=10)
        if last_run is None:
            last_run = current_run - delta - buffer
        if current_run < last_run + delta + buffer:
            continue
        current_run = current_run if job_settings.FREQUENCY == 'HOURLY' else current_run.replace(hour=0)
        last_run = current_run
        if io.in_redis_set(ETL_BLOCK_SET, str((getattr(job_settings, 'SCRIPT_NAME', 'unknown'), last_run,))):
            continue
        io.to_redis_set(ETL_BLOCK_SET, str((getattr(job_settings, 'SCRIPT_NAME', 'unknown'), last_run,)))
        exec('from jobs.{0} import main as {0}_main'.format(i))
        handle = eval('{0}_main'.format(i))
        handles[i] = {
            'handle': handle,
            'dependencies': getattr(job_settings, 'DEPENDENCIES', list()),
            'params': getattr(job_settings, 'DEFAULT_PARAMS', dict()),
            'name': getattr(job_settings, 'SCRIPT_NAME', 'unknown'),
            'latest': last_run
        }
        # io.to_redis(RUN_EXPIRE, **{LAST_RUN_KEY.format(i): last_run})
    logger.info('Resolving job dependencies')
    jobs = resolve_dependencies(handles)
    jobs = [[(handles[z[0]]['handle'],)+tuple(z[1:]) for z in i] for i in jobs]
    jobs = [i for i in jobs if len(i) > 0]
    return jobs


def store_status():
    current = monitoring.Status.get_all_current()
    complete = monitoring.Status.get_all_complete()
    status = list(current.values()) + list(complete.values())
    status = [dict(i) for i in status]
    status = pandas.DataFrame(status)
    conn = db_conn.connect('tops_mysql')
    io.to_database(conn, 'etl', status=status)
    db_conn.close_connections('tops_mysql')


def update_detached_status(limit=timedelta(hours=6), lookback=timedelta(days=3)):
    current = monitoring.Status.get_all_current()
    status = list(current.values())
    status = set([dict(i)['uuid'] for i in status])
    start_limit = datetime.utcnow() - limit
    started = datetime.utcnow() - lookback
    conn = db_conn.connect('tops_mysql')
    cur = conn.cursor()
    cur.execute('USE etl')
    processing = pandas.read_sql_query(
        """
            SELECT uuid
            FROM etl.status
            WHERE state = 'processing'
                AND started >= '{0}'
                AND started < '{1}'
            GROUP BY uuid
        """.format(started, start_limit),
        conn
    )
    completed = pandas.read_sql_query(
        """
            SELECT uuid
            FROM etl.status
            WHERE state IN ('completed', 'error')
                AND started >= '{0}'
            GROUP BY uuid
        """.format(started),
        conn
    )
    hung = processing[
        ~processing['uuid'].isin(set(completed['uuid']))
        & ~processing['uuid'].isin(set(status))
    ]
    cur.execute(
        """
            UPDATE etl.status
            SET state = %s
            WHERE uuid IN ('{0}')
                AND state = 'processing'
        """.format("', '".join(set(hung['uuid']))),
        (monitoring.State.HUNG.value,)
    )
    conn.commit()
    cur.close()
    db_conn.close_connections('tops_mysql')


def run(**kwargs):
    try:
        logger.info('Storing status of all jobs')
        store_status()
        update_detached_status()
        monitoring.Status.flush_complete()
        max_processes = kwargs.pop('max_processes', 4)
        jobs = get_jobs(**kwargs)
        if len(jobs) == 0:
            logger.info('No jobs to be run right now')
            return
        times = {}
        for group in jobs:
            threads = {}
            logger.info('Starting threads for {0} jobs'.format(len(group)))
            for job, params, name, latest in group:
                params['_expire'] = RUN_EXPIRE
                params['_name'] = LAST_RUN_KEY.format(name)
                params['_time'] = latest
                thread = multiprocessing.Process(target=job.run, kwargs=params)
                logger.info('Started process for job \'{0}\''.format(name))
                times[name] = latest
                thread.start()
                threads[name] = thread
                while len(threads) == max_processes:
                    logger.info('Max processes consumed.  Waiting for resources to be freed')
                    sleep(5)
                    names = set(threads.keys())
                    threads = {k: v for k, v in threads.items() if v.is_alive()}
                    for key in names:
                        if key not in threads:
                            io.delete_from_redis_set(ETL_BLOCK_SET, str((key, times[key],)))
            logger.info('Waiting for group to complete')
            while len(threads) > 0:
                threads = {k: v for k, v in threads.items() if v.is_alive()}
                sleep(15)
        logger.info('Completed all jobs.  Exiting now')
    except KeyboardInterrupt as err:
        logger.info('KeyBoard Interrupt')
    finally:
        io.delete_redis(ETL_BLOCK_SET)


if __name__ == '__main__':
    run()
