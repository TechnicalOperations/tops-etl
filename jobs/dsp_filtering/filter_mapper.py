mapper = {
    'request': {
        'mobileFilter': 'DSP Config',
        'SecureFilter': 'DSP Config',
        'adsizeInclude': 'Ad Size Filter',
        'adstxt': 'Ads.txt',
        'appIDBlacklist': 'White/Blacklist',
        'bannerFilter': 'DSP Config',
        'blacklist': 'White/Blacklist',
        'country': 'DSP Config',
        'countryInclude': 'DSP Config',
        'ctvFilter': 'DSP Config',
        'customRequestFilter': 'Custom Connector Filter',
        'desktopFilter': 'DSP Config',
        'deviceID': 'Device ID',
        'displayFilter': 'DSP Config',
        'dspMaxFloor': 'DSP Config',
        'dspMinTimeout': 'DSP Config',
        'freqCap': 'User Frequency Filter',
        'freqCapNoBuyerUID': 'User Frequency Filter',
        'ibvFilter': 'DSP Config',
        'iframe filter': 'DSP Config',
        'isAppFilter': 'DSP Config',
        'isSiteFilter': 'DSP Config',
        'nativeFilter': 'DSP Config',
        'pubIDBlacklist': 'White/Blacklist',
        'requireDeal': 'Require Deal',
        'rgSourceRating': 'RG Filter',
        'rgUserSuspicious': 'RG Filter',
        'siteIDBlacklist': 'White/Blacklist',
        'sspInclude': 'DSP Config',
        'sspPrivatePMP': 'DSP Config',
        'trafficMgmtListLowRPM': 'Traffic Performance',
        'trafficMgt': 'Traffic Performance',
        'trafficUserMatch': 'User Frequency Filter',
        'videoFilter': 'DSP Config',
        'videoLinearityFilter': 'DSP Config',
        'videoSizeInclude': 'Ad Size Filter',
        'whitelist': 'White/Blacklist',
        'notFiltered': 'notFiltered',
        'badRequest': 'badRequest',
        'vpaidFilter': 'DSP Config',
        'adsizeExclude': 'DSP Config'
    },
    'response': {
        'auction_loss': 'Lost Auction',
        'bidpricefloor': 'Price Below Bid Floor',
        'blockedadv': 'Blocked Attribute',
        'blockedattr': 'Blocked Attribute',
        'blockedcat': 'Blocked Attribute',
        'blockeddspseat': 'Blocked Attribute',
        'cridblacklist': 'White/Blacklist',
        'cridibv': 'Blocked Attribute',
        'cridwhitelist': 'White/Blacklist',
        'insecureAdm': 'Insecure ADM',
        'noadomainbundle': 'No Adomain',
        'vastinvalidVAST': 'Invalid Video',
        'vastinvalidXML': 'Invalid Video',
        'notFiltered': 'notFiltered',
        'dealbidfloor': 'Deal Attribute'
    },
    'impression': {
        'blacklist-domaindsp': 'White/Blacklist',
        'blacklist-domainglobal': 'White/Blacklist',
        'gmp-inview': 'Inview',
        'rg-block': 'RG Blocked',
        'unblocked': 'unblocked',
        'unknown': 'Unknown',
        'whitelist-domaindsp': 'White/Blacklist',
        'source:blocklist': 'RG Source Block',
        'source:blocklist,domains': 'RG Source Block',
        'user:ip': 'RG User Block',
        'source:blocklist|user:useragent': 'RG Source Block',
        'source:domains': 'RG Source Block',
        'source:domains|user:ip,useragent': 'RG Source Block',
        'source:domains|user:ip': 'RG Source Block',
        'user:ip,useragent': 'RG User Block',
        'source:blocklist|user:ip': 'RG Source Block',
        'source:domains|user:useragent': 'RG Source Block',
        'source:blocklist|user:ip,useragent': 'RG Source Block',
        'source:blocklist,domains|user:ip': 'RG Source Block',
        'user:useragent': 'RG User Block'
    }
}


def apply_mapper(data, state):
    if state == 'impression':
        data['filter_bucket'] = data['filter_reason'].apply(lambda x: mapper[state].get(x, x))
    else:
        data['filter_bucket'] = data['filter_reason'].apply(lambda x: mapper[state][x])
    return data


def filter_conditions(data, ssp_ids, pub_ids, placement_ids, site_names):
    if ssp_ids is not None and len(ssp_ids) > 0:
        data = data[data['ssp_id'].isin(ssp_ids)].reset_index(drop=True)
    if pub_ids is not None and len(pub_ids) > 0:
        data = data[data['pub_id'].isin(pub_ids)].reset_index(drop=True)
    else:
        data['pub_id'] = data['ssp_id'].apply(lambda x: 'all_{0}_pubs'.format(x))
    if placement_ids is not None and len(placement_ids) > 0:
        data = data[data['placement_id'].isin(placement_ids)].reset_index(drop=True)
    else:
        data['placement_id'] = data['ssp_id'].apply(lambda x: 'all_{0}_placements'.format(x))
    # if site_names is not None and len(site_names) > 0:
    #     data = data[data['site_name'].str.contains('|'.join(site_names))].reset_index(drop=True)
    data = data.groupby(list(data.columns)[:len(data.columns)-1]).sum().reset_index()
    return data
