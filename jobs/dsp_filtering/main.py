import pandas
from io import BytesIO
from etl.util import db_conn
from datetime import datetime
from datetime import timedelta
from etl.logger import get_logger
from jobs import decorators
from jobs.dsp_filtering import writer
from jobs.dsp_filtering import settings
from jobs.dsp_filtering import request_fan
from jobs.dsp_filtering import transformers
from jobs.dsp_filtering import filter_mapper
from jobs.dsp_filtering import notifier as filter_notifier


@decorators.validator_common
def param_validator(**kwargs):
    logger = get_logger(settings.SCRIPT_NAME)
    logger.info('Checking notifier object')
    if 'to' not in kwargs:
        return False, dict({'status': 'fail', 'message': 'Email Required'})
    if '@rhythmone.com' not in kwargs['to']:
        return False, dict({'status': 'fail', 'message': 'Only RhythmOne email addresses permitted'})
    if 'cc' in kwargs:
        if type(kwargs['cc']) == str:
            kwargs['cc'] = [i for i in kwargs['cc'].split(',') if i.strip() != '']
        kwargs['cc'] = [i for i in kwargs['cc'] if '@rhythmone.com' in i.strip()]
    logger.info('Checking pub and placement objects')
    if kwargs['pub_ids'] is None or kwargs['placement_ids'] is None:
        conn = db_conn.DbConnection('tops_mysql')
        query = "SELECT publisher, placement FROM rmp.ssp_pub"
        tmp = conn.get_frame(query, redis=False)
        kwargs['pub_ids'] = list()
        kwargs['placement_ids'] = list()
    if 'start_date' not in kwargs or 'end_date' not in kwargs\
            or kwargs['start_date'] is None or kwargs['end_date'] is None\
            or kwargs['start_date'] == '' or kwargs['end_date'] == '':
        kwargs['end_date'] = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
        kwargs['end_date'] = kwargs['end_date'] - timedelta(days=1)
        kwargs['start_date'] = kwargs['end_date'] - timedelta(days=7)
    if type(kwargs['pub_ids']) == str:
        kwargs['pub_ids'] = kwargs['pub_ids'].split(',')
    if type(kwargs['placement_ids']) == str:
        kwargs['placement_ids'] = kwargs['placement_ids'].split(',')
    if type(kwargs['ssp_ids']) == str:
        kwargs['ssp_ids'] = [i.strip() for i in kwargs['ssp_ids'].split(',') if i.strip() != '']
    elif type(kwargs['ssp_ids']) is None:
        kwargs['ssp_ids'] = []
    else:
        kwargs['ssp_ids'] = [str(i).strip() for i in kwargs['ssp_ids'] if str(i).strip() != '']
    kwargs['pub_ids'] = [i.strip() for i in kwargs['pub_ids'] if i.strip() != '']
    kwargs['placement_ids'] = [i.strip() for i in kwargs['placement_ids'] if i.strip() != '']
    return True, kwargs,


@decorators.job_wrapper
def run(
        pub_ids=None,
        placement_ids=None,
        start_date=datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=1),
        end_date=datetime.now().replace(hour=0, minute=0, second=0, microsecond=0),
        site_names=None,
        to=None,
        cc=[],
        ssp_ids=None,
        ticket='No Ticket',
        **kwargs
    ):
    logger = get_logger(settings.SCRIPT_NAME)
    logger.info("Connecting to snowflake")
    conn = db_conn.DbConnection('snowflake')

    logger.info("Fetching and transforming overview data")
    overview = transformers.overview_frame(ssp_ids, pub_ids, placement_ids, site_names, start_date, end_date)
    if overview is None:
        filter_notifier.send_report(
            None,
            to,
            *tuple(),
            ssp_ids=ssp_ids,
            pub_ids=pub_ids,
            placement_ids=placement_ids
        )
        return None

    logger.info("Fetching and transforming request details")
    requests_details = conn.get_frame_date_append(
        'dsp_filtering/requests',
        'date',
        start_date,
        end_date,
        start_date=start_date,
        end_date=end_date
    )
    requests_details = filter_mapper.filter_conditions(requests_details, ssp_ids, pub_ids, placement_ids, site_names)
    requests_details = filter_mapper.apply_mapper(requests_details, 'request')

    logger.info("Fetching and transforming response details")
    responses_details = conn.get_frame_date_append(
        'dsp_filtering/responses',
        'date',
        start_date,
        end_date,
        start_date=start_date,
        end_date=end_date
    )
    responses_details = filter_mapper.filter_conditions(responses_details, ssp_ids, pub_ids, placement_ids, site_names)
    responses_details = filter_mapper.apply_mapper(responses_details, 'response')

    logger.info("Fetching and transforming impression details")
    impression_details = conn.get_frame_date_append(
        'dsp_filtering/impressions',
        'date',
        start_date,
        end_date,
        start_date=start_date,
        end_date=end_date
    )
    impression_details = filter_mapper.filter_conditions(impression_details, ssp_ids, pub_ids, placement_ids, site_names)
    impression_details = filter_mapper.apply_mapper(impression_details, 'impression')

    logger.info("Creating excel file")
    excel_output = BytesIO()
    excel_writer = pandas.ExcelWriter(excel_output, engine='xlsxwriter')
    overview_out, summary, = writer.overview(excel_writer, overview)
    logger.info("Creating request details excel sheet")
    writer.step_details(excel_writer, overview_out, requests_details, 'Requests Details', 0, summary)
    if len(responses_details) > 0:
        logger.info("Creating response details excel sheet")
        writer.step_details(excel_writer, overview_out, responses_details, 'Responses Details', 1, summary)
    else:
        logger.info("Responses details page not created because of no responses")
    if len(impression_details) > 0:
        logger.info("Creating impression details excel sheet")
        writer.step_details(excel_writer, overview_out, impression_details, 'Impressions Details', 2, summary)
    else:
        logger.info("Impressions details page not created because of no impressions")

    logger.info("Creating RG Source Rating if data set small enough")
    request_fan.rg_source_rating(excel_writer, requests_details, start_date, end_date, site_names)

    logger.info("Creating Time Series Page for Requests")
    requests_details.to_excel(excel_writer, sheet_name='Request Time Series', index=False)

    if len(responses_details) > 0:
        logger.info("Creating Time Series Page for Responses")
        responses_details.to_excel(excel_writer, sheet_name='Response Time Series', index=False)

    if len(impression_details) > 0:
        logger.info("Creating Time Series Page for Impressions")
        impression_details.to_excel(excel_writer, sheet_name='Impression Time Series', index=False)

    excel_writer.save()
    excel_writer.close()
    excel_output.seek(0)

    if to is not None:
        logger.info("Sending out email notification")
        filter_notifier.send_report(
            excel_output,
            to,
            *tuple(cc),
            ssp_ids=ssp_ids,
            pub_ids=pub_ids,
            placement_ids=placement_ids,
            start_date=start_date,
            end_date=end_date,
            ticket=ticket
        )

    excel_output.seek(0)
    return excel_output.read()


if __name__ == '__main__':
    import os
    from jobs import arguments
    kwargs = arguments.create_arguments(settings)
    data = run(**kwargs)
    with open(os.path.expanduser('~') + '/Documents/data/dsp_filter_excel.xlsx', 'wb') as f:
        f.write(data)
