import json
from etl.util import db_conn
from etl.logger import  get_logger
from urllib import parse
from jobs.dsp_filtering import settings
from jobs.dsp_filtering import filter_mapper


def check_rmp_url(data):
    data = data[(data['app_id'] == 'unknown')][['ssp_id', 'pub_id', 'placement_id', 'page']]
    data['parsed'] = data['page'].apply(parse.urlparse)
    data = data[data['parsed'].apply(lambda x: x.netloc == 'tag.1rx.io')]
    data['rmp_params'] = data['parsed'].apply(lambda x: parse.parse_qs(x.query))
    del data['parsed']
    data['rmp_params'] = data['rmp_params'].apply(json.dumps)
    return data


def grapeshot(results):
    gs = results.get(
        'grapeshot',
        {'grapeshot_result': {'channels': []}}
    )
    gs = gs.get(
        'grapeshot_result',
        {'channels': []}
    )
    gs = [] if gs is None else gs.get('channels', [])
    if gs is None or len(gs) == 0:
        return 'Grapeshot: No Results'
    gv = [str(i['name']) for i in gs if i['name'][:3] == 'gv_']
    if len(gv) > 0:
        return 'Grapeshot Unsafe: {0}'.format(','.join(gv))
    else:
        return 'Valid GS Results'


def domain_blacklist(results):
    blacklist = results.get(
        'domains',
        {'blacklist': []}
    )['blacklist']
    if len(blacklist) == 0:
        return 'No Blacklist'
    else:
        return 'Blacklist hit: {0}'.format(','.join(blacklist))


def domain_whitelist(results):
    whitelist = results.get(
        'domains',
        {'whitelist': []}
    )['blacklist']
    if len(whitelist) == 0:
        return 'No Whitelist'
    else:
        return 'Whitelist hit: {0}'.format(','.join(whitelist))


def ip(results):
    ips = results.get('ip', [])
    if len(ips) == 0:
        return 'Valid IP'
    else:
        return 'Invalid IP: {0}'.format(','.join(ips))


def get_reason(row):
    if row['ip'] != 'Valid IP':
        return row['ip']
    if row['grapeshot'] != 'Valid GS Results':
        return row['grapeshot']
    if row['blacklist'] != 'No Blacklist':
        return row['blacklist']
    if row['whitelist'] != 'No Whitelist':
        return row['whitelist']
    return 'Valid Requests'


def rg_source_rating(excel_writer, details, start_date, end_date, site_names):
    # logger = get_logger(settings.SCRIPT_NAME)
    # if 'all_' in list(set(details['pub_id']))[0] or 'all_' in list(set(details['placement_id']))[0]:
    #     logger.info("Too many pubs to look for.  Returning without creating RG Source Rating sheet")
    #     return
    conn = db_conn.DbConnection('snowflake')
    data = conn.get_frame_date_append(
        'dsp_filtering/rg_source_rating',
        'date',
        start_date,
        end_date,
        start_date=start_date,
        end_date=end_date,
        # pubs="', '".join(set(details['pub_id'])),
        # placements="', '".join(set(details['placement_id']))
    )
    if 'all_' not in list(set(details['pub_id']))[0]:
        pubs = set(details['pub_id'])
    else:
        pubs = set(data['pub_id'])
    if 'all_' not in list(set(details['placement_id']))[0]:
        placs = set(details['placement_id'])
    else:
        placs = set(data['placement_id'])
    data = filter_mapper.filter_conditions(
        data, set(data['ssp_id']), pubs, placs, site_names
    )
    del data['date']
    data = data.groupby(list(data.columns)[:len(data.columns)-1]).sum().reset_index()
    # data['results'] = data['results'].apply(json.loads)
    # rmp_urls = check_rmp_url(data)
    # data = data.merge(rmp_urls, on=['ssp_id', 'pub_id', 'placement_id', 'page'], how='left')
    # data['grapeshot'] = data['results'].apply(grapeshot)
    # data['blacklist'] = data['results'].apply(domain_blacklist)
    # data['whitelist'] = data['results'].apply(domain_whitelist)
    # data['ip'] = data['results'].apply(ip)
    if len(data) == 0:
        return
    data['filter_reason'] = data[['grapeshot', 'blacklist', 'whitelist', 'ip']].apply(get_reason, axis=1)
    # del data['page']
    del data['checkset']
    # del data['results']
    del data['grapeshot']
    del data['blacklist']
    del data['whitelist']
    del data['ip']
    data = data.groupby(list(data.columns)[:len(data.columns)-1]).sum().reset_index()
    # count = data['count']
    # del data['count']
    # data['count'] = count
    # data['rmp_params'] = data['rmp_params'].fillna({})
    data.to_excel(excel_writer, sheet_name='RG Source Rating', index=False)
