import pandas
import numpy as np
from etl.util import db_conn
from datetime import datetime
from etl.logger import get_logger
from jobs.dsp_filtering import settings
from jobs.dsp_filtering import filter_mapper


def overview_transformer(data, top, column):
    data = data[~data[column].isnull()].reset_index(drop=True)
    del data['date']
    del top['date']
    # del data['site_name']
    # del top['site_name']
    data[column] = data[column].apply(lambda x: '% {0}'.format(x))
    adder = list({'ssp_id', 'pub_id', 'placement_id', 'dsp_id', column} ^ set(top.columns))[0]
    top = top.groupby(['ssp_id', 'pub_id', 'placement_id', 'dsp_id', column]).agg(
        {adder: np.sum}
    )
    index_len = len(top.index.names)
    top = top.groupby(level=[0, 1, 2, 3]).apply(lambda x: x.sort_values(by=adder, ascending=False)[:3])
    top = top.reset_index(level=[0, 1, 2, 3], drop=len(top.index.names) > index_len).reset_index()
    del top[adder]
    top = top.groupby(['ssp_id', 'pub_id', 'placement_id', 'dsp_id']).agg(lambda x: '\n'.join(list(x)))
    top.columns = pandas.MultiIndex.from_product([[adder], ['Top Reasons']])
    data = pandas.pivot_table(
        data,
        index=['ssp_id', 'pub_id', 'placement_id', 'dsp_id'],
        columns=[column],
        fill_value=0,
        aggfunc=np.sum
    )
    data.columns.names = [None for i in data.columns.names]
    # for column in data.columns:
    #     data[column] = data[column].fillna(0).astype(int)
    columns = list(data.columns)
    name = list(data.columns.levels[0])[0]
    if len(columns) > 1:
        data[(name, 'Total',)] = data[columns[0]] + data[columns[1]]
    else:
        data[(name, 'Total',)] = data[columns[0]]
    data = data[[(name, 'Total',)]+columns]
    # data[columns[0]] = data[columns[0]] * 100.0 / data[(name, 'Total',)]
    # data[columns[1]] = data[columns[1]] * 100.0 / data[(name, 'Total',)]
    data = data.merge(top, left_index=True, right_index=True, how='left')
    return data


def overview_frame(ssp_ids, pub_ids, placement_ids, site_names, start_date, end_date):
    logger = get_logger(settings.SCRIPT_NAME)
    conn = db_conn.DbConnection('snowflake')

    logger.info("Fetching requests overview")
    requests_overview = conn.get_frame_date_append(
        'dsp_filtering/requests',
        'date',
        start_date,
        end_date,
        start_date=start_date,
        end_date=end_date
    )
    logger.info("Formatting requests overview")
    requests_overview = filter_mapper.filter_conditions(requests_overview, ssp_ids, pub_ids, placement_ids, site_names)
    if len(requests_overview) == 0:
        return None
    requests_top = requests_overview.copy()
    requests_overview['filter_reason'] = requests_overview['filter_reason'].apply(
        lambda x: 'Requests Sent' if x == 'notFiltered' else 'Requests Filtered'
    )
    requests_overview = overview_transformer(requests_overview, requests_top, 'filter_reason')
    overview = requests_overview.copy()

    logger.info("Fetching responses overview")
    responses_overview = conn.get_frame_date_append(
        'dsp_filtering/responses',
        'date',
        start_date,
        end_date,
        start_date=start_date,
        end_date=end_date
    )
    responses_overview = filter_mapper.filter_conditions(responses_overview, ssp_ids, pub_ids, placement_ids, site_names)
    responses_top = responses_overview.copy()
    logger.info("Formatting responses overview")
    if len(responses_overview) > 0:
        responses_overview['filter_reason'] = responses_overview['filter_reason'].apply(
            lambda x: 'Bids Sent' if x == 'notFiltered' else 'Filtered'
        )
        filter_reasons = set(responses_overview['filter_reason'])
        if len(filter_reasons) == 1 and 'Bids Sent' in filter_reasons:
            row_tmp = responses_overview.iloc[0].copy()
            row_tmp['filter_reason'] = 'Filtered'
            row_tmp['bids'] = 0
            responses_overview = responses_overview.append(row_tmp)
        responses_overview = overview_transformer(responses_overview, responses_top, 'filter_reason')
        columns = [i for i in list(responses_overview.columns)]
        columns = [columns[0], columns[2], columns[1], columns[3]]
        responses_overview = responses_overview[columns]
        del columns
        overview = overview.merge(responses_overview, left_index=True, right_index=True, how='left')
    else:
        logger.info("Publisher had no responses.  Continuing to impressions")

    logger.info("Fetching impressions serve overview")
    impressions_overview = conn.get_frame_date_append(
        'dsp_filtering/impressions',
        'date',
        start_date,
        end_date,
        start_date=start_date,
        end_date=end_date
    )
    impressions_overview = filter_mapper.filter_conditions(impressions_overview, ssp_ids, pub_ids, placement_ids, site_names)
    impressions_top = impressions_overview.copy()
    logger.info("Formatting impressions overview")
    if len(impressions_overview) > 0:
        serve_overview = impressions_overview.copy()
        serve_top = impressions_top.copy()
        del serve_overview['billable_impressions']
        del serve_top['billable_impressions']
        serve_overview['filter_reason'] = serve_overview['filter_reason'].apply(
            lambda x: 'Pixels' if x == 'unblocked' else 'Blocked'
        )
        serve_overview = overview_transformer(serve_overview, serve_top, 'filter_reason')
        overview = overview.merge(serve_overview, left_index=True, right_index=True, how='left')

        pixel_overview = impressions_overview.copy()
        pixel_top = impressions_top.copy()
        del pixel_overview['impressions_serve']
        del pixel_top['impressions_serve']
        pixel_overview['filter_reason'] = pixel_overview['filter_reason'].apply(
            lambda x: 'Pixels' if x == 'unblocked' else 'Blocked'
        )
        pixel_overview = overview_transformer(pixel_overview, pixel_top, 'filter_reason')
        overview = overview.merge(pixel_overview, left_index=True, right_index=True, how='left')
    else:
        logger.info("Publisher had no impressions")

    logger.info("Creating and returning final overview dataframe")
    for column in overview.columns:
        try:
            overview[column] = overview[column].fillna(0).astype(int)
        except (TypeError, ValueError) as err:
            pass
    return overview
