from etl.notification import email_notifier


def send_report(excel_data, to, *cc, **params):
    ticket = params.pop('ticket', 'No Ticket')
    if excel_data is None:
        mailer = email_notifier.get_mailer_object('dsp_filtering/no_data')
        mailer.set_formatter(
            {
                'ssps': '' if params['ssp_ids'] is None else ', '.join(params['ssp_ids']),
                'pubs': '' if params['pub_ids'] is None else ', '.join(params['pub_ids']),
                'placements': '' if params['placement_ids'] is None else ', '.join(params['placement_ids'])
            }
        )
        mailer.send_mail('[{0}] DSP Filter Report'.format(ticket), to)
    else:
        file_name = 'traffic_performance_report_{0}-{1}.xlsx'.format(params['start_date'], params['end_date'])
        mailer = email_notifier.get_mailer_object('dsp_filtering/mailer')
        mailer.set_formatter(
            {
                'start_date': params['start_date'],
                'end_date': params['end_date'],
                'ssps': '' if params['ssp_ids'] is None else ', '.join(params['ssp_ids']),
                'pubs': '' if params['pub_ids'] is None else ', '.join(params['pub_ids']),
                'placements': '' if params['placement_ids'] is None else ', '.join(params['placement_ids'])
            }
        )
        mailer.attach_file(excel_data, file_name)
        mailer.send_mail('[{0}] DSP Filter Report'.format(ticket), to, cc=list(cc))
