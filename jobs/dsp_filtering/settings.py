from datetime import datetime
from datetime import timedelta


FREQUENCY = 'MANUALLY'

SCRIPT_NAME = 'nodsp_filters'

DISPLAY_NAME = 'Traffic Performance Report'

DESCRIPTION = 'Traffic Quality Report'

FIELD_DESCRIPTORS = [
    {
        'name': 'start_date',
        'type': 'datetime_str',
        'descriptor': 'Start date for data search',
        'required': True
    },
    {
        'name': 'end_date',
        'type': 'datetime_str',
        'descriptor': 'End date for data search',
        'required': True
    },
    {
        'name': 'ssp_ids',
        'type': 'str_array',
        'descriptor': 'List of SSPs to look for',
        'required': True
    },
    {
        'name': 'pub_ids',
        'type': 'str_array',
        'descriptor': 'List of publisher IDs to look for',
        'required': True
    },
    {
        'name': 'placement_ids',
        'type': 'str_array',
        'descriptor': 'List of placement IDs to look for',
        'required': True
    },
    {
        'name': 'to',
        'type': 'str',
        'required': True,
        'descriptor': 'Who to send the results to'
    },
    {
        'name': 'cc',
        'type': 'str_array',
        'required': False,
        'descriptor': 'Anyone to CC on the results'
    }
]

DEFAULT_PARAMS = {
    'to': None,
    'cc': [],
    'start_date': datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=1),
    'end_date': datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
}

IS_PUBLIC = True

TYPE = 'data_tools'
