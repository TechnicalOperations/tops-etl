import pandas
import numpy as np
from etl.logger import get_logger
from jobs.dsp_filtering import settings


OVERVIEW_SHEET_NAME = 'Overview'


def overview(excel_writer, raw_overview):
    sheet_name = OVERVIEW_SHEET_NAME
    # del data['date']
    # data = data.groupby(['ssp_id', 'pub_id', 'placement_id', 'dsp_id']).agg(
    #     lambda x: '\n'.join(set([z for i in x for z in i])) if 'Top Reasons' in x.name else np.sum(x)
    # )
    raw_figs = raw_overview.copy()
    for column in list(raw_overview.columns.levels[0]):
        for i in list(raw_overview[column].columns):
            if i == 'Total':
                continue
            try:
                raw_overview[(column, i,)] = raw_overview[(column, i,)] / raw_overview[(column, 'Total',)]
            except (TypeError, ValueError) as err:
                pass

    summary = raw_figs.copy()
    summary = summary.reset_index(level=3, drop=True)
    for column in list(summary.columns):
        if summary[column].dtype in [np.int64, np.int32]:
            continue
        summary[column] = summary[column].fillna('')
    summary = summary.groupby(level=[0, 1, 2]).agg(
        lambda x: np.sum(x)
        if x.dtype in [np.int64, np.int32]
        else '\r\n'.join(set([z.strip() for i in x if type(i) == str for z in i.split('\n') if z.strip() != '']))
    )
    for column in list(summary.columns.levels[0]):
        for i in list(summary[column].columns):
            if i == 'Total':
                continue
            try:
                summary[(column, i,)] = summary[(column, i,)] / summary[(column, 'Total',)]
            except (TypeError, ValueError) as err:
                pass

    raw_overview.to_excel(
        excel_writer,
        sheet_name=sheet_name,
        startrow=len(summary) + len(raw_overview.columns.levels) + len(summary.columns.levels)
    )

    # Same as above except with DSP figures aggregated as well
    summary.to_excel(
        excel_writer,
        sheet_name=sheet_name,
        startcol=1
    )

    sheet = excel_writer.sheets[sheet_name]
    book = excel_writer.book
    formatter = book.add_format()
    formatter.set_num_format('0.00%')
    sheet.set_column('F:G', None, formatter)
    sheet.set_column('J:K', None, formatter)
    sheet.set_column('N:O', None, formatter)
    sheet.set_column('R:S', None, formatter)
    return raw_overview, summary,


def get_group_percent(group, column):
    group['% total ssp/pub/placement'] = group[column] / group[column].sum()
    return group


def step_details(excel_writer, overview_data, details, sheet_name, offset, summary):
    logger = get_logger(settings.SCRIPT_NAME)
    logger.info("Creating the two tables (one including DSP and one not)")
    details = details.copy()
    del details['date']
    details['filter_reason'] = details['filter_reason'].apply(
        lambda x: '-' if x in ['unblocked', 'notFiltered'] else x
    )
    details = details.groupby(
        ['ssp_id', 'pub_id', 'placement_id', 'dsp_id', 'filter_bucket', 'filter_reason']
    ).sum()
    reasons = details.copy().reset_index()
    del reasons['dsp_id']
    del reasons['filter_reason']
    reasons = reasons.groupby(['ssp_id', 'pub_id', 'placement_id', 'filter_bucket']).sum()
    column = list(reasons.columns)[0]
    reasons['% total ssp/pub/placement'] = 0.0
    reasons = reasons.groupby(level=[0, 1, 2]).apply(lambda x: get_group_percent(x, column))
    reasons_out = reasons.sort_values(by=column, ascending=False).reset_index()

    details.reset_index().to_excel(
        excel_writer,
        sheet_name=sheet_name,
        startrow=len(reasons_out) + 3,
        index=False
    )

    reasons_out.sort_values(
        by=['ssp_id', 'pub_id', 'placement_id', '% total ssp/pub/placement'],
        ascending=[True, True, True, False]
    ).set_index(['ssp_id', 'pub_id', 'placement_id']).to_excel(
        excel_writer,
        sheet_name=sheet_name
    )

    sheet = excel_writer.sheets[sheet_name]
    book = excel_writer.book
    formatter = book.add_format()
    formatter.set_num_format('0.00%')
    i = 2
    for value in reasons_out['% total ssp/pub/placement']:
        sheet.write('F{0}'.format(i), value, formatter)
        i += 1
    i += 1
    sheet.write('A{0}'.format(i), 'Table to pivot')

    logger.info("Creating graphs for each pub")
    book = excel_writer.book
    sheet = excel_writer.sheets[sheet_name]
    idx_size = len(overview_data.index.names)
    # row, column (both 0 indexed)
    pubs = list(set(reasons.reset_index()['pub_id']))
    row_insert = 0
    column_insert = len(reasons.reset_index().columns) + 2
    total_offset = idx_size + 4 * offset + 1
    # length = len(overview_data) + 2
    init = len(summary) + len(summary.columns.levels) + len(overview_data.columns.levels) + 3
    for pub in pubs:
        group_length = len(overview_data[(overview_data.index.get_level_values(1) == pub)]) + init
        chart = book.add_chart({'type': 'column', 'subtype': 'stacked'})
        chart.add_series({
            'name': [OVERVIEW_SHEET_NAME, 1, total_offset + 1],
            'categories': [OVERVIEW_SHEET_NAME, init, 1, group_length - 1, idx_size - 1],
            'values': [
                OVERVIEW_SHEET_NAME, init, total_offset + 1, group_length - 1, total_offset + 1
            ],
            'gap': 2
        })
        chart.add_series({
            'name': [OVERVIEW_SHEET_NAME, 1, total_offset],
            'categories': [OVERVIEW_SHEET_NAME, init, 1, group_length - 1, idx_size - 1],
            'values': [
                OVERVIEW_SHEET_NAME, init, total_offset, group_length - 1, total_offset
            ],
            'gap': 2
        })
        chart.set_y_axis({'major_gridlines': {'visible': False}})
        chart.set_title({'name': pub})
        sheet.insert_chart(row_insert, column_insert, chart)
        row_insert += 3
        init = group_length
