DEFAULT_PARAMS = {}

DEPENDENCIES = []

DESCRIPTION = 'GDPR Consent String Parser'

DISPLAY_NAME = 'GDPR Consent String Parser'

FIELD_DESCRIPTORS = []

FREQUENCY = 'MANUALLY'

IS_PUBLIC = False

SCRIPT_NAME = 'gdpr_consent_string'

TYPE = 'data_tools'
