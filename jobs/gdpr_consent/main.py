import os
import json
import requests
from jobs import decorators
from etl.util import io
from etl.util import db_conn
from etl.logger import get_logger
from datetime import datetime
from base64 import urlsafe_b64decode
from base64 import urlsafe_b64encode
from jobs.gdpr_consent import settings


vendor_lists = io.get_redis('gdpr_consent/vendors')
vendor_lists = {} if vendor_lists is None else vendor_lists
logger = get_logger(settings.SCRIPT_NAME)


class ValueTracker:

    def __init__(self, array):
        self.array = array
        self.value = 0
        self.current = 0
        self.index = 0
        self.reset()

    def reset(self):
        self.value = 0
        self.current = 0
        self.index = 0

    def get_bits(self, count):
        iter_count = 0
        while self.current < count:
            self.value = (self.value << 8) + self.array[self.index]
            self.index += 1
            self.current += 8
            iter_count += 1
        diff = self.current - count
        out = int(self.value >> diff)
        self.current = diff
        if type(diff) == float:
            raise ValueError
        self.value = int(self.value) & int((2 ** diff) - 1)
        return out


def get_vendor_list(version):
    global vendor_lists
    try:
        default = vendor_lists['default']
    except KeyError as err:
        res = requests.get('https://vendorlist.consensu.org/vendorlist.json').text
        default = json.loads(res)
        vendor_lists['default'] = default
    url = 'https://vendorlist.consensu.org/v-{version}/vendorlist.json'
    try:
        return vendor_lists[version]
    except KeyError as err:
        res = requests.get(url.format(version=version)).text
        try:
            vendor_lists[version] = json.loads(res)
        except json.decoder.JSONDecodeError as j_err:
            vendor_lists[version] = default
        return vendor_lists[version]


def decode(x):
    if not isinstance(x, str) or x.lower() in [b'null', b'']:
        return None
    x = x.encode('utf-8')
    if len(x) < 10:
        return None
    f = False
    for i in range(8):
        try:
            x = urlsafe_b64decode(x)
            f = False
            break
        except:
            x += b'='
            f = True
    if f:
        return None
    else:
        return x


def encode(x):
    if not isinstance(x, bytes):
        return x
    x = urlsafe_b64encode(x)
    x = x.replace(b'=', b'')
    return x.decode('utf-8')


def parse(x):
    if not isinstance(x, bytes):
        return None
    if x == b'':
        return {}
    output = {}
    tracker = ValueTracker(x)
    try:
        output['version'] = tracker.get_bits(6)
        output['created'] = datetime.fromtimestamp(tracker.get_bits(36) / 10)
        output['updated'] = datetime.fromtimestamp(tracker.get_bits(36) / 10)
        output['cmp_id'] = tracker.get_bits(12)
        output['cmp_version'] = tracker.get_bits(12)
        output['consent_screen'] = tracker.get_bits(6)
        tmp = []
        tmp.append(int(tracker.get_bits(6)) + 65)
        tmp.append(int(tracker.get_bits(6)) + 65)
        output['consent_language'] = bytes(tmp).decode('utf-8')
        output['vendor_list_version'] = int(tracker.get_bits(12))
        tmp = int(tracker.get_bits(24))
        output['purposes_allowed'] = []
        exp = 2 ** 23
        pid = 1
        while exp > 0:
            if tmp & exp > 0:
                output['purposes_allowed'].append(pid)
            pid += 1
            exp = int(exp >> 1)
        output['max_vendor_id'] = int(tracker.get_bits(16))
        output['encoding_type'] = int(tracker.get_bits(1))
        if output['encoding_type'] == 0:
            output['bit_field'] = {}
            section = output['bit_field']
            section['field'] = []
            for i in range(output['max_vendor_id']):
                try:
                    tmp = tracker.get_bits(1)
                    if tmp > 0:
                        section['field'].append(i+1)
                except IndexError as err:
                    break
        else:
            output['range'] = {}
            section = output['range']
            section['default_consent'] = int(tracker.get_bits(1))
            entries = int(tracker.get_bits(12))
            section['entries'] = entries
            section['vendors'] = []
            for i in range(entries):
                tmp = {}
                tmp['single'] = int(tracker.get_bits(1))
                if tmp['single'] == 0:
                    tmp['vendor'] = int(tracker.get_bits(16))
                else:
                    tmp['start'] = int(tracker.get_bits(16))
                    tmp['end'] = int(tracker.get_bits(16))
                section['vendors'].append(tmp)
    except IndexError as err:
        if 'vendor_list_version' in output and get_vendor_list(output['vendor_list_version']) is None:
            logger.info('Index error when parsing \'{0}\'.  Failed at index {1} with parsed indices: {2}'.format(
                urlsafe_b64encode(x).decode('utf-8'),
                tracker.index,
                ', '.join(list(output.keys()))
            ))
            return None
        else:
            return None
    if 'vendor_list_version' not in output:
        logger.info('Parsed result has no vendor list version.  Setting parsed result to NULL')
        return None
    return output


def get_data():
    conn = db_conn.DbConnection('snowflake')
    data = conn.get_frame('gdpr_consent/strings')
    return data


def get_allowed_vendors(parsed):
    if not isinstance(parsed, dict):
        return None
    if 'bit_field' in parsed:
        return parsed['bit_field'].get('field', [])
    elif 'range' in parsed:
        try:
            default = parsed['range']['default_consent']
        except IndexError as err:
            return []
        finally:
            vendors = [
                [z for z in range(x['start'], x['end']+1, 1)] if x['single'] == 1 else [x['vendor']]
                for x in parsed['range']['vendors']
            ]
            vendors = set([z for i in vendors for z in i])
            try:
                if default == 1:
                    vendor_list = get_vendor_list(parsed['vendor_list_version'])
                    vendor_list = set(i['id'] for i in vendor_list['vendors'])
                    vendors = (vendor_list ^ vendors) & vendor_list
                return list(vendors)
            except (AttributeError, TypeError) as err:
                return []
    else:
        return []


@decorators.validator_common
def param_validator(**kwargs):
    return True, kwargs,


@decorators.job_wrapper
def run(**kwargs):
    global vendor_lists
    data = get_data()
    data['consent'] = data['consent'].apply(decode)
    data['parsed'] = data['consent'].apply(parse)
    data['consent'] = data['consent'].apply(encode)
    data['created'] = data['parsed'].apply(
        lambda x: x.get('created', 0) if isinstance(x, dict) else None
    )
    data['updated'] = data['parsed'].apply(
        lambda x: x.get('updated', 0) if isinstance(x, dict) else None
    )
    data['consent_language'] = data['parsed'].apply(
        lambda x: x.get('consent_language', '') if isinstance(x, dict) else None
    )
    data['consent_screen'] = data['parsed'].apply(
        lambda x: x['consent_screen'] if isinstance(x, dict) else -1
    )
    data['cmp_version'] = data['parsed'].apply(
        lambda x: x['cmp_version'] if isinstance(x, dict) else -1
    )
    data['cmp_id'] = data['parsed'].apply(
        lambda x: x['cmp_id'] if isinstance(x, dict) else -1
    )
    data['vendor_list_version'] = data['parsed'].apply(
        lambda x: x['vendor_list_version'] if isinstance(x, dict) else -1
    )
    data['purposes_allowed'] = data['parsed'].apply(
        lambda x: x.get('purposes_allowed', []) if isinstance(x, dict) else None
    )
    data['vendors_allowed'] = data['parsed'].apply(get_allowed_vendors)
    print(data.loc[0, 'parsed'])
    del data['parsed']
    print(data[:10])
    data = data[[
        'date',
        'ssp_id',
        'consent',
        'created',
        'updated',
        'consent_language',
        'consent_screen',
        'cmp_version',
        'cmp_id',
        'vendor_list_version',
        'purposes_allowed',
        'vendors_allowed',
        'unique_users',
        'requests'
    ]]
    io.to_redis(36000, **{'gdpr_consent/vendors': vendor_lists})
    return data


if __name__ == '__main__':
    import pandas
    pandas.set_option('display.width', 5000)
    output = run()
    output['purposes_allowed'] = output['purposes_allowed'].apply(json.dumps)
    output['vendors_allowed'] = output['vendors_allowed'].apply(json.dumps)
    output.to_excel(os.path.expanduser('~') + '/Documents/data/gdpr_consent.xlsx', index=False)
