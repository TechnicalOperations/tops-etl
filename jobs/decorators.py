import inspect
import traceback
from etl.util import io
from etl import monitoring
from etl.notification import slack_notifier


RUN_EXPIRE = 1209600


def _check_is_r1_or_not_email(v):
    v = v.split('@')
    if len(v) > 2 or len(v) == 1:
        return True
    if v[1] == 'rhythmone.com':
        return True
    return False


def _traverse_obj_email(kwargs):
    is_r1 = True
    if type(kwargs) == list:
        for item in kwargs:
            if type(item) == dict:
                is_r1 = _traverse_obj_email(item)
            if not is_r1:
                return is_r1
        return is_r1
    for k, v in kwargs.items():
        if type(v) == str:
            is_r1 = _check_is_r1_or_not_email(v)
        elif type(v) == dict:
            is_r1 = _traverse_obj_email(v)
        elif type(v) == list:
            is_r1 = _traverse_obj_email(v)
        if not is_r1:
            return is_r1
    return is_r1


def validator_common(func):
    def common(**kwargs):
        email_r1 = _traverse_obj_email(kwargs)
        if not email_r1:
            return False, {
                'status': 'fail',
                'message': 'Email addresses must only be RhythmOne emails'
            },
        return func(**kwargs)
    return common


def job_wrapper(func):
    def run(*status, **kwargs):
        try:
            ticket = kwargs.pop('ticket', None)
            expire = kwargs.pop('_expire', None)
            name = kwargs.pop('_name', None)
            time = kwargs.pop('_time', None)
            ticket = 'No Ticket' if ticket is None or ticket.strip() == '' else ticket.strip()
            if len(status) == 0:
                if name is not None:
                    script_name = name.split('/')[1]
                else:
                    script_name = 'manual'
                status = monitoring.Status(script_name, '127.0.0.1', **kwargs)
            elif len(status) == 1 and type(status[0]) == monitoring.Status:
                status = status[0]
            else:
                raise ValueError('More than one position argument present or argument wasn\'t type monitoring.Status')
            status.start()
            if 'ticket' in inspect.getfullargspec(func).args and ticket != 'No Ticket':
                kwargs['ticket'] = ticket
            output = func(**kwargs)
            status.success()
            if expire is not None and name is not None and time is not None:
                io.to_redis(expire, **{name: time})
            return output
        except Exception as err:
            trace = ''.join(traceback.format_exc())
            status.fail(trace)
            if name is None and name != 'manual':
                message = 'An error has occurred when processing a job.  Ticket: {0}'.format(ticket)
            else:
                message = 'An error has occurred when processing job \'{0}\', for ticket {1}:'.format(name, ticket)
            message += '\nFailed with parameters: {0}'.format(kwargs)
            slack_notifier.send_message(
                message,
                file_upload=trace,
                file_name='automation_err.txt'
            )
            raise err
        except KeyboardInterrupt as inter:
            status.fail('Keyboard Interrupt')
            raise inter
    return run
