FREQUENCY = 'MANUALLY'

SCRIPT_NAME = 'basic_queries'

DISPLAY_NAME = 'Some basic queries to run'

DESCRIPTION = 'Just a Bunch of Basic Queries'

FIELD_DESCRIPTORS = []

DEFAULT_PARAMS = {
    'queries': [
        {
            'query_name': 'basic/adstxt_update',
            'sheet_name': 'Ads.txt',
            'source': 'snowflake',
            'to': 'cbeecher@rhythmone.com',
            'cc': ['mvillaseca@rhythmone.com'],
            'title': 'Ads.txt Update'
        }
    ]
}

IS_PUBLIC = False
