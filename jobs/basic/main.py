from io import BytesIO
from etl.util import io
from etl.util import db_conn
from etl.logger import get_logger
from etl.notification import email_notifier
from jobs import decorators
from jobs.basic import settings


@decorators.validator_common
def param_validator(**kwargs):
    return True, kwargs,


@decorators.job_wrapper
def run(queries=settings.DEFAULT_PARAMS['queries']):
    logger = get_logger(settings.SCRIPT_NAME)
    logger.info('Processing {0} queries'.format(len(queries)))
    for query in queries:
        logger.info('Processing query \'{0}\''.format(query['query_name']))
        conn = db_conn.DbConnection(query['source'])
        data = conn.get_frame(query['query_name'])
        excel = BytesIO()
        io.to_excel(excel, **{query['sheet_name']: data})
        mailer = email_notifier.get_mailer_object('basic/template')
        mailer.set_formatter(
            {'query_used': io.get_query(query['query_name'])}
        )
        mailer.attach_file(excel, '{0}.xlsx'.format(query['sheet_name']))
        mailer.send_mail(query['title'], query['to'], cc=query['cc'])


if __name__ == '__main__':
    run()
