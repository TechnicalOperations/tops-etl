# These are the default settings for every job/process
# Every job module must contains a settings file, even if blank
# Overrides to settings are done at the module level
# Every job must have a file 'main.py' and within contain a function 'run()'

# FREQUENCY
# How often this report is to run.  The framework itself will run in increments of 1 hour
# Available values
# HOURLY
# DAILY
# WEEKLY
# MANUALLY
FREQUENCY = 'MANUALLY'

# DEPENDENCIES
# List of strings on other jobs/modules this module depends on (package level names)
DEPENDENCIES = []

# SCRIPT_NAME
# A way to associate a job with the module name as well as creating script locks when necessary (and logging)
SCRIPT_NAME = None

# DESCRIPTION
# Human readable description of what the job does
DESCRIPTION = None

# IS_PUBLIC
# Will this job be present as runnable in the UI?
IS_PUBLIC = False

# FIELD_DESCRIPTORS
# Description of each field in a jobs kwargs
# Structure: {
#   'name': '<kwarg_name>',
#   'type': '',
#   'required': <true or false>,  // Defaults to false
#   'default': <default_value>,  // Not required to include this, whether required=false or not
#   'descriptor': '<value>'  // Brief, human-readable, description of when the field is used for
#   'nested': [...]  // similar to base structure.  Only used with type 'json'
# }
# Current supported types blow:
# json
# array
# int_array
# float_array
# str_array
# datetime_str_array
# int
# float
# str
# datetime_str
# boolean
# csv - Used for long lists of input by a CSV (one column)
FIELD_DESCRIPTORS = []

# DEFAULT_PARAMS
# Default kwargs for run
DEFAULT_PARAMS = {}

# DISPLAY_NAME
# How the job is to be displayed in the UI
DISPLAY_NAME = None

# TYPE
# Default: data_tools
# Current types
# data_tools
# crawlers
TYPE = 'data_tools'
