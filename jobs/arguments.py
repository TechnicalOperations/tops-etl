import json
import argparse
from etl.util import io


def create_arguments(settings):
    parser = argparse.ArgumentParser()
    for field in settings.FIELD_DESCRIPTORS:
        f_type = field['type']
        name = field['name']
        descriptor = field['descriptor']
        default = settings.DEFAULT_PARAMS.get(name, None)
        arg = '--{0}'.format(name)
        if f_type in ['json', 'array', 'datetime_str']:
            if default is not None:
                default = str(default) if f_type == 'datetime_str' else json.dumps(default)
            parser.add_argument(arg, default=default, help=descriptor)
        elif f_type in ['float', 'float_array']:
            if f_type == 'float_array':
                parser.add_argument(arg, type=float, nargs='*', default=default, help=descriptor)
            else:
                parser.add_argument(arg, type=float, default=default, help=descriptor)
        elif f_type in ['int', 'int_array']:
            if f_type == 'int_array':
                parser.add_argument(arg, type=int, nargs='*', default=default, help=descriptor)
            else:
                parser.add_argument(arg, type=int, default=default, help=descriptor)
        elif f_type in ['str', 'str_array', 'csv']:
            if f_type in ['str_array', 'csv']:
                parser.add_argument(arg, type=str, nargs='*', default=default, help=descriptor)
            else:
                parser.add_argument(arg, type=str, default=default, help=descriptor)
        elif f_type == 'datetime_str_array':
            if default is not None:
                default = json.dumps([str(i) for i in default], help=descriptor)
            parser.add_argument(arg, default=default)
        elif f_type == 'boolean':
            parser.add_argument(arg, type=bool, default=default, help=descriptor)
        else:
            raise TypeError('Unsupported type \'{0}\' for \'{1}\''.format(f_type, name))
    args = parser.parse_args()
    kwargs = vars(args)
    for field in settings.FIELD_DESCRIPTORS:
        name = field['name']
        f_type = field['type']
        if kwargs[name] is None:
            continue
        if f_type == 'datetime_str':
            kwargs[name] = io.convert_datetime_string(kwargs[name])
        elif f_type == 'datetime_str_array':
            kwargs[name] = [io.convert_datetime_string(i) for i in json.loads(kwargs[name])]
        elif f_type == 'json':
            kwargs[name] = json.loads(kwargs[name])
    return kwargs
