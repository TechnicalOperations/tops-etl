
FREQUENCY = 'MANUALLY'

SCRIPT_NAME = 'rmpssp_update'

DISPLAY_NAME = 'RMP S2S Publisher Performances'

DESCRIPTION = 'RMP S2S Publisher Performances'

FIELD_DESCRIPTORS = []

DEFAULT_PARAMS = {
    'to': 'cbeecher@rhythmone.com',
    'cc': ['mvillaseca@rhythmone.com']
}

IS_PUBLIC = False
