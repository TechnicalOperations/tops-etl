import os
import pandas
import locale
import matplotlib
import numpy as np
matplotlib.use('Agg')
from io import BytesIO
from etl.util import io
from etl.util import db_conn
from etl.logger import get_logger
from etl.notification import email_notifier
from jobs import decorators
from jobs.dsp_filtering import settings
from datetime import datetime
from datetime import timedelta


try:
    locale.setlocale(locale.LC_ALL, 'en_US')
except locale.Error as err:
    try:
        locale.setlocale(locale.LC_ALL, 'en-US')
    except locale.Error as err:
        locale.setlocale(locale.LC_ALL, 'en_US.utf-8')


@decorators.validator_common
def param_validator(**kwargs):
    if 'to' not in kwargs:
        return False, kwargs,
    if kwargs.get('cc', None) is None:
        kwargs['cc'] = []
    return True, kwargs,


def fetch_data():
    logger = get_logger(settings.SCRIPT_NAME)
    days = 7
    start = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=days)
    start = datetime(start.year, start.month, start.day)
    end = start + timedelta(days=days)
    conn = db_conn.DbConnection('snowflake')
    tops_conn = db_conn.DbConnection('tops_mysql')

    logger.info('Getting publisher names and associated ssps from tops replica')
    pub_q = """
        SELECT *
        FROM rmp.ssp_pub
    """
    publishers = tops_conn.get_frame(pub_q)
    publishers['publisher'] = publishers['publisher'].astype(str)
    publishers['placement'] = publishers['placement'].astype(str)

    logger.info('Getting publisher stats from snowflake')
    data = conn.get_frame(
        'rmpssp_update/pub_stats',
        start_date=start,
        end_date=end
    )
    del data['ssp']
    data['requests'] = data['requests'].astype(int)

    logger.info('Formatting publisher frame to dictionary array')
    pubs = publishers[['publisher', 'publisher_name']].to_dict(orient='records')
    pubs = {i['publisher']: i['publisher_name'] for i in pubs}
    ssps = publishers[['publisher', 'ssp']].to_dict(orient='records')
    ssps = {i['publisher']: i['ssp'] for i in ssps}
    logger.info('Populating stats frame with human readable names')
    data['ssp'] = data['publisher'].apply(lambda x: ssps.get(x, 'other'))
    data['publisher'] = data['publisher'].apply(lambda x: pubs.get(x, ''))
    # data['placement'] = data['placement'].apply(lambda x: placs.get(x, ''))
    data = data[['date', 'ssp', 'publisher', 'user_matches', 'media_type', 'requests', 'responses', 'impression_pixels',
                 'revenue']]
    data['media_type'] = data['media_type'].fillna('unknown')
    data = data.fillna(0)
    return data, pubs,


def create_matchrate_frame(data):
    matches = data[['ssp', 'media_type', 'user_matches', 'requests']].copy()
    matches = pandas.pivot_table(matches, columns=['media_type'], index=['ssp'], fill_value=0, aggfunc=np.sum)
    matches = matches['user_matches'] * 100.0 / matches['requests']
    matches = matches.reset_index()
    matches.columns.name = None
    matches = matches.reset_index()[['ssp', 'site', 'app']].fillna(0)
    return matches


def create_publisher_template(data, pubs):
    templates = []
    ssps = list(set(data['ssp']))
    for ssp in ssps:
        tmp_pubs = [i for i in pubs if i[1] in set(data[(data['ssp'] == ssp)]['publisher'])]
        template = '<h2>{ssp}</h2><table>'.format(ssp=ssp[0].upper() + ssp[1:].lower()) + ''.join([
            "<tr>" + ''.join(
                ['<td><img src="{{ multiline:pub_{0} }}" /></td>'.format(z[0]) for z in tmp_pubs[i: i + 3]]) + "</tr>"
            for i in range(0, len(tmp_pubs), 3)
        ]) + '</table>'
        template = template.replace('{', '{{').replace('}', '}}')
        templates.append(template)
    template = '\n'.join(templates)
    return template


@decorators.job_wrapper
def run(to=None, cc=None):
    logger = get_logger(settings.SCRIPT_NAME)

    logger.info('Getting rmpssp publisher stats for the last 7 days')
    data, pubs,  = fetch_data()

    logger.info('Creating user match rates frame')
    matches = create_matchrate_frame(data)

    logger.info('Sorting publishers from highest responses to lowest')
    pubs = list([(k, v,) for k, v in pubs.items() if v in set(data['publisher'])])
    pubs.sort(key=lambda x: (
        0 - data[(data['ssp'] == data[(data['publisher'] == x[1])].iloc[0]['ssp'])]['responses'].sum(),
        0 - data[(data['publisher'] == x[1])]['responses'].sum(),)
              )

    logger.info('Creating publisher graph templates')
    template = create_publisher_template(data, pubs)

    logger.info('Creating mailer object')
    subs = {
        'pubs': template
    }
    m = email_notifier.get_mailer_object('rmpssp_update/template', subs=subs)

    # Everything below this line is for email creation.  Proceed with caution if you wish to keep your sanity
    m.make_graph('user_matches', matches, colors=['#16d1f0', '#fedd00'], title="User Match Rates", rot=-8)
    ssp_revenue = pandas.pivot_table(
        data[['date', 'ssp', 'revenue']],
        index=['date'],
        columns=['ssp'],
        aggfunc=np.sum,
        fill_value=0
    )
    ssp_revenue.columns = ssp_revenue.columns.droplevel(0)
    ssp_revenue = ssp_revenue.reset_index()
    m.make_graph(
        'ssp_revenue',
        ssp_revenue,
        title='Revenue By SSP - Last 7 Days'
    )

    logger.info('Creating user match graphs')
    matches = data[['date', 'media_type', 'user_matches', 'requests']].groupby(
        ['date', 'media_type']).sum().reset_index()
    matches['match_rate'] = matches['user_matches'] * 100.0 / matches['requests']
    matches = matches[['date', 'media_type', 'match_rate']]
    matches = pandas.pivot_table(matches, columns=['media_type'], index=['date'], fill_value=0, aggfunc=np.sum)
    matches.columns = matches.columns.droplevel(0)
    matches.columns.name = None
    matches = matches.reset_index()[['date', 'site', 'app']]
    m.make_graph('user_matches_4', matches, colors=['#16d1f0', '#fedd00'], title="Total Trend (7 Days)")

    matches = data[['publisher', 'user_matches', 'requests']].groupby('publisher').sum()
    matches = matches.sort_values(by='requests', ascending=False)[:5]
    matches = matches.reset_index()
    matches['User Match Rate'] = matches['user_matches'] * 100.0 / matches['requests']
    matches = matches[['publisher', 'User Match Rate']]
    m.make_graph('user_matches_pub', matches, colors=['#16d1f0', '#fedd00'], title="Match Rate of Top 5 Publishers",
                 fontsize=6, rot=-8)

    matches = data[data['publisher'].isin(matches['publisher'])][
        ['date', 'publisher', 'user_matches', 'requests']].groupby(['date', 'publisher']).sum()
    matches = matches.reset_index()
    matches['match_rate'] = matches['user_matches'] * 100.0 / matches['requests']
    matches = matches[['date', 'publisher', 'match_rate']]
    matches = pandas.pivot_table(matches, index=['date'], columns=['publisher'], fill_value=0, aggfunc=np.sum)
    matches.columns = matches.columns.droplevel(0)
    matches = matches.reset_index()
    matches.columns.name = None
    m.make_graph('user_matches_4_pub', matches, title="Match Rate of Top 5 Publishers (7 Days)")

    logger.info('Creating highest and lowest efficiency rate graphs')
    perf = data[['publisher', 'requests', 'impression_pixels']]
    perf = perf.groupby(['publisher']).sum()
    perf['imp_rate'] = perf['impression_pixels'] * 100.0 / perf['requests']
    del perf['impression_pixels']
    del perf['requests']
    perf = perf.reset_index()
    perf = perf.rename(columns={'imp_rate': 'Efficiency Rate'})
    perf = perf.sort_values(by='Efficiency Rate', ascending=True)
    m.make_graph('low_perf', perf[:4], title="Lowest Performing Publishers", fontsize=6, rot=-8)
    perf = perf.sort_values(by='Efficiency Rate', ascending=False)
    m.make_graph('high_perf', perf[:4], title="Highest Performing Publishers", fontsize=6, rot=-8)

    logger.info('Creating highest and lowest fill rate graphs')
    perf = data[['publisher', 'responses', 'impression_pixels', 'revenue']]
    perf = perf.groupby(['publisher']).sum()
    perf['imp_rate'] = perf['impression_pixels'] * 100.0 / perf['responses']
    del perf['impression_pixels']
    del perf['responses']
    perf = perf.reset_index()
    perf = perf.rename(columns={'imp_rate': 'Fill Rate'})
    perf = perf.sort_values(by='Fill Rate', ascending=True)
    for column in list(perf.columns)[1:]:
        perf[column] = perf[column].apply(lambda x: 0 if np.isinf(x) else x).fillna(0)
    m.make_graph('low_perf_res', perf[:4], title="Lowest Performing Publishers", fontsize=6, rot=-8)
    perf = perf.sort_values(by='Fill Rate', ascending=False)
    m.make_graph('high_perf_res', perf[:4], title="Highest Performing Publishers", fontsize=6, rot=-8)

    logger.info('Creating final publisher stats frame')
    pdata = data[['date', 'publisher', 'requests', 'responses', 'impression_pixels', 'revenue']]
    pdata = pdata.groupby(['date', 'publisher']).sum().reset_index()
    res_lim = (0, pdata['responses'].max(),)
    req_lim = (0, pdata['requests'].max(),)
    pdata = pandas.pivot_table(pdata, index=['date'], columns=['publisher'], fill_value=0, aggfunc=np.sum)
    pdata.columns = pdata.columns.swaplevel(0, 1)

    logger.info('Formatting publisher graphs into the template')
    for pub in pubs:
        name = pub[1]
        ptmp = pdata[pub[1]].copy().reset_index()
        pub = pub[0]
        m.make_graph('pub_{0}'.format(pub),
                     ptmp[['date', 'responses', 'impression_pixels']],
                     ptmp[['date', 'requests']],
                     "{0} - {1} - ${2}".format(
                         data[(data['publisher'] == name)].iloc[0]['ssp'],
                         name,
                         m.format_float(ptmp['revenue'].sum(), 0)
                     ),
                     shade_right=True,
                     left_y_lim=res_lim,
                     right_y_lim=req_lim)

    logger.info('Creating excel file and attaching it to the email')
    current_date = data['date'].max().strftime('%m/%d/%Y')
    excel_output = BytesIO()
    io.to_excel(excel_output, **{'RMP S2S Update': data})
    excel_output.seek(0)
    m.attach_file(excel_output, 'RMP_Server2Server_Stats_{0}.xlsx'.format(current_date.replace('/', '_')))
    m.set_formatter(formatter={
        'date': current_date
    })

    logger.info('Sending email')
    title = "S2S Status for {date} - ${revenue}".format(
        date=current_date,
        revenue=m.format_float(data[(data['date'] == data['date'].max())]['revenue'].sum(), 0)
    )
    m.send_mail(
        title,
        to,
        cc=list(cc)
    )


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--to')
    parser.add_argument('--cc', nargs='*', default=[])
    args = parser.parse_args()
    run(
        to=args.to,
        cc=args.cc
    )
