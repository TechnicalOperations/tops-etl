import os
from io import BytesIO
from etl.util import io
from etl.util import db_conn
from etl.logger import get_logger
from etl.notification import email_notifier
from jobs import decorators
from jobs.pmp_metrics import settings


@decorators.validator_common
def param_validator(**kwargs):
    if 'start_date' not in kwargs or 'end_date' not in kwargs:
        return False, {'status': 'fail', 'message': 'Both start and end date are required'},
    kwargs['start_date'] = io.convert_datetime_string(kwargs['start_date'])
    kwargs['end_date'] = io.convert_datetime_string(kwargs['end_date'])
    kwargs['deal_ids'] = kwargs['deal_ids'].split(',')
    kwargs['deal_ids'] = [i.strip() for i in kwargs['deal_ids'] if i.strip() != '']
    if len(kwargs['deal_ids']) == 0:
        return False, {'status': 'fail', 'message': 'At least one deal is required'},
    return True, kwargs,


@decorators.job_wrapper
def run(start_date=None, end_date=None, deal_ids=None, to=None, cc=[]):
    # logger = get_logger(settings.SCRIPT_NAME)
    # logger.info('Initiating script')
    conn = db_conn.DbConnection('snowflake')
    data = conn.get_frame(
        'pmp_metrics/metrics',
        start_date=start_date,
        end_date=end_date,
        deal_ids="', '".join(deal_ids)
    )
    output = BytesIO()
    io.to_excel(output, **{'results': data})
    output.seek(0)
    if to is not None:
        mailer = email_notifier.get_mailer_object('pmp_metrics/template')
        mailer.attach_file(output, 'results.xlsx')
        mailer.send_mail(
            'PMP Metrics',
            to,
            cc=cc
        )
    return output.read()


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--start_date', default='2018-06-27')
    parser.add_argument('--end_date', default='2018-06-28')
    parser.add_argument('--deal_ids', nargs='*', default=['1R-RX1-66918-20180516238833', '1R-RX1-20510-20180516125930'])
    args = parser.parse_args()
    excel = run(
        start_date=args.start_date,
        end_date=args.end_date,
        deal_ids=args.deal_ids
    )
    with open(os.path.expanduser('~') + '/Documents/data/results_deal.xlsx', 'wb') as f:
        f.write(excel)
