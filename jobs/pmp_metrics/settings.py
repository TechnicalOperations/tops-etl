from datetime import datetime
from datetime import timedelta


DEFAULT_PARAMS = {
    'start_date': datetime.utcnow().replace(day=1, hour=0, minute=0, second=0, microsecond=0),
    'end_date': datetime.utcnow().replace(day=1, hour=0, minute=0, second=0, microsecond=0) - timedelta(days=31),
    'deal_ids': []
}

DEPENDENCIES = []

DESCRIPTION = 'PMP Metrics'

FIELD_DESCRIPTORS = [
    {
        'name': 'start_date',
        'type': 'datetime_str',
        'descriptor': 'Start date for data search',
        'required': True
    },
    {
        'name': 'end_date',
        'type': 'datetime_str',
        'descriptor': 'End date for data search',
        'required': True
    },
    {
        'name': 'to',
        'type': 'str',
        'required': True,
        'descriptor': 'Who to send the results to'
    },
    {
        'name': 'cc',
        'type': 'str_array',
        'required': False,
        'descriptor': 'Anyone to CC on the results'
    },
    {
        'name': 'deal_ids',
        'type': 'str_array',
        'descriptor': 'List of Deal IDs to pull metrics for',
        'required': True
    }
]

FREQUENCY = 'MANUALLY'

IS_PUBLIC = True

SCRIPT_NAME = 'pmp_metrics'

DISPLAY_NAME = 'PMP Metrics'

TYPE = 'data_tools'
