import gzip
import json
import logging
from uuid import UUID
from PIL import Image
from time import sleep
from io import BytesIO
from etl.util import io
from etl.util import db_conn
from etl.logger import get_logger
from datetime import datetime
from jobs import arguments
from jobs import decorators
from jobs.pistachio import settings
from base64 import b64encode
from browsermobproxy import RemoteServer
from selenium.webdriver import Chrome
from selenium.webdriver import ChromeOptions
from jobs.pistachio import dbops
from jobs.pistachio import render_app
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.remote.remote_connection import LOGGER
LOGGER.setLevel(logging.WARNING)


MOD_EXP = 2 ** 128


@decorators.validator_common
def param_validator(**kwargs):
    return True, kwargs,


def get_adms():
    # Order or data tuple
    # crid, adm, banner_w, banner_h, bid_attr, bid_cat
    data = io.get_redis('pistachio/adms')
    if data is None:
        con = db_conn.connect('snowflake')
        cur = con.cursor()
        query = io.get_query('pistachio/get_adms')
        cur.execute(query)
        data = [i for i in cur.fetchall()]
        adms = {i[1]: i[5] for i in data}
        data = list(set([(i[1], i[3], i[4], adms[i[1]]) for i in data]))
        cur.close()
        db_conn.close_connections('snowflake')
        io.to_redis(3600, **{'pistachio/adms': data})
    return data


def crop_adm(driver, crid, render_outputs):
    img_bytes = BytesIO()
    img_bytes.write(driver.get_screenshot_as_png())
    element = driver.find_element_by_id('adm-wrapper')
    location = element.location
    size = element.size
    left = location['x']
    top = location['y']
    right = location['x'] + size['width']
    bottom = location['y'] + size['height']
    img = Image.open(img_bytes)
    img = img.crop((left, top, right, bottom))
    img_bytes = BytesIO()
    img.save(img_bytes, format='PNG')
    img_bytes.seek(0)
    img = gzip.compress(img_bytes.read())
    img = b64encode(img).decode('utf-8')
    render_outputs.append(
        {
            'render': img,
            'size':
                {
                    'width': size['width'],
                    'height': size['height']
                }
        }
    )


# selenium renderer, may require more functions for better containment
def render(proxy, profile, url, adms, attempts=3, wait=3):
    output = {}
    attr = None
    cat = None
    for crid, w, h, adm in adms:
        proxy.new_har(crid)
        driver = Chrome(chrome_options=profile)
        driver.set_page_load_timeout(10)
        driver.set_script_timeout(10)
        try:
            cat = json.loads(cat) if cat is not None else None
        except json.decoder.JSONDecodeError as err:
            cat = [cat] if type(cat) == str else None
        output[crid] = {
            'uuid': str(UUID(int=hash((crid, adm,)) % MOD_EXP)),
            'size': {'width': w, 'height': h},
            'renders': [],
            'har': None,
            'attr': json.loads(attr) if attr is not None else None,
            'cat': cat,
            'adm': adm,
            'date': str(datetime.utcnow())
        }
        try:
            for i in range(attempts):
                try:
                    driver.get(url.format(crid=crid))
                    sleep(wait)
                    crop_adm(driver, crid, output[crid]['renders'])
                except (TimeoutException, WebDriverException) as err:
                    continue
        except SystemError as err:
            pass
        output[crid]['har'] = proxy.har
        driver.stop_client()
        driver.close()
    return output


@decorators.job_wrapper
def run(ip='127.0.0.1', port=5000):
    logger = get_logger(settings.SCRIPT_NAME)
    logger.info('Starting rendering server')
    render_app.run_server(ip, port)
    logger.info('Fetching ADMs')
    adms = get_adms()
    logger.info('Storing ADMs in shared queue')
    for adm in adms:
        render_app.adm_q.put(adm)
    logger.info('Connecting to proxy server')
    server = RemoteServer('127.0.0.1', 8080)
    logger.info('Creating proxy connection')
    proxy = server.create_proxy()
    logger.info('Starting web driver')
    profile = ChromeOptions()  # FirefoxProfile(os.environ['FIREFOXDRIVER'])
    profile.add_argument('--proxy-server={0}'.format(proxy.proxy))
    profile.add_argument('--headless')
    profile.add_argument('--no-sandbox')
    logger.info('Starting web renders')
    results = render(
        proxy,
        profile,
        'http://{ip}:{port}/adm_render/?crid={{crid}}'.format(ip=ip, port=port),
        adms
    )
    logger.info('Closing proxy')
    proxy.close()
    logger.info('Storing results in DB')
    render_app.adms = dict()
    api = dbops.PistachioApi()
    api.insert_new_scans(results)
    api.close()
    logger.info('Exiting script')
    return results


if __name__ == '__main__':
    kwargs = arguments.create_arguments(settings)
    adm_results = run(**kwargs)
