import os
import json
from io import BytesIO
from flask import abort
from flask import Flask
from flask import request
from flask import jsonify
from flask import Response
from flask import send_file
from jobs.pistachio import dbops
from jobs.pistachio import parse


app = Flask('adm_summary')
api = dbops.PistachioApi()


with open(os.environ['PISTACHIO_STATIC'] + '/webui/pistachio.html', 'r') as f:
    summary_html = f.read()


static = {
    'css': {},
    'img': {},
    'js': {}
}

for key in static.keys():
    path = os.environ['PISTACHIO_STATIC'] + '/webui/'+key+'/'
    fs = os.listdir(path)
    for f in fs:
        with open(path+f, 'rb') as h:
            if key == 'img':
                static[key][f] = h.read()
            else:
                static[key][f] = h.read().decode('utf-8')


@app.route('/', methods=['GET', 'POST'], defaults={'path': ''})
@app.route('/<path:path>')
def summary(path):
    if request.method == 'GET':
        if path == '':
            return summary_html
        try:
            if 'css/' in path or 'js/' in path:
                path = path.split('/')
                return Response(static[path[0]][path[1]], mimetype='text/css' if 'css' in path else 'application/x-javascript')
            else:
                path = path.split('/')
                if 'png' in path[1]:
                    return send_file(BytesIO(static[path[0]][path[1]]), mimetype='image/png')
                elif 'jpg' in path[1]:
                    return send_file(BytesIO(static[path[0]][path[1]]), mimetype='image/jpeg')
        except (IndexError, KeyError) as err:
            print('=== {0} ==='.format(path))
            return abort(404)
    if 'crid' in request.form:
        results = api.get_uuids(request.form['crid'])
    else:
        results = api.get_crids()
    return jsonify(results)


@app.route('/har/', methods=['POST'])
def get_har():
    if 'uuid' not in request.form:
        return abort(404)
    uuid = request.form['uuid']
    results = api.get_raw_results(uuid)
    results = results[0]
    results['har'] = parse.har_extractor(results['har'])
    return Response(json.dumps(results), mimetype='application/json')


@app.route('/image/', methods=['GET'])
def creative_image():
    uuid = request.args.get('uuid')
    page = request.args.get('page')
    imgs = api.get_image_results(uuid, page)
    if len(imgs) == 0:
        return '', 204,
    else:
        return send_file(imgs[0], mimetype='image/png')


if __name__ == '__main__':
    app.run('10.7.0.139', 8080)
