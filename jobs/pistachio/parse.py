from jobs.pistachio import dbops


def har_extractor(har):
    har = har['log']['entries']
    har = [
        {
            'method': h['request']['method'],
            'url': h['request']['url'],
            'status': h['response']['status'],
            'mimeType': h['response']['content']['mimeType'],
            'responseSize': h['response']['bodySize'],
            'time': h['time']
        }
        for h in har
    ]
    return har
