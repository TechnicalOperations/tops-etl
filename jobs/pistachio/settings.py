DEFAULT_PARAMS = {}

DEPENDENCIES = []

DESCRIPTION = 'Creative Scanner'

DISPLAY_NAME = 'Creative Scanner'

FIELD_DESCRIPTORS = []

FREQUENCY = 'MANUALLY'

IS_PUBLIC = False

SCRIPT_NAME = 'pistachio'

TYPE = 'crawlers'
