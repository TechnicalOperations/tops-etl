import os
import json
import gzip
from io import BytesIO
from etl.util import io
from etl.util import db_conn
from base64 import b64decode


class PistachioApi:

    @staticmethod
    def connect():
        return db_conn.connect('tops_mysql')

    def __init__(self):
        self.con = PistachioApi.connect()

    def insert_new_scans(self, results):
        # Entry: {crid: {'uuid':x, 'size': {'width': x, 'height': x}, 'renders': [{'img': x, 'size': {'width': x, 'height': x}], 'har': x, 'attr': x, 'cat': x}
        if self.con.open == 0:
            self.con = PistachioApi.connect()
        data = []
        har = []
        render = []
        for crid, row in results.items():
            cat = row['cat']
            cat = ','.join([str(i) for i in cat]) if cat is not None else None
            attr = row['attr']
            attr = ','.join([str(i) for i in attr]) if attr is not None else None
            data.append(
                (row['date'], row['uuid'], crid, cat, attr, row['size']['width'], row['size']['height'], row['adm'],)
            )
            har.append(
                (row['uuid'], json.dumps(row['har']),)
            )
            render += [
                (row['uuid'], row['renders'][i]['render'], i,)
                for i in range(len(row['renders']))
            ]
        cur = self.con.cursor()
        io.to_redis(3600, **{'data': data})
        cur.executemany("""
            INSERT INTO pistachio.data (date, uuid, crid, cat, attr, width, height, adm)
            VALUES
            (%s, %s, %s, %s, %s, %s, %s, %s)
        """, data)
        self.con.commit()
        cur.executemany("""
            INSERT INTO pistachio.log (uuid, har_content)
            VALUES
            (%s, %s)
        """, har)
        self.con.commit()
        cur.executemany("""
            INSERT INTO pistachio.render (uuid, base64_render, group_id)
            VALUES
            (%s, %s, %s)
        """, render)
        self.con.commit()
        cur.close()

    def get_raw_results(self, uuid):
        if self.con.open == 0:
            self.con = PistachioApi.connect()
        q = io.get_query('pistachio/fetch_raw_results')
        cur = self.con.cursor()
        cur.execute(q.format(uuid=uuid))
        data = cur.fetchall()
        results = {}
        for uuid, crid, cat, attr, width, height, adm, har in data:
            results[uuid] = {
                'cat': cat,
                'attr': attr,
                'size': {'width': width, 'height': height},
                'adm': adm,
                'har': json.loads(har)
            }
        results = list(results.values())
        return results

    def get_image_results(self, uuid, page):
        if self.con.open == 0:
            self.con = PistachioApi.connect()
        q = io.get_query('pistachio/get_images')
        cur = self.con.cursor()
        cur.execute(q)
        imgs = cur.fetchall()
        cur.close()
        imgs = [BytesIO(gzip.decompress(b64decode(i[0]))) for i in imgs]
        for img in imgs:
            img.seek(0)
        return imgs

    def get_crids(self):
        if self.con.open == 0:
            self.con = PistachioApi.connect()
        q = io.get_query('pistachio/fetch_crids')
        cur = self.con.cursor()
        cur.execute(q)
        crids = cur.fetchall()
        cur.close()
        tmp = {}
        for crid, uuid in crids:
            if crid not in tmp:
                tmp[crid] = []
            tmp[crid].append(uuid)
        return tmp

    def get_uuids(self, crid):
        if self.con.open == 0:
            self.con = PistachioApi.connect()
        q = io.get_query('pistachio/fetch_uuid')
        cur = self.con.cursor()
        cur.execute(q.format(crid=crid))
        uuids = cur.fetchall()
        uuids = [i[0] for i in uuids]
        return uuids


    # def get_scan_results(self, crid):
    #     if self.con.open == 0:
    #         self.con = PistachioApi.connect()
    #     cur = self.con.cursor()

    def close(self):
        try:
            db_conn.close_connections('tops_mysql')
        except KeyError as err:
            pass

    def __del__(self):
        try:
            db_conn.close_connections('tops_mysql')
        except KeyError as err:
            pass
