import os
import queue
import threading
from flask import Flask
from flask import request


FILE_PATH = os.path.dirname(os.path.realpath(__file__))
app = Flask('pistachio_http')
adms = dict()
adm_q = queue.Queue()
with open(FILE_PATH + '/adm_wrapper.html', 'r') as f:
    adm_wrapper = f.read()


@app.route('/adm_render/', methods=['GET'])
def adm_render():
    global adms
    return adm_wrapper.format(adm=adms[request.args.get('crid')])


def build_adm_list():
    global adm_q
    while True:
        adm = adm_q.get()
        adms[adm[0]] = adm[3]
        adm_q.task_done()


def run_server(host, port):
    server_thread = threading.Thread(target=app.run, args=(host, port,))
    server_thread.daemon = True
    server_thread.start()
    adm_builder = threading.Thread(target=build_adm_list)
    adm_builder.daemon = True
    adm_builder.start()
