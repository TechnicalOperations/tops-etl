#!/bin/python3

from browsermobproxy import RemoteServer
from selenium import webdriver
from pyvirtualdisplay import Display
import json
import ast
import subprocess, signal
import os
import re
import _thread
from urllib.parse import urlparse
import queue
import uuid
from etl.logger import get_logger
from jobs.demand_scan import settings


class AdScan:

	def __init__(self):
		self.display = None
		self.proxy = None
		self.driver = None
		self.regex = None
		self.domain_info = {}
		self.logger = get_logger(settings.SCRIPT_NAME)

	def setup(self):
		def display_setup():
			self.logger.info("Starting display bypass...")
			try:
				self.display = Display(visible=0, size=(800, 1000))
				self.display.start()
				self.logger.info("Bypass active")
			except Exception as e:
				self.logger.error(e)


		def server_setup():
			'''
			Start browsermob proxy server
			Used to collect network call information
			:return: Proxy object
			'''
			self.logger.info("Starting network proxy...")
			try:
				server = RemoteServer("127.0.0.1", 8080)
				self.proxy = server.create_proxy()
				self.logger.info("Network proxy started")
			except Exception as e:
				self.logger.error(e)
				raise e
			return


		def browser_setup():
			'''
			Create Firefox webdriver
			Presets User Agent string
			Disregards SSL warnings
			:return: Driver object
			'''
			self.logger.info("Starting WebDriver instance...")
			try:
				chrome_options = webdriver.ChromeOptions()
				chrome_options.add_argument("--proxy-server={0}".format(self.proxy.proxy))
				self.driver = webdriver.Chrome(chrome_options = chrome_options)

				self.driver.set_page_load_timeout(10)
				self.logger.info("WebDriver started")
			except Exception as e:
				self.logger.error(e)
			return

		display_setup()
		server_setup()
		browser_setup()
		return


	def load_regex(self, file):
		'''
		Reads advertiser JSON
		:return: Parsed JSON advertiser data
		'''
		self.logger.info("Loading regex: {}".format(file))
		try:
			f = open(file, 'r')
			ads = f.read()
			self.regex = json.loads(ads)
			self.logger.info("Regex loaded")
		except Exception as e:
			self.logger.error(e)
		return



	def analyze(self, domain):
		self.domain_info = {}
		def regex_url(pos, q, urls):
			'''
			Pattern matcher for ads.json and network call flows
			:param pos: Thread ID
			:param q: Queue object for returning data
			:param urls: Network call flow list
			:return:
			'''
			single_map = self.regex[str(pos)]
			single_map_name = single_map['name']
			single_map_cat = single_map['cat']
			single_map_regex = single_map['regex']
			ad_dict = {}
			for i in urls:
				for j in single_map_regex:
					m = re.search("{}".format(j), i)
					if m:
						ad_dict['name'] = single_map_name
						ad_dict['cat'] = single_map_cat
						beacon = urlparse(i)
						beacon_url = "{0}://{1}".format(beacon.scheme, beacon.netloc)
						ad_dict['beacon'] = beacon_url
						ad_dict['status'] = True
						if beacon.scheme == "https":
							ad_dict['secure'] = True
						else:
							ad_dict['secure'] = False
						q.put(ad_dict)
						return
			q.put(None)
			return

		def regex_map(urls):
			'''
			Builds thread map for regex comparison
			:param urls: Array of URLs seen in network flows
			:return: Array of sorted regex matches
			'''
			adv = []
			self.logger.info("Building thread pool")
			thread_count = len(self.regex)
			self.logger.info("Threads: {}".format(thread_count))
			self.logger.info("Parsing regex maps")

			d = []
			threads = []
			q = queue.Queue()
			for i in range(0, thread_count):
				t = _thread.start_new_thread(regex_url, (i, q, urls))
				threads.append(t)

			for i in threads:
				item = q.get()
				d.append(item)

			d = list(filter(None.__ne__, d))
			return d

		def task_id():
			'''
			Random alphanumeric job ID
			:return: 16 character alphanumeric string
			'''
			return (str(uuid.uuid4()).replace("-",""))[0:16]

		self.logger.info("Targetting: {}".format(domain))
		domain_fqdn = "http://{}".format(domain)
		self.proxy.new_har(domain_fqdn, options={'captureHeaders': True})
		self.logger.info("Capturing 10s of traffic...")
		try:
			self.driver.get(domain_fqdn)
		except Exception as e:
			pass

		result = json.dumps(self.proxy.har, ensure_ascii=False)
		result = ast.literal_eval(result)
		self.logger.info("Capture complete: {0} results".format(len(result)))
		x = 0
		a = []
		for i in result['log']['entries']:
			a.append(i['request']['url'])
			x += 1
		self.logger.info("Network calls: {}".format(x))
		adv = regex_map(a)
		d = {}
		d['domain'] = domain
		d['ads'] = adv
		self.domain_info[task_id()] = d
		return



	def kill_proxy(self):
		'''
		Searches and kills browsermob proxy PIDs
		Due to known bug in server.stop() and proxy.close() methods
		:return:
		'''
		p = subprocess.Popen(['ps', 'a'], stdout=subprocess.PIPE)
		out, err = p.communicate()
		for line in out.splitlines():
			if 'browsermob' in str(line):
				self.logger.info("Killing network proxy PID")
				pid = int(line.split(None, 1)[0])
				os.kill(pid, signal.SIGKILL)
				self.logger.info("PID killed")

	def kill_driver(self):
		'''
		Terminates Selenium WebDriver
		:return:
		'''
		self.logger.info("Killing WebDriver instance")
		self.driver.close()
		self.driver.quit()
		self.logger.info("WebDriver killed")
		return

	def kill_display(self):
		'''
		Terminates VirtualDisplay Bypass
		:return:
		'''
		if self.display is not None:
			self.logger.info("Killing Display bypass")
			self.display.stop()
			self.logger.info("Bypass killed")
		else:
			self.logger.info('Virtual display never created')
		return

	def shutdown(self):
		#self.kill_proxy()
		self.kill_driver()
		self.kill_display()
