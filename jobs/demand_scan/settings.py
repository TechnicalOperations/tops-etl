DEFAULT_PARAMS = {
        'domain': None,
        'to': None,
        'cc': []

}

DESCRIPTION = 'Publisher Demand Scanner'

DISPLAY_NAME = 'Pub Demand Scanner'

FIELD_DESCRIPTORS = [
    {
        'name': 'domain',
        'type': 'csv',
        'descriptor': 'Array of domains  to scan',
        'required': True
    },
    {
        'name': 'to',
        'type': 'str',
        'required': True,
        'descriptor': 'Who to send the results to'
    },
    {
        'name': 'cc',
        'type': 'str_array',
        'required': False,
        'descriptor': 'Anyone to CC on the results'
    }
]

FREQUENCY = 'MANUALLY'

IS_PUBLIC = True

SCRIPT_NAME = 'demand_scan'

TYPE = 'crawlers'
