from jobs import decorators
from jobs.demand_scan.ad_scanner import AdScan
from etl.logger import get_logger
from jobs.demand_scan import settings
from jobs.demand_scan import excel
from jobs.demand_scan import notifier as email_notifier
from jobs import arguments
import re


@decorators.validator_common
def param_validator(**kwargs):
	logger = get_logger(settings.SCRIPT_NAME)

	logger.info('Checking notifier object')
	if 'to' not in kwargs:
		return False, dict({'status': 'fail', 'message': 'Email Required'})
	if '@rhythmone.com' not in kwargs['to']:
		return False, dict({'status': 'fail', 'message': 'Only RhythmOne email addresses permitted'})
	if 'cc' in kwargs:
		if type(kwargs['cc']) == str:
			kwargs['cc'] = [i for i in kwargs['cc'].split(',') if i.strip() != '']
		kwargs['cc'] = [i for i in kwargs['cc'] if '@rhythmone.com' in i.strip()]

	logger.info('Checking domain object')
	if 'domain' not in kwargs:
		return False, dict({'status': 'fail', 'message': 'Domain Required'})
	regex = re.compile(
		r'^(?:http|ftp)s?://'
		r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'
		r'localhost|'
		r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'
		r'(?::\d+)?'
		r'(?:/?|[/?]\S+)$', re.IGNORECASE)
	valid_domains = []
	if isinstance(kwargs['domain'], str):
		kwargs['domain'] = kwargs['domain'].split('\n')
		kwargs['domain'] = [i.strip() for i in kwargs['domain'] if i.strip() != '']
	for domain in kwargs['domain']:
		if (re.match(regex, "{0}{1}".format("http://", domain))) is not None:
			valid_domains.append(domain)
	if len(valid_domains) == 0:
		return False, dict({'status': 'fail', 'message': 'Invalid Domain'})

	kwargs['domain'] = valid_domains
	return True, kwargs,


@decorators.job_wrapper
def run(
		domain=None,
		to=None,
		cc=[]
	):
	logger = get_logger(settings.SCRIPT_NAME)
	logger.info("Creating AdScan instance")
	scanner = AdScan()
	scanner.setup()

	logger.info("Loading regex")
	scanner.load_regex("jobs/demand_scan/ads.json")

	writer = excel.Writer()
	logger.info("Beginning analysis")
	for d in domain:
		logger.info('Analyzing {0}'.format(d))
		scanner.analyze(d)

		logger.info("Sending data to Excel writer")
		writer.write(scanner.domain_info)

	output, filename = writer.get_output()

	logger.info("Sending data to email notifier")
	email_notifier.send_report(output, filename, to, *tuple())

	logger.info("Shutting down WebDriver and Display Bypass")
	scanner.shutdown()

	# logger.info("Calling for file removal")
	# excel.remove(xlsx_doc)


if __name__ == '__main__':
	kwargs = arguments.create_arguments(settings)
	run(**kwargs)
