from io import BytesIO
from etl.logger import get_logger
from jobs.demand_scan import settings
import xlsxwriter
import uuid
import os


class Writer:

    def __init__(self):
        logger = get_logger(settings.SCRIPT_NAME)
        self.output = BytesIO()
        self.filename = "{0}-{1}.xlsx".format(settings.SCRIPT_NAME,str(uuid.uuid4().hex)[:8])
        logger.info("Creating workbook: {}".format(self.filename))
        self.workbook = xlsxwriter.Workbook(self.output, {'strings_to_urls': False})
        self.worksheet = self.workbook.add_worksheet()
        header = ['Domain', 'Advertiser', 'Beacon']
        header_format = self.workbook.add_format({'bold': True})
        self.h_col = 0
        for i in header:
            self.worksheet.write(0, self.h_col, i, header_format)
            self.h_col += 1
        self.row = 1

    def write(self, data):
        logger = get_logger(settings.SCRIPT_NAME)

        logger.info("Writing xlsx data")
        try:
            for i in data:
                domain = data[i]['domain']
                self.worksheet.write(self.row, 0, domain)

                ads = data[i]['ads']
                for j in ads:
                    self.worksheet.write(self.row, 1, j['name'])
                    self.worksheet.write(self.row, 2, j['beacon'])
                    self.row += 1
                self.row += 1
        except Exception as e:
            logger.error(e)
            raise e

    def get_output(self):
        self.workbook.close()
        return self.output, self.filename,


def remove(filename):
    logger = get_logger(settings.SCRIPT_NAME)
    dir_path = os.path.dirname(os.path.realpath(__file__))
    if (os.path.isfile("{0}/{1}".format(dir_path, filename))):
        logger.info("Removing Excel doc")
        try:
            os.remove("{0}{1}".format(dir_path, filename))
            logger.info("Excel doc removed")
        except Exception as e:
            logger.error(e)
    return
