import json
import socket
from io import BytesIO
from etl.util import io
from etl.util import db_conn
from etl.logger import get_logger
from etl.notification import email_notifier
from datetime import datetime
from datetime import timedelta
from jobs import decorators
from jobs.avails import settings


def check_protocols(x):
    final = []
    if x == 'vast':
        final += [1, 2, 3, 4, 5, 6, 7, 8]
    if 'vast' in x and '4' in x:
        if 'wrapper' in x:
            final += [7, 8]
        else:
            final += [7]
    if 'vast' in x and '3' in x:
        if 'wrapper' in x:
            final += [3, 6]
        else:
            final += [3]
    if 'vast' in x and '2' in x:
        if 'wrapper' in x:
            final += [2, 5]
        else:
            final += [2]
    if 'vast' in x and '1' in x:
        if 'wrapper' in x:
            final += [1, 4]
        else:
            final += [1]
    if 'daast' in x and 'wrapper' in x:
        final += [9, 10]
    elif 'daast' in x:
        final += [9]
    return [z for i in final for z in i]


def check_api(x):
    final = []
    if x == 'vpaid':
        final += [1, 2]
    if 'vpaid' in x:
        if '2' in x:
            final += [2]
        else:
            final += [1]
    if x == 'mraid':
        final += [1, 2]
    if 'mraid' in x:
        if '3' in x:
            final += [6]
        if '2' in x:
            final += [5]
        if '1' in x:
            final += [3]
    if x == 'ormma':
        final += [4]
    return [z for i in final for z in i]


@decorators.validator_common
def param_validator(**kwargs):
    if 'initiator' not in kwargs:
        kwargs['initiator'] = socket.gethostbyname(socket.gethostname())
    initiators = io.get_redis('avails/initiators')
    if initiators is None:
        initiators = {}
    if kwargs['initiator'] in initiators and datetime.utcnow() - initiators[kwargs['initiator']] < timedelta(days=1):
        return False, dict({'status': 'fail', 'message': 'User may only run one of these per day'})
    if 'cities' in kwargs and kwargs['cities'] is not None:
        kwargs['cities'] = kwargs['cities'].split(',')
        kwargs['cities'] = [i.strip() for i in kwargs['cities'] if i.strip() != '']
    if 'protocols' in kwargs:
        kwargs['protocols'] = list(set([
            z
            for i in kwargs['protocols'].split(',')
            for z in check_protocols(i.strip().lower())
            if i.strip() != ''
        ]))
    else:
        kwargs['protocols'] = None
    if 'api' in kwargs:
        kwargs['api'] = list(set([
            z
            for i in kwargs.split(',')
            for z in check_api(i.strip().lower())
            if i.strip() != ''
        ]))
    else:
        kwargs['api'] = None
    if 'dsps' in kwargs:
        kwargs['dsps'] = [i.strip() for i in kwargs.split(',') if i.strip() != '']
    else:
        kwargs['dsps'] = None
    if 'imp_type' in kwargs:
        kwargs['imp_type'] = [i.strip().lower() for i in kwargs['imp_type'].split(',') if i.strip() != '']
    else:
        kwargs['imp_type'] = None
    if 'media_type' in kwargs:
        kwargs['media_type'] = [i.strip().lower() for i in kwargs['media_type'].split(',') if i.strip() != '']
    else:
        kwargs['media_type'] = None
    if 'device' in kwargs:
        kwargs['device'] = [i.strip().lower() for i in kwargs['device'].split(',') if i.strip() != '']
    else:
        kwargs['device'] = None
    initiators[kwargs['initiator']] = datetime.utcnow()
    io.to_redis(90000, **{'avails/initiators': initiators})
    return True, kwargs,


@decorators.job_wrapper
def run(
        start_date=None,
        end_date=None,
        to=None,
        cc=None,
        cities=None,
        protocols=None,
        api=None,
        dsps=None,
        imp_type=None,
        media_type=None,
        device=None,
        **kwargs
    ):
    logger = get_logger(settings.SCRIPT_NAME)
    conn = db_conn.connect('snowflake')
    cur = conn.cursor()
    cur.execute(io.get_query('avails/dsps'))
    dsps = [i[0] for i in cur.fetchall() if dsps is None or i[0] in dsps]
    cities = [i.strip().lower() for i in cities] if cities is not None else None
    sf_conn = db_conn.DbConnection('snowflake')
    data = sf_conn.get_frame(
        'avails/city_avails',
        start_date=start_date,
        end_date=end_date,
        cities='AND A.ssp_request:user:geo:city::string IN (\'{0}\')\n'.format(
            "', '".join(cities)
        ) if cities is not None else ''
        if cities is not None and len(cities) > 0 else '',
        protocols='AND A.ssp_request:imp[0]:video:protocols RLIKE \'.*{0}.*\'\n'.format(
            '.*|.*'.join([str(z) for z in protocols])
        )
        if protocols is not None and len(protocols) > 0 else '',
        api='AND A.ssp_request:imp[0]:video:api RLIKE \'.*{0}.*\'\n'.format(
            '.*|.*'.join([str(z) for z in api])
        )
        if api is not None and len(api) > 0 else '',
        dsps='AND A.dsps RLIKE \'.*{0}.*\'\n'.format(
            '.*|.*'.join([str(z) for z in dsps])
        )
        if dsps is not None and len(dsps) > 0 else '',
        imp_type='AND A.imp_type IN (\'{0}\')'.format(
            "', '".join(imp_type)) if imp_type is not None and len(imp_type) > 0 else ''
        ,
        media_type='AND A.media_type IN (\'{0}\')'.format(
            "', '".join(media_type)) if media_type is not None and len(media_type) > 0 else ''
        ,
        device='AND A.device IN (\'{0}\')'.format(
            "', '".join(device)) if device is not None and len(device) > 0 else ''
    )
    # data = data[~data['city'].isnull()]
    # data['city'] = data['city'].apply(lambda x: x.lower())
    # data['dsps'] = data['dsps'].apply(lambda x: json.loads(x) if type(x) == str else None)
    # data = data[
    #     data['city'].isin(cities)
    #     & data[~data['dsps'].isnull()]['dsps'].apply(lambda x: len(set(x) & set(dsps)) > 0)
    # ]
    # data['dsps'] = data['dsps'].apply(lambda x: json.dumps(list(set(x) & set(dsps))))
    # del data['date']
    # data = data.groupby(list(data.columns)[:len(data.columns)-1]).sum().reset_index()

    mailer = email_notifier.get_mailer_object('avails/cities')
    mailer.set_formatter(
        {
            'start_date': start_date,
            'end_date': end_date,
            'dsps': ', '.join(dsps) if dsps is not None else '',
            'cities': ', '.join(cities) if cities is not None else ''
        }
    )
    excel_io = BytesIO()
    io.to_excel(excel_io, **{'Avails': data})
    excel_io.seek(0)
    mailer.attach_file(excel_io, 'city_avails-{0}-{1}.xlsx'.format(start_date, end_date))
    mailer.send_mail(
        'City Avails',
        to
    )
    return data
