DEFAULT_PARAMS = {}

DEPENDENCIES = []

DESCRIPTION = 'Request Avails'

DISPLAY_NAME = 'Request Avails'

FIELD_DESCRIPTORS = [
    {
        'name': 'start_date',
        'type': 'datetime_str',
        'descriptor': 'Start date for data search',
        'required': True
    },
    {
        'name': 'end_date',
        'type': 'datetime_str',
        'descriptor': 'End date for data search',
        'required': True
    },
    {
        'name': 'dsp_ids',
        'type': 'str_array',
        'descriptor': 'List of DSPs to look for',
        'required': True
    },
    {
        'name': 'cities',
        'type': 'str_array',
        'descriptor': 'List of cities to look for',
        'required': True
    },
    {
        'name': 'protocols',
        'type': 'str_array',
        'descriptor': 'List of protocols to look for (VAST 1, VAST 2, etc.)',
        'required': True
    },
    {
        'name': 'api',
        'type': 'str_array',
        'descriptor': 'List of APIs to look for (VPAID, MRAID, etc.)',
        'required': True
    },
    {
        'name': 'imp_type',
        'type': 'str_array',
        'descriptor': 'List of impression types to look for',
        'required': True
    },
    {
        'name': 'media_type',
        'type': 'str_array',
        'descriptor': 'List of media types to look for',
        'required': True
    },
    {
        'name': 'device',
        'type': 'str_array',
        'descriptor': 'List of device types to look for (mobile, desktop, ctv)',
        'required': True
    },
    {
        'name': 'to',
        'type': 'str',
        'required': True,
        'descriptor': 'Who to send the results to'
    },
    {
        'name': 'cc',
        'type': 'str_array',
        'required': False,
        'descriptor': 'Anyone to CC on the results'
    }
]

FREQUENCY = 'MANUALLY'

IS_PUBLIC = True

SCRIPT_NAME = 'avails'

TYPE = 'data_tools'
