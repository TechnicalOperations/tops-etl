DEFAULT_PARAMS = {}

DEPENDENCIES = []

DESCRIPTION = 'Get SSP/DSP connections and metrics'

DISPLAY_NAME = 'Get SSP/DSP connections and metrics'

FIELD_DESCRIPTORS = []

FREQUENCY = 'MANUALLY'

IS_PUBLIC = False

SCRIPT_NAME = 'ssp_dsp'

TYPE = 'data_tools'
