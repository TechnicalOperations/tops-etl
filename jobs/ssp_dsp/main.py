import pandas
import numpy as np
from jobs import arguments
from jobs import decorators
from jobs.ssp_dsp import settings
from etl.util import db_conn
from matplotlib import cm
from matplotlib import pyplot as plt


@decorators.validator_common
def param_validator(**kwargs):
    return True, kwargs,


def plot_heat_map(data):
    x = data.columns.values
    y = data.index.values
    z = data.values
    fig, ax = plt.subplots()
    im = ax.imshow(z, interpolation='nearest', cmap=cm.coolwarm)
    # heatmap = ax.pcolor(z, cmap=plt.cm.Blues)
    ax.set_xticks(np.arange(len(x)))
    ax.set_yticks(np.arange(len(y)))
    ax.set_xticklabels(x)
    ax.set_yticklabels(y)
    plt.setp(ax.get_xticklabels(), rotation=45, ha='right', rotation_mode='anchor')
    # plt.colorbar(heatmap)
    # for i in range(len(y)):
    #     for j in range(len(x)):
    #         text = ax.text(j, i, z[i, j], ha='center', va='center', color='w')
    fig.tight_layout()
    cbar = fig.colorbar(im, ticks=[-1, 0, 1])
    return ax


def plot_groups(grp):
    title = ', '.join(grp.iloc[0].name[:3])
    data = grp.reset_index()[['ssp_id', 'dsp_id', 'requests', 'responses']]
    data['rates'] = data['responses'] * 100.0 / data['requests']
    data = pandas.pivot_table(data, index=['ssp_id'], columns=['dsp_id'], fill_value=0, aggfunc=np.sum)['rates']
    ax = plot_heat_map(data)
    ax.set_title(title)
    plt.draw()
    return grp


# @decorators.job_wrapper
def run(**kwargs):
    conn = db_conn.DbConnection('snowflake')
    data = conn.get_frame('ssp_dsp/metrics')
    rates = conn.get_frame('ssp_dsp/dsp_rates')
    rates['responses'] = rates['responses'].fillna(0).astype(int)
    rates = rates[['dsp_id', 'requests', 'responses']].groupby('dsp_id').sum()
    rates['rates'] = rates['responses'] * 100.0 / rates['requests']
    data = data[(data['filter_reason'] == 'notFiltered')].reset_index(drop=True)
    data = data.groupby(['imp_type', 'media_type', 'device', 'ssp_id', 'dsp_id']).sum()
    data = data.groupby(level=[0, 1, 2]).apply(plot_groups)
    print(data[:30])
    plt.show()


if __name__ == '__main__':
    pandas.set_option('display.width', 5000)
    kwargs = arguments.create_arguments(settings)
    run(**kwargs)
