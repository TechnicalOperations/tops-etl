from io import BytesIO
from etl.logger import get_logger
from jobs.gs_scan import settings
import pandas as pd
import requests
import re
import uuid


class GsScan:

	def __init__(self):
		self.logger = get_logger(settings.SCRIPT_NAME)
		self.data = {'Site': [], 'GS_Allowed': [], 'RobotsTxt': []}


	def load_csv(self, list_file):
		sitelist = []
		try:
			self.logger.info("Reading CSV sitelist...")
			df_csv = pd.read_csv(list_file)
			for idx, row in df_csv.iterrows():
				sitelist.append(row[0])
		except Exception as e:
			self.logger.error(e)
		return sitelist


	def analyze(self, domain):
		def gs_check(robots_txt):
			'''
			Parses robots.txt info (restrictions only)
			:param robots_txt: Robots.txt data
			:return: Returns GS Allowed boolean
			'''

			self.logger.info("Starting GS pattern match")
			wild_ua = re.compile("User-agent:\s\**$")
			gs_ua = re.compile("User-agent:\sgrapeshot*$")
			allow = re.compile("Allow:.+")
			allow_root = re.compile("Allow:\s\/(?!.+)")
			disallow = re.compile("Disallow:.+")
			disallow_root = re.compile("Disallow:\s\/(?!.+)")

			def matchr(j):
				test = True
				for l in j:
					am1 = allow.match(l)
					if am1:
						am = allow_root.match(l)
						if am:
							return True
						else:
							return False
				for l in j:
					dm1 = disallow.match(l)
					if dm1:
						dm = disallow_root.match(l)
						if dm:
							return False
						else:
							return True
				return test

			s = robots_txt.split("\n\n")
			for i in s:
				i = i.split("\n")
				gs_m = gs_ua.match(i[0])
				if gs_m:
					gs_test = matchr(i[1:])
					return gs_test
				else:
					wc_m = wild_ua.match(i[0])
					if wc_m:
						gs_test = matchr(i[1:])
						return gs_test
			return True


		def get_page(src):
			'''
			GETs robots.txt as variable (if exists)
			Hands variable to gs_check function to parse robots.txt
			Appends data hash arrays with results
			:param src: Root domain (ex. domain.com)
			:return: Returns to pool mapping
			'''

			self.logger.info("Starting robots.txt pull: {}".format(src))
			requests.packages.urllib3.disable_warnings()
			headers = {
				'User-Agent': 'Mozilla/5.0 (compatible; GrapeshotCrawler/2.0; +http://www.grapeshot.co.uk/crawler.php)'}
			try:
				r = requests.get("http://{}/robots.txt".format(src), headers=headers, allow_redirects=True, timeout=10)
			except Exception as e:
				self.data['Site'].append(src)
				self.data['GS_Allowed'].append("False")
				return
			if r.status_code == 200:
				robots_txt = ""
				try:
					robots_txt = r.content.decode('utf-8')
				except Exception as e:
					self.data['Site'].append(src)
					self.data['GS_Allowed'].append("False")
					self.data['RobotsTxt'].append("NA")
				gs_result = gs_check(robots_txt)
				print("GS Allowed: {0} ({1})".format(src, gs_result))
				self.data['Site'].append(src)
				self.data['GS_Allowed'].append(gs_result)
				self.data['RobotsTxt'].append(robots_txt)
				return
			else:
				self.data['Site'].append(src)
				self.data['GS_Allowed'].append("False")
				self.data['RobotsTxt'].append("NA")
				return

		get_page(domain)
		return


	def write_data(self):
		'''
		CSV writer for GS data
		:param filename: output file for writing
		:return:
		'''
		df = pd.DataFrame(data=self.data, columns=['Site', 'GS_Allowed'])
		filename = "{0}-{1}.xlsx".format(settings.SCRIPT_NAME, str(uuid.uuid4().hex)[:8])
		output = BytesIO()
		try:
			self.logger.info("Writing output CSV: {}".format(filename))
			df.to_excel(output, index=False)
		except Exception as e:
			self.logger.error(e)
		return output, filename