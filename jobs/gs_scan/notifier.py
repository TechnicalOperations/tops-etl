from etl.notification import email_notifier
from etl.logger import get_logger
from jobs.gs_scan import settings


def send_report(output, filename, to, *cc):
	logger = get_logger(settings.SCRIPT_NAME)
	try:
		logger.info("Creating mailer object")
		mailer = email_notifier.get_mailer_object('gs_scan/mailer')
		mailer.attach_file(output, filename)
		mailer.send_mail('Grapeshot Crawler Report', to, cc=list(cc))
		logger.info("Email sent")
	except Exception as e:
		logger.error(e)
	return
