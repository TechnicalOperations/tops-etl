from jobs import decorators
from etl.logger import get_logger
from jobs.gs_scan import settings
from jobs.gs_scan.gs_scanner import GsScan
from jobs.gs_scan import notifier as email_notifier


@decorators.validator_common
def param_validator(**kwargs):
    logger = get_logger(settings.SCRIPT_NAME)

    logger.info('Checking notifier object')
    if 'to' not in kwargs:
        return False, dict({'status': 'fail', 'message': 'Email Required'})
    if '@rhythmone.com' not in kwargs['to']:
        return False, dict({'status': 'fail', 'message': 'Only RhythmOne email addresses permitted'})
    if 'cc' in kwargs:
        if type(kwargs['cc']) == str:
            kwargs['cc'] = [i for i in kwargs['cc'].split(',') if i.strip() != '']
        kwargs['cc'] = [i for i in kwargs['cc'] if '@rhythmone.com' in i.strip()]

    if isinstance(kwargs['domain'], str):
        kwargs['domain'] = [i.strip() for i in kwargs['domain'].split('\n') if i.strip() != '']

    return True, kwargs,


@decorators.job_wrapper
def run(
        domain=None,
        to=None,
        cc=[]
    ):
    logger = get_logger(settings.SCRIPT_NAME)

    logger.info("Creating GsScan instance")
    scanner = GsScan()

    logger.info("Analyzing domain: {}".format(domain))
    for d in domain:
        scanner.analyze(d)

    csv_doc, filename = scanner.write_data()

    logger.info("Sending data to email notifier")
    email_notifier.send_report(csv_doc, filename, to, *tuple())


if __name__ == '__main__':
    from jobs import arguments
    kwargs = arguments.create_arguments(settings)
    run(**kwargs)
