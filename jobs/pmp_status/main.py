import pandas
import locale
import numpy as np
from jobs import decorators
from etl.util import db_conn
from etl.notification import email_notifier
from datetime import datetime, timedelta, date
pandas.set_option('display.width', 5000)


try:
    locale.setlocale(locale.LC_ALL, 'en_US')
except locale.Error as err:
    try:
        locale.setlocale(locale.LC_ALL, 'en-US')
    except locale.Error as err:
        locale.setlocale(locale.LC_ALL, 'en_US.utf-8')


@decorators.validator_common
def param_validator(**kwargs):
    return True, kwargs,


@decorators.job_wrapper
def run(to=None, cc=None):
    end_date = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    start_date = end_date - timedelta(days=4)
    conn = db_conn.DbConnection('snowflake')
    data = conn.get_frame(
        'pmp_status/status_data',
        expire=43200,
        start_date=start_date,
        end_date=end_date
    )
    data = data.groupby(['date', 'deal_id', 'ssp_id', 'dsp_id']).sum().reset_index()

    q = """
        SELECT external_deal_id AS deal_id, type, description
        FROM ods.rx.deal
        GROUP BY external_deal_id, type, description
    """
    details = conn.get_frame(q)
    types = {
        1: 'Legacy Direct',
        2: 'Always On',
        3: 'Managed',
        4: 'Self Service',
        5: 'Test',
        6: 'Managed'
    }
    types = {i['deal_id']: types[i['type']] for i in details.to_dict(orient='records')}
    details = {i['deal_id']: i['description'] for i in details.to_dict(orient='records')}

    data['deal_type'] = data['deal_id'].apply(lambda x: types.get(x, 'Managed - No Deal'))
    deals = data[(data['date'] == data['date'].max())][['deal_type', 'deal_id', 'margin', 'revenue']].groupby(
        ['deal_type', 'deal_id']
    ).sum().reset_index()
    odeals = deals.copy()
    m = email_notifier.get_mailer_object('pmp_status/template')

    formatter = {}
    for deal_type in ['Self Service', 'Managed']:
        deals = odeals[(odeals['deal_type'] == deal_type)].copy()

        total_revenue = int(np.round(data[(data['date'] == data['date'].max()) & (data['deal_type'] == deal_type)]['revenue'].sum(), 0))
        total_margin = int(np.round(data[(data['date'] == data['date'].max()) & (data['deal_type'] == deal_type)]['margin'].sum(), 0))
        total = data[(data['deal_type'] == deal_type)][['date', 'revenue', 'margin']].groupby('date').sum().reset_index()
        deal_type = deal_type.lower().replace(' ', '_')

        deals = deals.sort_values(by='revenue', ascending=False).reset_index(drop=True)
        top = set(deals['deal_id'][:6])
        deals['deal_id'] = deals['deal_id'].apply(lambda x: details.get(x, 'Managed - No Deal') if x in top else 'Other')
        deals = deals.groupby('deal_id').sum().sort_values(by='revenue', ascending=False).reset_index()
        top_margin = deals[['deal_id', 'margin']]
        top_revenue = deals[['deal_id', 'revenue']]
        total['date'] = total['date'].apply(lambda x: date(x.year, x.month, x.day))

        m.make_graph('{0}_top_revenue'.format(deal_type), top_revenue, title='Revenue')
        legend = m.make_graph('{0}_top_margin'.format(deal_type), top_margin, title='Margin')
        m.make_graph('{0}_total'.format(deal_type), total, colors=['#16d1f0', '#fedd00'], title=None)

        legend = list(legend)
        legend.sort(
            key=lambda x: 0 - top_revenue[(top_revenue['deal_id'] == x[0])]['revenue'].sum() if x[0] != 'Other' else 0)
        legend = [(i[0], i[1], m.format_float(top_revenue[(top_revenue['deal_id'] == i[0])]['revenue'].sum()),) for i in
                  legend]
        top_legend = "\n".join([
            """<tr><td style="background-color: {1}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>{0}</td><td>${2}</td></tr>""".format(
                i[0], i[1], i[2])
            for i in legend
        ])
        current_date = data['date'].max().strftime('%m/%d/%Y')
        formatter['{0}_total_revenue'.format(deal_type)] = locale.format("%d", total_revenue, grouping=True)
        formatter['{0}_total_margin'.format(deal_type)] = locale.format("%d", total_margin, grouping=True)
        formatter['{0}_top_legend'.format(deal_type)] = top_legend
        formatter['date'] = current_date

    formatter['total_revenue'] = locale.format("%d", odeals['revenue'].sum(), grouping=True)
    m.set_formatter(formatter=formatter)
    m.send_mail(
        'Deal Metrics for {0} - ${1}'.format(data['date'].max().strftime('%m/%d/%Y'), formatter['total_revenue']),
        to,
        cc=cc
    )


if __name__ == '__main__':
    import argparse
    # to = 'tops@rhythmone.com'
    # cc = ['pmpops@rhythmone.com', 'krayes@rhythmone.com', 'jmurphy@rhythmone.com', 'llam@rhythmone.com']
    parser = argparse.ArgumentParser()
    parser.add_argument('--to')
    parser.add_argument('--cc', nargs='*', default=[])
    args = parser.parse_args()
    run(
        to=args.to,
        cc=args.cc
    )
