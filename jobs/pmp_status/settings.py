
FREQUENCY = 'DAILY'

SCRIPT_NAME = 'pmp_status'

DISPLAY_NAME = 'PMP Daily Email'

DESCRIPTION = 'PMP Daily Status'

FIELD_DESCRIPTORS = []

DEFAULT_PARAMS = {
    'to': 'tops@rhythmone.com',
    'cc': ['pmpops@rhythmone.com', 'krayes@rhythmone.com', 'llam@rhythmone.com', 'cmoran@rhythmone.com']
}

IS_PUBLIC = False
