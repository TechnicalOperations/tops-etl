import pandas
import numpy as np
from etl.util import stats
from datetime import date
from etl.notification import slack_notifier


def some_function(data):
    ias_stats = stats.weighted_stats(data[['date', 'ias_rate', 'requests']], 'date', 'ias_rate', 'requests')
    dv_stats = stats.weighted_stats(data[['date', 'dv_ivt', 'requests']], 'date', 'dv_ivt', 'requests')

    ias_slope = ias_stats[['avg']].reset_index()
    ias_slope['date'] = ias_slope['date'].apply(lambda x: x.toordinal())
    dv_slope = dv_stats[['avg']].reset_index()
    dv_slope['date'] = dv_slope['date'].apply(lambda x: x.toordinal())

    ias_slope = stats.slope_angle(ias_slope, 'date', 'avg')
    ias_slope['date'] = ias_slope['date'].apply(lambda x: date.fromordinal(int(x)))
    dv_slope = stats.slope_angle(dv_slope, 'date', 'avg')
    dv_slope['date'] = dv_slope['date'].apply(lambda x: date.fromordinal(int(x)))

    ias_avg = (ias_stats['avg'] * ias_stats['requests']).sum() / ias_stats['requests'].sum()
    ias_std = ((ias_stats['avg'] - ias_avg).apply(lambda x: x**2) * ias_stats['requests']).sum() / ias_stats['requests'].sum()
    ias_std = ias_std ** (1/2)

    dv_avg = (dv_stats['avg'] * dv_stats['requests']).sum() / dv_stats['requests'].sum()
    dv_std = ((dv_stats['avg'] - dv_avg).apply(lambda x: x**2) * dv_stats['requests']).sum() / dv_stats['requests'].sum()
    dv_std = dv_std ** (1/2)

    ias_slope = ias_slope['angle'].mean()
    dv_slope = dv_slope['angle'].mean()

    days = data['date'].max() - data['date'].min()
    days = days.total_seconds() / 60 / 60 / 24
    days = int(days)
    # if ias_slope > 20 and dv_slope > 20:
    #     slack_notifier.send_message(
    #         '<@cbeecher> <@mgillespie> <@mvillaseca>\nBoth IAS and DV IVT rates have been increasing the past {0} day(s)'
    #             .format(days)
    #     )
    # elif ias_slope > 20:
    #     slack_notifier.send_message(
    #         '<@cbeecher> <@mgillespie> <@mvillaseca>\nIAS rates have been increasing the past {0} day(s)'.
    #             format(days)
    #     )
    # elif dv_slope > 20:
    #     slack_notifier.send_message(
    #         '<@cbeecher> <@mgillespie> <@mvillaseca>\nDV rates have been increasing the past {0} day(s)'.
    #             format(days)
    #     )


def create_keys(data):
    keys = [
        (i['ssp_id'], i['pub_id'], i['placement_id'],)
        for i in data[['ssp_id', 'pub_id', 'placement_id']].to_dict(orient='records')
    ]
    keys = {keys[i]: i for i in range(len(keys))}
    data['ssp_id'] = data.apply(lambda x: keys[(x['ssp_id'], x['pub_id'], x['placement_id'],)], axis=1)
    del data['pub_id']
    del data['placement_id']
    data = data.rename(columns={'ssp_id': 'key'})
    keys = {v: k for k, v in keys.items()}
    return keys, data,
