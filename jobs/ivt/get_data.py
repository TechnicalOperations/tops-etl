import logging
import traceback
from jobs.ivt import job_name
from etl.util import io
from etl.util.db_conn import DbConnection
from datetime import datetime
from datetime import timedelta


def get_ivt(lookback=timedelta(days=1)):
    logger = logging.getLogger(job_name)
    conn = DbConnection('snowflake')
    logger.info('Fetching IVT data')
    try:
        end = datetime.utcnow().replace(minute=0, second=0, microsecond=0)
        start = end - lookback
        data = conn.get_frame_date_append(
            'ivt/pub_site_domain_ivt',
            'date',
            start,
            end,
            start_date=start,
            end_date=end
        )
    except Exception as err:
        logger.error('Error occurred while fetching IVT data:')
        traceback.print_exception()
        exit()
    logger.info('Successfully fetched IVT data')
    return data


def get_dsp_thresholds():
    logger = logging.getLogger(job_name)
    threshold_query = 'ivt/dsp_thresholds'
    data = io.get_redis(threshold_query)
    conn = DbConnection('rx_mysql')
    logger.info('Fetching DSP IVT thresholds')
    try:
        append = conn.get_frame(threshold_query, redis=False, expire=21600)
    except Exception as err:
        logger.error('Error occurred while fetching IVT thresholds:')
        traceback.print_exception()
        exit()
    append['pulled'] = datetime.utcnow()
    if data is None:
        data = append
    else:
        data = data.append(append)
    io.to_redis(21600, **{threshold_query: data})
    return data
