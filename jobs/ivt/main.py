from . import process
from . import get_data


def run(**kwargs):
    ivt_data = get_data.get_ivt()
    # thresholds = get_data.get_dsp_thresholds()
    process.some_function(ivt_data)


if __name__ == '__main__':
    run()
