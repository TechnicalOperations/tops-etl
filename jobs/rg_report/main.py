import json
import pandas
from io import BytesIO
from datetime import date
from jobs import decorators
from jobs.rg_report import settings
from etl.util import db_conn, io
from etl.notification import email_notifier
from matplotlib import pyplot as plt


@decorators.validator_common
def param_validator(**kwargs):
    if isinstance(kwargs['pub_ids'], str):
        kwargs['pub_ids'] = kwargs['pub_ids'].split(',')
    if not isinstance(kwargs['pub_ids'], list):
        return False, dict({'status': 'fail', 'message': 'Publishers must be a list'}),
    kwargs['pub_ids'] = [i.strip() for i in kwargs['pub_ids'] if i.strip() != '']
    if 'to' not in kwargs:
        return False, dict({'status': 'fail', 'message': 'Email Required'})
    if '@rhythmone.com' not in kwargs['to']:
        return False, dict({'status': 'fail', 'message': 'Only RhythmOne email addresses permitted'})
    if isinstance(kwargs.get('cc', ''), str):
        kwargs['cc'] = [i for i in kwargs['cc'].split(',') if i.strip() != '']
    kwargs['cc'] = [i for i in kwargs['cc'] if '@rhythmone.com' in i.strip()]
    return True, kwargs,


def gs_results(x):
    if not isinstance(x, dict):
        return 'No Results'
    if 'error' in x or 'channels' not in x:
        return 'Error'
    if x['channels'] is None:
        return 'No Results'
    channels = [i['name'] for i in x['channels']]
    if len(channels) == 0:
        return 'No Results'
    if 'gv_' in channels:
        return 'GV Results'
    return 'Valid'


@decorators.job_wrapper
def run(**kwargs):
    conn = db_conn.DbConnection('snowflake')
    data = conn.get_frame(
        'rg_report/base',
        date=kwargs['date'],
        pubs="', '".join(kwargs['pub_ids']),
        # redis=False,
        # force_query=True
    )
    data = data[(data['date'] > data['date'].min()) & (data['date'] < data['date'].max())].reset_index(drop=True)
    js_cols = ['blocklist_filter', 'domains_filter', 'ip_filter', 'ua_filter', 'gs_results', 'dsps']
    dsps = {'tradedesk', 'tradedeskex', 'bidswitch', 'bidswitchvideo', 'turn'}
    for col in js_cols:
        data[col] = data[col].apply(lambda x: json.loads(x) if isinstance(x, str) else None)
    data['ias_rate'] = data['ias_rate'].fillna(0).apply(lambda x: x * 100).astype(int)
    data['dv_rate'] = data['dv_rate'].fillna(0).apply(lambda x: x * 100).astype(int)
    data['blocklist_filter'] = data['blocklist_filter'].apply(lambda x: ','.join(x) if isinstance(x, list) else None)
    data['whitelist'] = data['domains_filter'].apply(
        lambda x: ','.join(x.get('whitelist', [])) if isinstance(x, dict) else None
    )
    data['blacklist'] = data['domains_filter'].apply(
        lambda x: ','.join(x.get('blacklist', [])) if isinstance(x, dict) else None
    )
    del data['domains_filter']
    data['ip_filter'] = data['ip_filter'].apply(
        lambda x: ','.join(
            [i if 'pixalate_' not in i else 'pixalate' for i in x]
        ) if isinstance(x, list) else None
    )
    data['device_type'] = data['ua_filter'].apply(lambda x: x.get('deviceType', '-') if isinstance(x, dict) else '-')
    data['found_ua'] = data['ua_filter'].apply(lambda x: x.get('found', False) if isinstance(x, dict) else False)
    data['ua_list'] = data['ua_filter'].apply(lambda x: x.get('list', '-') if isinstance(x, dict) else '-')
    data['ua_os'] = data['ua_filter'].apply(lambda x: x.get('os', '-') if isinstance(x, dict) else '-')
    for dsp in dsps:
        data[dsp] = data.apply(lambda x: x['count'] if isinstance(x['dsps'], list) and dsp in x['dsps'] else 0, axis=1)
    del data['dsps']
    del data['ua_filter']
    del data['ua_os']
    del data['device_type']
    data['gs_results'] = data['gs_results'].apply(gs_results)
    # print(data.sort_values(by='count', ascending=False)[:10])
    # print(list(data.columns))
    cols = [
        'source_rating',
        'overall_blacklist',
        'source_blacklist',
        'user_blacklist',
        'segments_blacklist',
        'pub_id',
        'ias_rate',
        'dv_rate',
        'placement_id',
        'bundle_domain',
        'gs_results',
        'blocklist_filter',
        'blacklist',
        'whitelist',
        'ip_filter',
        'found_ua',
        'ua_list',
        'count'
    ] + list(dsps)
    for col in cols:
        data[col] = data[col].fillna('-').apply(lambda x: '-' if x == '' else x)
    data = data[['date', 'ssp_id'] + cols]
    source = pandas.pivot_table(
        data[['date', 'source_blacklist', 'count']], index=['date'], columns=['source_blacklist'], aggfunc=sum
    )
    source.columns = source.columns.droplevel(0)
    user = pandas.pivot_table(
        data[['date', 'user_blacklist', 'count']], index=['date'], columns=['user_blacklist'], aggfunc=sum
    )
    user.columns = user.columns.droplevel(0)
    segments = pandas.pivot_table(
        data[['date', 'segments_blacklist', 'count']], index=['date'], columns=['segments_blacklist'], aggfunc=sum
    )
    segments.columns = segments.columns.droplevel(0)
    ivt = data[['date', 'ias_rate', 'dv_rate', 'count']].copy()
    ivt['ias_rate'] = ivt['ias_rate'] * ivt['count']
    ivt['dv_rate'] = ivt['dv_rate'] * ivt['count']
    ivt = ivt.groupby(['date']).sum().reset_index()
    ivt['ias_rate'] = ivt['ias_rate'] / ivt['count']
    ivt['dv_rate'] = ivt['dv_rate'] / ivt['count']
    source.plot()
    plt.title('Source Blacklist Hit')
    source = BytesIO()
    plt.savefig(source, format='png')
    user.plot()
    plt.title('User Blacklist Hit')
    user = BytesIO()
    plt.savefig(user, format='png')
    plt.title('Segments Blacklist Hit')
    segments = BytesIO()
    plt.savefig(segments, format='png')
    ivt.plot(x='date', y=['dv_rate', 'ias_rate'])
    plt.title('Avg IVT Rates')
    ivt = BytesIO()
    plt.savefig(ivt, format='png')
    data['date'] = data['date'].apply(lambda x: date(x.year, x.month, x.day))
    data = data.rename(columns={
        # 'date': 'Date (EST)',
        'ssp_id': 'SSP',
        'source_rating': 'Source Rating',
        'rating_override': 'Rating Override',
        'source_blacklist': 'Source Blacklist',
        'user_blacklist': 'User Blacklist',
        'segments_blacklist': 'Segments Blacklist',
        'overall_blacklist': 'Overall Blacklist',
        'pub_id': 'Publisher ID',
        'placement_id': 'Placement ID',
        'bundle_domain': 'Domain/Bundle',
        'gs_results': 'GS Results',
        'blocklist_filter': 'Blocklist Filter',
        'whitelist': 'Domains Whitelist',
        'blacklist': 'Domains Blacklist',
        'ip_filter': 'IP Filter',
        'ua_filter': 'User Agent Filter',
        'device_type': 'Device Type',
        'found_ua': 'Found UA',
        'ua_list': 'UA List',
        'ua_os': 'OS'
    })
    data = data.groupby(list(data.columns)[:len(data.columns)-1]).sum().reset_index()
    data['ias_rate'] = data['ias_rate'].astype(float).apply(lambda x: x / 100.0)
    data['dv_rate'] = data['dv_rate'].astype(float).apply(lambda x: x / 100.0)
    output = BytesIO()
    writer = pandas.ExcelWriter(output, engine='xlsxwriter')
    data.to_excel(writer, sheet_name='Data', index=False)
    sheet = writer.book.add_worksheet('Charts')
    sheet.insert_image('A1', 'source.png', {'image_data': source})
    sheet.insert_image('A25', 'user.png', {'image_data': user})
    sheet.insert_image('K1', 'segments.png', {'image_data': segments})
    sheet.insert_image('K25', 'ivt.png', {'image_data': ivt})
    writer.save()
    writer.close()
    output.seek(0)
    if 'to' in kwargs:
        if 'cc' not in kwargs:
            kwargs['cc'] = ''
        if isinstance(kwargs['cc'], str):
            kwargs['cc'] = kwargs['cc'].split(',')
        mailer = email_notifier.get_mailer_object('basic/template')
        mailer.set_formatter(
            {
                'query_used': io.get_query('rg_report/base').format(
                    pubs="', '".join(kwargs['pub_ids']), date=kwargs['date']
                )
            }
        )
        mailer.attach_file(output, '[{0}] rg_report.xlsx'.format(kwargs.get('ticket', 'No Ticket')))
        mailer.send_mail('[{0}] RG Report'.format(kwargs.get('ticket', 'No Ticket')), kwargs['to'], cc=kwargs['cc'])
    return output.read()


if __name__ == '__main__':
    import os
    from jobs import arguments
    pandas.set_option('display.width', 5000)
    pandas.set_option('display.max_columns', 500)
    kwargs = arguments.create_arguments(settings)
    data = run(**kwargs)
    with open(os.path.expanduser('~') + '/Documents/data/results.xlsx', 'wb') as f:
        f.write(data)
