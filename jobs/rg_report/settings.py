DEFAULT_PARAMS = {}

DEPENDENCIES = []

DESCRIPTION = 'RG Rating Report'

DISPLAY_NAME = 'RG Rating Report'

FIELD_DESCRIPTORS = [
    {
        'name': 'date',
        'type': 'datetime_str',
        'descriptor': 'Start date for data search',
        'required': True
    },
    # {
    #     'name': 'end_date',
    #     'type': 'datetime_str',
    #     'descriptor': 'End date for data search',
    #     'required': True
    # },
    {
        'name': 'pub_ids',
        'type': 'str_array',
        'descriptor': 'List of publisher IDs to look for',
        'required': True
    },
    {
        'name': 'to',
        'type': 'str',
        'required': True,
        'descriptor': 'Who to send the results to'
    },
    {
        'name': 'cc',
        'type': 'str_array',
        'required': False,
        'descriptor': 'Anyone to CC on the results'
    }
]

FREQUENCY = 'MANUALLY'

IS_PUBLIC = False

SCRIPT_NAME = 'rg_report'

TYPE = 'data_tools'
