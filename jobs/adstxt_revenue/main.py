import pandas
import tldextract
from io import BytesIO
from etl.util import io
from etl.util import db_conn
from etl.logger import get_logger
from jobs import decorators
from jobs.adstxt_revenue import settings
from datetime import datetime, timedelta
from etl.notification import email_notifier
pandas.set_option('display.width', 5000)


ADSTXT_REV_FILE = 'adstxt_revenue.xlsx'
IMPRESSIONS_KEY = 'adstxt_revenue/impressions'
ADSTXT_KEY = 'adstxt_revenue/adstxt'


logger = get_logger(settings.SCRIPT_NAME)
date = datetime.now()
date = date.replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=8)


@decorators.validator_common
def param_validator(**kwargs):
    if 'to' not in kwargs:
        return False, dict()
    kwargs = {
        'to': kwargs['to'],
        'cc': kwargs.get('cc', list())
    }
    kwargs['cc'] = [i.strip() for i in kwargs['cc'].split(',') if i.strip() != '']
    return True, kwargs,


@decorators.job_wrapper
def run(to=None, cc=[]):
    conn = db_conn.DbConnection('snowflake')
    logger.info('Getting request data')
    data = conn.get_frame(
        IMPRESSIONS_KEY,
        date=date
    )

    logger.info('Getting Ads.txt data')
    ads = conn.get_frame(
        ADSTXT_KEY,
        date=date
    )

    logger.info('Transforming request data')
    data['domain'] = data['domain'].apply(lambda x: tldextract.extract(x).registered_domain.strip())
    data = data.groupby(['date', 'domain', 'dsp', 'seat']).sum().reset_index()

    logger.info('Merging data sets')
    ads_date = {x: pandas.Timestamp(year=x.year, month=x.month, day=x.day) for x in set(ads['date'])}
    ads['date'] = ads['date'].apply(lambda x: ads_date[x])
    data_date = {x: pandas.Timestamp(year=x.year, month=x.month, day=x.day) for x in set(data['date'])}
    data['date'] = data['date'].apply(lambda x: data_date[x])

    data = data.merge(ads, on=['domain', 'date'], how='left')

    logger.info('Data Merged')

    grand_total = data['revenue'].sum()
    data = data[~data['has_r1'].isnull()].reset_index(drop=True)
    data['has_r1'] = data['has_r1'].astype(int)
    data['has_r1_direct'] = data['has_r1_direct'].astype(int)
    data = data[['date', 'domain', 'dsp', 'seat', 'has_r1', 'has_r1_direct', 'revenue', 'cost']]

    logger.info('Creating data sets to write to excel file')
    total = 'Ads.txt Revenue Last {0}'.format((data['date'].max() - data['date'].min()).days)
    columns = pandas.Index([
        total,
        'Has R1 Ads.txt',
        '% of Total ',
        'R1 listed as Direct',
        ' % of Total',
        'R1 not listed as Direct',
        ' % of Total '
    ])
    index = pandas.Index([0])
    overview = pandas.DataFrame(
        columns=columns,
        index=index
    )

    overview[total] = data['revenue'].sum()
    overview['Has R1 Ads.txt'] = data[(data['has_r1'] == 1)]['revenue'].sum()
    overview['% of Total '] = overview['Has R1 Ads.txt'].sum() * 100.0 / overview[total].sum()
    overview['R1 listed as Direct'] = data[(data['has_r1_direct'] == 1)]['revenue'].sum()
    overview[' % of Total'] = overview['R1 listed as Direct'].sum() * 100.0 / overview[total].sum()
    overview['R1 not listed as Direct'] = data[(data['has_r1'] == 1) & (data['has_r1_direct'] == 0)]['revenue'].sum()
    overview[' % of Total '] = overview['R1 not listed as Direct'].sum() * 100.0 / overview[total].sum()

    domain = data[(data['has_r1'] >= 0)][['domain', 'has_r1', 'has_r1_direct', 'revenue', 'cost']].groupby(['domain', 'has_r1', 'has_r1_direct']).sum().reset_index()
    dsp = data[(data['has_r1'] >= 0)][['dsp', 'has_r1', 'has_r1_direct', 'revenue', 'cost']].groupby(['dsp', 'has_r1', 'has_r1_direct']).sum().reset_index()
    seat = data[(data['has_r1'] >= 0)][['seat', 'has_r1', 'has_r1_direct', 'revenue', 'cost']].groupby(['seat', 'has_r1', 'has_r1_direct']).sum().reset_index()

    excel_output = BytesIO()
    writer = pandas.ExcelWriter(excel_output, engine='xlsxwriter')

    overview.to_excel(writer, sheet_name='Overview', index=False)
    domain.to_excel(writer, sheet_name='Domain Aggregate', index=False)
    dsp.to_excel(writer, sheet_name='DSP Aggregate', index=False)
    seat.to_excel(writer, sheet_name='Seat Aggregate', index=False)
    # data.to_excel(writer, sheet_name='Day Aggregate', index=False)

    writer.save()

    logger.info('Generating email')
    m = email_notifier.get_mailer_object('adstxt_revenue/template')

    domain_table = '''
        <tr>
            <td>{domain}</td>
            <td class='does-have'>&nbsp;</td>
            <td class='{has_r1}'>&nbsp;</td>
            <td class='{has_r1_direct}'>&nbsp;</td>
            <td>{revenue}</td>
        </tr>
    '''
    table_data = domain[['domain', 'has_r1', 'has_r1_direct', 'revenue']].sort_values(by='revenue', ascending=False)
    table_data = table_data[table_data.apply(lambda x: x['has_r1'] == 0 or x['has_r1_direct'] == 0, axis=1)]
    table_data = table_data.reset_index(drop=True)[:50]
    table_data = table_data.to_dict(orient='records')
    output = []
    for row in table_data:
        row['has_adstxt'] = 1
        row['has_r1'] = 'does-have' if row['has_r1'] == 1 else 'does-not-have'
        row['has_r1_direct'] = 'does-have' if row['has_r1_direct'] == 1 else 'does-not-have'
        row['revenue'] = m.format_float(row['revenue'], 2)
        output.append(domain_table.format(**row))
    output = '\n'.join(output)

    now = datetime.now()
    data = data[data['date'].apply(
        lambda x: (x.year == now.year and (x.day < now.day or x.month < now.month)) or (x.year < now.year and x.month > now.month)
    )]
    md1 = now-timedelta(days=1)
    md2 = now-timedelta(days=2)
    md3 = now-timedelta(days=3)
    day1 = data['date'].apply(lambda x: x.day == md1.day)
    day2 = data['date'].apply(lambda x: x.day == md2.day)
    day3 = data['date'].apply(lambda x: x.day == md3.day)
    has_r1 = data[(data['has_r1'] == 1)]
    hr = [None] * 3
    hr[0] = has_r1['date'].apply(lambda x: x.day == md1.day)
    hr[1] = has_r1['date'].apply(lambda x: x.day == md2.day)
    hr[2] = has_r1['date'].apply(lambda x: x.day == md3.day)
    formatter = {
        'date': now.strftime('%Y-%m-%d'),
        'start_date': data[(data['has_r1'] >= 0)]['date'].min().strftime('%m/%d/%Y'),
        'end_date': data[(data['has_r1'] >= 0)]['date'].max().strftime('%m/%d/%Y'),
        'r1p': overview['% of Total '].sum(),
        'total_revenue': data['revenue'].sum(),
        'day1': md1.strftime('%m-%d'),
        'day2': md2.strftime('%m-%d'),
        'day3': md3.strftime('%m-%d'),
        'perc1': 100.0 - has_r1[hr[0]]['revenue'].sum() * 100.0 / data[day1]['revenue'].sum(),
        'perc2': 100.0 - has_r1[hr[1]]['revenue'].sum() * 100.0 / data[day2]['revenue'].sum(),
        'perc3': 100.0 - has_r1[hr[2]]['revenue'].sum() * 100.0 / data[day3]['revenue'].sum(),
        'abs1': data[(data['has_r1'] >= 0)]['revenue'].sum(),
        'abs2': overview['Has R1 Ads.txt'].sum(),
        'abs3': overview['R1 listed as Direct'].sum(),
        'abs4': overview['R1 not listed as Direct'].sum(),
        'rp1': data['revenue'].sum() * 100.0 / grand_total,
        'rp2': overview['% of Total '].sum(),
        'rp3': overview[' % of Total'].sum(),
        'rp4': overview[' % of Total '].sum(),
        'domain_table': output
    }

    for k, v in formatter.items():
        if type(v) != str:
            formatter[k] = m.format_float(v, 2)

    logger.info('Attaching excel file and sending email')
    excel_output.seek(0)
    m.attach_file(excel_output, ADSTXT_REV_FILE)
    m.set_formatter(formatter=formatter)
    m.send_mail('Ads.txt Revenue', to, cc=cc)
    io.delete_redis(IMPRESSIONS_KEY)
    io.delete_redis(ADSTXT_KEY)


if __name__ == '__main__':
    import argparse
    pandas.set_option('display.width', 5000)
    parser = argparse.ArgumentParser()
    parser.add_argument('--to')
    parser.add_argument('--cc', nargs='*')
    args = parser.parse_args()
    run(
        to=args.to,
        cc=args.cc
    )
