FREQUENCY = 'MANUALLY'

SCRIPT_NAME = 'adstxt_revenue'

DISPLAY_NAME = 'Ads.txt Revenue'

DESCRIPTION = 'Ads.txt Revenue Coverage Report'

FIELD_DESCRIPTORS = [
    {
        'name': 'to',
        'type': 'str',
        'required': True,
        'descriptor': 'Who to send the results to'
    },
    {
        'name': 'cc',
        'type': 'str_array',
        'required': False,
        'descriptor': 'Anyone to CC on the results'
    }
]

DEFAULT_PARAMS = {
    'to': 'cbeecher@rhythmone.com',
    'cc': ['mvillaseca@rhythmone.com']
}

IS_PUBLIC = False
