import re
import json
import time
import random
import pandas
import requests
import traceback
import tldextract
from io import BytesIO
from etl.util import io
from etl.logger import get_logger
from etl.job_processor import app
from etl.notification import email_notifier
from datetime import datetime
from jobs import decorators
from jobs.arguments import create_arguments
from jobs.adstxt_crawler import settings


logger = get_logger(settings.SCRIPT_NAME)

html = re.compile('<.*?html|<.*?\?.*?xml|<.*?script|<.*?meta|<.*?head', re.I)
subdomains_re = re.compile('subdomain.*?=([^#$ =\n]*)', re.M | re.I)
comment = re.compile("#.*$")

header = {
    'User-Agent': None
}

ua = [
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2226.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
    'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20130401 Firefox/31.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A',
    'Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.13+ (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.3 Safari/534.53.10'
]


def ping(domain, timeout, padding):
    """
    Iterator wrapper for the crawler
    :param domain: The domain to be pinged
    :param timeout: Timeout for requests
    :param padding: Additional padding for timeout of exception
    :return: Returns results of the attempted get of ads.txt
    """
    header['User-Agent'] = random.choice(ua)
    try:
        # Check https first
        data = requests.get('https://' + domain + '/ads.txt', allow_redirects=True, headers=header, timeout=timeout)
        if html.search(data.text.lower()) is not None or data.status_code != 200:
            data = requests.get('http://' + domain + '/ads.txt', allow_redirects=True, headers=header, timeout=timeout)
    except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout):
        # If and error is thrown, try http first instead of https
        try:
            data = requests.get('http://' + domain + '/ads.txt', allow_redirects=True, headers=header, timeout=timeout+padding)
        except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout) as e:
            # If the same error is thrown as before, prepend on www. to the domain and retry
            # If adding www. to the domain still fails, raise the exception
            if 'www.' not in domain:
                return ping('www.'+domain, timeout, padding)
            else:
                raise e
    except requests.exceptions.TooManyRedirects:
        data = requests.get('http://' + domain + '/ads.txt', allow_redirects=True, headers=header, timeout=timeout)
    return data


def get_adstxt(domain, timeout, padding):
    """
    Wrapper for the ads.txt ping and cleaner before returning results
    :param domain: The domain to be pinged
    :param timeout: Timeout for requests
    :param padding: Additional padding for timeout of exception
    :param debug: kwarg for disabling domain extraction for testing purposed
    :param use_tld: Extract top-level domain, otherwise use subdomain
    :return: Returns tuple of (extracted domain, ads result,)
    """
    # If the domain was already processed, this is indicated by the -1 return
    if domain is None:
        return domain, -1
    # Ping domain for ads.txt
    data = ping(domain, timeout, padding)
    # Check if results are valid or not
    if data is not None:
        data.close()
        if data.status_code == 200 and html.search(data.text.lower()) is None:
            return domain, re.sub(r'[^\x00-\x7f]', r'', data.text)
    return domain, None


def enumerate_exceptions(string):
    """
    Enumerates specific errors to an error code
    :param string: Full stack trace
    :return: Returns the error code
    """
    if 'TooManyRedirects' in string:
        return 'redirects'
    if 'ChunkedEncodingError' in string:
        return 'chunkencoding'
    if 'ConnectionError' in string:
        return 'connection'
    if 'LocationValueError' in string:
        return 'location'
    if 'ContentDecodingError' in string:
        return 'contentdecoding'
    if 'SSLError' in string:
        return 'ssl'
    if 'UnicodeError' in string:
        return 'unicodeerror'
    if 'ReadTimeout' in string:
        return 'timeout'
    if 'ValueError' in string:
        return 'character'
    return 'unknown'


def expand(domain, ads_txt):
    if ads_txt is None:
        return domain, None,
    try:
        ads_txt = ads_txt.split('\n')
    except (AttributeError, TypeError) as err:
        logger.info("Adstxt entry for '{0}' is invalid. Adstxt Type: {1}".format(
            domain, type(ads_txt)
        ))
        return domain, None,
    result = []
    for line in ads_txt:
        line = comment.sub('', line).strip().split(',')
        line = [i.strip() for i in line if i.strip() != '']
        if len(line) < 3 or len(line) > 4:
            continue
        result.append({
                'seller': line[0],
                'pub_id': line[1],
                'relationship': line[2],
                'tag_id': line[3] if len(line) > 3 else None
            }
        )
    return domain, result,


def build_item(domain, ads, error):
    """
    Build the json/dictionary object to be written to and output
    :param domain: The domain that was pinged
    :param ads: The result of the ping
    :param error: Any errors that were thrown
    :return: Returns the object (dictionary)
    """
    current_time = datetime.now()
    current_time = int(time.mktime(current_time.timetuple()) * 1000)
    tld_domain = tldextract.extract(domain).registered_domain.lower().strip() if domain is not None else None
    item = {
        'domain': tld_domain.lower().strip() if domain is not None else None,
        'subdomain': None if domain == tld_domain else domain,
        'has_adstxt': 1 if ads is not None else 0,
        'adstxt_data': ads.strip() if ads is not None else None,
        'errors': None if error is None else enumerate_exceptions(error),
        'has_r1_adstxt': 1 if ads is not None and ('1rx' in ads.lower() or 'rhythmone' in ads.lower()) else 0,
        'is_empty': 1 if ads is not None and ads.strip() == '' else 0,
        'time_server': current_time
    }
    if item['domain'] is not None and item['adstxt_data'] is not None:
        item['parsed_data'] = expand(item['domain'], item['adstxt_data'])[1]
    return item


@app.task
def crawler(domain_q, output_q, processed, timeout, padding):
    """
    Iterator wrapper for the crawler
    :param domain_q: The domain queue
    :param timeout: Timeout for requests
    :param padding: Additional padding for timeout of exception
    :param debug: kwarg for disabling domain extraction for testing purposed
    :return: Returns nothing/None
    """
    domain = io.pop_redis_queue(domain_q)
    while domain is not None:
        if io.in_redis_set(processed, domain):
            domain = io.pop_redis_queue(domain_q)
            continue
        try:
            # Check for ads.txt
            ads = get_adstxt(domain, timeout, padding)
            if ads[1] == -1:
                continue
            error = None
        except:
            # Still log any domains which threw an error and log that error
            ads = (domain, None,)
            error = ''.join(traceback.format_exc())
        finally:
            item = build_item(ads[0], ads[1], error)
        text = item['adstxt_data']
        if text is not None:
            subdomains = subdomains_re.findall(text)
            for subdomain in subdomains:
                if subdomain.strip() != '':
                    io.to_redis_queue(domain_q, subdomain.strip())
        io.to_redis_queue(output_q, item)
        io.to_redis_set(processed, domain)
        domain = io.pop_redis_queue(domain_q)
    return None


@decorators.validator_common
def param_validator(**kwargs):
    if kwargs.get('domains', '').strip() == '':
        return False, dict({'status': 'fail', 'message': 'At least one domain is required to run the report'}),
    kwargs['domains'] = kwargs['domains'].split('\n')
    kwargs['domains'] = [i.strip() for i in kwargs['domains'] if i.strip() != '']
    if isinstance(kwargs.get('cc', ''), str):
        kwargs['cc'] = kwargs.get('cc', '').split(',')
        kwargs['cc'] = [i.strip() for i in kwargs['cc'] if i.strip() != '']
    return True, kwargs,


@decorators.job_wrapper
def run(
    domains=None,
    processes=4,
    to=None,
    cc=[],
    ticket='No Ticket'
):
    domain_q = 'adstxt_crawler_doms'
    output_q = 'adstxt_crawler_output'
    processed = 'adstxt_crawler_processed'
    logger.info('Building out domain queue')
    for domain in domains:
        domain = tldextract.extract(domain).registered_domain
        if domain is None or domain.strip() == '':
            continue
        io.to_redis_queue(domain_q, domain.strip())
    logger.info('Starting celery processes')
    celery_processes = []
    for i in range(processes):
        process = crawler.delay(domain_q, output_q, processed, 5, 3,)
        celery_processes.append(process)
    output = []
    time.sleep(5)
    logger.info('Processing output queue')
    count = 0
    result = io.pop_redis_queue(output_q)
    while result is not None or count < 3:
        result = io.pop_redis_queue(output_q)
        if result is None:
            time.sleep(15)
            count += 1
            continue
        output.append(result)
        count = 0
    logger.info('Adding results to redis cache')
    output = pandas.DataFrame(output)
    output['parsed_data'] = output['parsed_data'].apply(json.dumps)
    io.to_redis(3600, **{'adstxt_crawler/results': output})
    io.delete_redis(domain_q)
    io.delete_redis(output_q)
    io.delete_redis(processed)
    if to is not None:
        logger.info('Emailing using notifier: {0}'.format(to))
        output = output[['domain', 'subdomain', 'has_adstxt', 'has_r1_adstxt', 'is_empty']]
        output['subdomain'] = output['subdomain'].fillna('')
        for column in ['has_adstxt', 'has_r1_adstxt', 'is_empty']:
            output[column] = output[column].fillna(0).astype(int)
        output = output.groupby(list(output.columns)).size().reset_index()
        del output[0]
        excel = BytesIO()
        io.to_excel(excel, **{'ads.txt results': output})
        excel.seek(0)
        mailer = email_notifier.get_mailer_object('basic/blank')
        mailer.attach_file(excel, 'adstxt_crawler_results-{0}.xlsx'.format(datetime.now()))
        mailer.send_mail('[{0}] Ads.txt Crawler Results'.format(ticket), to, cc=[])


if __name__ == '__main__':
    kwargs = create_arguments(settings)
    run(**kwargs)
