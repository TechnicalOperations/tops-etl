DEFAULT_PARAMS = {}

DEPENDENCIES = []

DESCRIPTION = 'Ads.txt Ad-Hoc Crawler'

DISPLAY_NAME = 'Ads.txt Ad-Hoc Crawler'

FIELD_DESCRIPTORS = [
    {
        'name': 'domains',
        'type': 'csv',
        'descriptor': 'List of domains to crawl for ads.txt',
        'required': True
    },
    {
        'name': 'to',
        'type': 'str',
        'required': True,
        'descriptor': 'Who to send the results to'
    },
    {
        'name': 'cc',
        'type': 'str_array',
        'required': False,
        'descriptor': 'Anyone to CC on the results'
    }
]

FREQUENCY = 'MANUALLY'

IS_PUBLIC = True

SCRIPT_NAME = 'adstxt_crawler'

TYPE = 'crawlers'
