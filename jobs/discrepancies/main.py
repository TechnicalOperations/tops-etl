from io import BytesIO
from etl.util import io
from etl.util import db_conn
from etl.logger import get_logger
from etl.notification import email_notifier
from jobs import decorators
from jobs.discrepancies import settings


@decorators.validator_common
def param_validator(**kwargs):
    items = ['placement_ids', 'start_date', 'end_date', 'ssp_ids', 'pub_ids', 'to', 'cc']
    if len(set(items) & set(kwargs.keys())) < len(items):
        return False, dict({'status': 'fail', 'message': 'Arguments missing in parameters'}),
    if 'to' not in kwargs:
        return False, dict({'status': 'fail', 'message': 'No recipient for results'})
    if '@rhythmone.com' not in kwargs['to']:
        return False, dict({'status': 'fail', 'message': 'Only RhythmOne email addresses permitted'})
    kwargs['placement_ids'] = kwargs['placement_ids'].split(',')
    kwargs['placement_ids'] = [i.strip() for i in kwargs['placement_ids'] if i.strip() != '']
    kwargs['ssp_ids'] = kwargs['ssp_ids'].split(',')
    kwargs['ssp_ids'] = [i.strip() for i in kwargs['ssp_ids'] if i.strip() != '']
    kwargs['pub_ids'] = kwargs['pub_ids'].split(',')
    kwargs['pub_ids'] = [i.strip() for i in kwargs['pub_ids'] if i.strip() != '']
    kwargs['cc'] = kwargs.get('cc', '').split(',')
    kwargs['cc'] = [i.strip() for i in kwargs['cc'] if i.strip() != '']
    return True, kwargs,


@decorators.job_wrapper
def run(
        start_date=None,
        end_date=None,
        ssp_ids=None,
        pub_ids=None,
        placement_ids=None,
        to=None,
        cc=[],
        **kwargs
    ):
    logger = get_logger(settings.SCRIPT_NAME)
    conn = db_conn.DbConnection('snowflake')
    logger.info('Fetching impression data')
    data = conn.get_frame(
        'discrepancies/res_imp',
        redis=False,
        start_date=start_date,
        end_date=end_date
    )
    logger.info('Filter data to specified values')
    if ssp_ids is not None and len(ssp_ids) > 0:
        logger.info('Filtering SSPs')
        data = data[data['ssp_id'].isin(ssp_ids)]
    if pub_ids is not None and len(pub_ids) > 0:
        logger.info('Filtering publishers')
        data = data[data['pub_id'].isin(pub_ids)]
    if placement_ids is not None and len(placement_ids) > 0:
        logger.info('Filtering placements')
        data = data[data['plac_id'].isin(placement_ids)]

    if to is not None:
        logger.info("Sending out email notification")
        mailer = email_notifier.get_mailer_object('discrepancies/template')
        mailer.set_formatter({
            'ssps': ssp_ids,
            'pubs': pub_ids,
            'placements': placement_ids,
            'start_date': start_date,
            'end_date': end_date
        })
        output = BytesIO()
        io.to_excel(output, **{'Impressions': data})
        output.seek(0)
        mailer.attach_file(
            output,
            'impressions discrepancy report.xlsx'
        )
        mailer.send_mail(
            'Impressions Discrepancy Initial Report',
            to,
            cc=cc
        )

    return data


if __name__ == '__main__':
    import os
    import pandas
    import argparse
    pandas.set_option('display.width', 5000)
    parser = argparse.ArgumentParser()
    parser.add_argument('--start_date')
    parser.add_argument('--end_date')
    parser.add_argument('--ssp_ids', nargs='*', default=None)
    parser.add_argument('--pub_ids', nargs='*', default=None)
    parser.add_argument('--to')
    parser.add_argument('--placement_ids', nargs='*', default=None)
    args = parser.parse_args()
    output = run(
        start_date=args.start_date,
        end_date=args.end_date,
        ssp_ids=args.ssp_ids,
        pub_ids=args.pub_ids,
        placement_ids=args.placement_ids,
        to=args.to
    )
    output.to_excel(os.path.expanduser('~')+'/Documents/data/output.xlsx', index=False)
