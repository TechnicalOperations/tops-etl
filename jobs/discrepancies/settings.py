DEFAULT_PARAMS = {}

DEPENDENCIES = []

DESCRIPTION = 'Impression Discrepancies'

DISPLAY_NAME = 'Discrepancy'

FIELD_DESCRIPTORS = [
    {
        'name': 'start_date',
        'type': 'datetime_str',
        'descriptor': 'Start date for data search',
        'required': True
    },
    {
        'name': 'end_date',
        'type': 'datetime_str',
        'descriptor': 'End date for data search',
        'required': True
    },
    {
        'name': 'ssp_ids',
        'type': 'str_array',
        'descriptor': 'List of SSPs to look for',
        'required': True
    },
    {
        'name': 'pub_ids',
        'type': 'str_array',
        'descriptor': 'List of publisher IDs to look for',
        'required': True
    },
    {
        'name': 'placement_ids',
        'type': 'str_array',
        'descriptor': 'List of site IDs to look for',
        'required': True
    },
    {
        'name': 'to',
        'type': 'str',
        'required': True,
        'descriptor': 'Who to send the results to'
    },
    {
        'name': 'cc',
        'type': 'str_array',
        'required': False,
        'descriptor': 'Anyone to CC on the results'
    }
]

FREQUENCY = 'MANUALLY'

IS_PUBLIC = True

SCRIPT_NAME = 'discrepancies'

TYPE = 'data_tools'
