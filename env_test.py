from etl.util import io
from etl.util import db_conn
from etl.notification import slack_notifier


conn = db_conn.DbConnection('snowflake')
io.to_redis(5, test_value='test_value')
slack_notifier.send_message('Testing environment')
