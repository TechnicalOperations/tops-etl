SELECT DATE_TRUNC(HOUR, event_time_est) AS date,
    ssp_id,
    ssp_request:user:ext:consent::string AS consent,
    COUNT(DISTINCT IFF(
            media_type = 'site',
            ssp_request:user:buyeruid::string,
            COALESCE(ssp_request:device:ifa::string,
                ssp_request:device:didsha1::string,
                ssp_request:device:didmd5::string,
                ssp_request:device:dpidsha1::string,
                ssp_request:device:dpidmd5::string,
                ssp_request:device:macsha1::string,
                ssp_request:device:macmd5::string)
        )) AS unique_users,
    SUM(sample_rate)::decimal AS requests
FROM ods.rx.bidrequest
WHERE event_time_est >= DATEADD(HOUR, -1, DATE_TRUNC(DAY, current_timestamp()))
    AND event_time_est > DATE_TRUNC(HOUR, current_timestamp())
GROUP BY 1, 2, 3