WITH reqs AS (
    SELECT request_id,
        dsps
    FROM ods.rx.bidrequest
    WHERE event_time >= DATEADD(DAY, -5, '{date}')
        AND event_time < DATEADD(DAY, 5, '{date}')
        AND pub_id IN ('{pubs}')
        AND ssp_id LIKE '%rmp%'
)
SELECT DATE_TRUNC(DAY, DATEADD(HOUR, -5, utc_time)) AS date,
    C.dsps,
    B.name AS ssp_id,
    A.raw_json:request:checkset::string AS checkset,
    A.raw_json:filter_report:source:rating::string AS source_rating,
    GET(A.raw_json:filter_report:source, 'rating-override')::boolean AS rating_override,
    A.raw_json:filter_report:user:blacklistHit::boolean AS user_blacklist,
    A.raw_json:filter_report:segments:blacklistHit::boolean AS segments_blacklist,
    A.raw_json:filter_report:source:blacklistHit::boolean AS source_blacklist,
    A.raw_json:filter_report:blacklistHit::boolean AS overall_blacklist,                    ----
    A.raw_json:request:pubID::string AS pub_id,
    A.raw_json:results:clientaudit:iasRate::decimal(10, 7) * 100.0 AS ias_rate,
    A.raw_json:results:clientaudit:dvRate::decimal(10, 7) * 100.0 AS dv_rate,
    // A.raw_json:request:ip::string AS user_ip,
    IFF(
        A.raw_json:request:siteID::string = 'unknown',
        A.raw_json:request:appID::string,
        A.raw_json:request:siteID::string
    ) AS placement_id,
    IFF(
        A.raw_json:request:domain::string = 'unknown',
        A.raw_json:request:appBundle::string,
        A.raw_json:request:domain::string
    ) AS bundle_domain,
    A.raw_json:results:grapeshot:grapeshot_result AS gs_results,
    A.raw_json:results:blocklist AS blocklist_filter,
    A.raw_json:results:domains AS domains_filter,
    A.raw_json:results:ip AS ip_filter,
    A.raw_json:results:useragent AS ua_filter,
    COUNT(1) AS count
FROM ods.rg.reports_rx_raw AS A
LEFT JOIN ods.rx.ssp AS B ON A.raw_json:request:sspID::string = B.id::string
LEFT JOIN reqs AS C ON A.raw_json:request:requestID::string = C.request_id
WHERE A.utc_time >= DATEADD(DAY, -5, '{date}')
    AND A.utc_time < DATEADD(DAY, 5, '{date}')
    AND A.raw_json:request:pubID::string IN ('{pubs}')
    AND B.name LIKE '%rmp%'
    AND A.raw_json:request:checkset::string ILIKE '%request%'
    AND A.raw_json:request:forceReport::boolean
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20