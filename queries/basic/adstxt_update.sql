select *
from (
    select t.domain, --ads_txt,
    t.has_adstxt, t.has_r1_adstxt
    from ods.tp.ADSTXT_CRAWLER t
    inner join (
        select domain, max(event_time) as MaxDate
        from ods.tp.ADSTXT_CRAWLER
        group by domain
        ) tm on t.domain = tm.domain
    and t.event_time = tm.MaxDate
    where t.adstxt is not null
    and t.adstxt is not null
    and LENGTH(trim(t.adstxt)) > 4
    and LOWER(trim(t.adstxt)) not like '%data-ad-client=%'
    and LOWER(trim(t.adstxt)) not like '%</div>%'
    and LOWER(trim(t.adstxt)) not like '%file not found%'
    and LOWER(trim(t.adstxt)) not like '%file does not exist%'
    and LOWER(trim(t.adstxt)) not like '%page not found%'
    and LOWER(trim(t.adstxt)) not like '%site doesn\'t exist%'
    and LOWER(trim(t.adstxt)) not like '%fatal error%'
    and LOWER(trim(t.adstxt)) not like '%error in exception handler'
    and LOWER(trim(t.adstxt)) not like '%module: not found%'
    and LOWER(trim(t.adstxt)) not like 'controller file%'
    and LOWER(trim(t.adstxt)) not like '%class does not exist%'
    and LOWER(trim(t.adstxt)) not like 'user-agent: *%'
    and LOWER(trim(t.adstxt)) not like 'unrecognized hostname%'
    and LOWER(trim(t.adstxt)) not like 'swn-webproxy03%'
    and LOWER(trim(t.adstxt)) not like 'sayn webmaster%'
    and LOWER(trim(t.adstxt)) not like 'not found%'
    and LOWER(trim(t.adstxt)) not like 'invalid quizz id%'
    and LOWER(trim(t.adstxt)) not like '<?php%'
    and LOWER(trim(t.adstxt)) not like '404'
    and LOWER(trim(t.adstxt)) not like '404 error%'
    and LOWER(trim(t.adstxt)) not like 'error'
    and LOWER(trim(t.adstxt)) not like 'an error has occured%'
    and LOWER(trim(t.adstxt)) not like 'error in exception handler%'
    and LOWER(trim(t.adstxt)) not like 'bad url'
    and LOWER(trim(t.adstxt)) not like '%<h1>%'
    and LOWER(trim(t.adstxt)) not like '%<h3>%'
    and LOWER(trim(t.adstxt)) not like '%href=%'
    and LOWER(trim(t.adstxt)) not like '%under construction%'
  )
;