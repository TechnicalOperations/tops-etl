WITH pubs AS (
    SELECT pub_source, pub_id, site_id
    FROM ods.rx.bidrequest
    WHERE event_time_est >= '{start_date}'
        AND event_time_est < '{end_date}'
        AND ssp_id = 'rmpssp'
    GROUP BY 1, 2, 3
),
wins AS (
    SELECT event_date, pub_id, media_type, SUM(impression_pixel) AS imps, SUM(revenue) AS revenue
    FROM r1_edw.rx.impression_d_est
    WHERE event_date >= '{start_date}'
        AND event_date < '{end_date}'
        AND ssp_id = 'rmpssp'
        AND (pub_id IN (SELECT DISTINCT pub_id FROM pubs)
            OR site_id IN (SELECT DISTINCT site_id FROM pubs))
    GROUP BY event_date, pub_id, media_type
),
responses AS (
    WITH items AS (
        SELECT DATE_TRUNC('DAY', A.event_time_est) AS event_date,
            A.request_id,
            A.pub_id,
            A.media_type,
            MAX(B.rx_sample_rate) AS responses
        FROM ods.rx.bidrequest AS A
        INNER JOIN ods.rx.bidresponse AS B ON A.request_id=B.rx_request_id
        WHERE A.event_time_est >= '{start_date}'
            AND A.event_time_est < '{end_date}'
            AND B.event_time_est >= '{start_date}'
            AND B.event_time_est < '{end_date}'
            AND A.ssp_id = 'rmpssp'
            AND (A.pub_id IN (SELECT DISTINCT pub_id FROM pubs)
                OR A.site_id IN (SELECT DISTINCT site_id FROM pubs))
        GROUP BY event_date, A.request_id, A.pub_id, A.media_type
    )
    SELECT event_date, pub_id, media_type, SUM(responses) AS responses
    FROM items
    GROUP BY event_date, pub_id, media_type

),
matches AS (
    SELECT DATE_TRUNC('DAY', event_time_est) AS event_date,
        pub_id,
        media_type,
        SUM(IFF(
            media_type = 'site' AND ssp_request:user:buyeruid::string LIKE 'RX-%',
            sample_rate,
            IFF(ssp_request:device:ifa::string IS NOT NULL
                OR ssp_request:device:didsha1::string IS NOT NULL
                OR ssp_request:device:didmd5::string IS NOT NULL
                OR ssp_request:device:dpidsha1::string IS NOT NULL
                OR ssp_request:device:dpidmd5::string IS NOT NULL
                OR ssp_request:device:macsha1::string IS NOT NULL
                OR ssp_request:device:macmd5::string IS NOT NULL,
                sample_rate,
                0)
        )) AS user_matches,
        SUM(sample_rate) AS requests
    FROM ods.rx.bidrequest
    WHERE event_time_est >= '{start_date}'
        AND event_time_est < '{end_date}'
        AND ssp_id = 'rmpssp'
        AND (pub_id IN (SELECT DISTINCT pub_id FROM pubs)
            OR site_id IN (SELECT DISTINCT site_id FROM pubs))
    GROUP BY event_date, pub_id, media_type
)
SELECT B.event_date AS date, B.pub_id AS publisher, D.pub_source AS ssp, A.media_type,
    B.user_matches AS user_matches,
    B.requests, C.responses, A.imps AS impression_pixels, A.revenue AS revenue
FROM matches AS B
LEFT JOIN wins AS A ON A.event_date=B.event_date
                    AND A.pub_id=B.pub_id
                    AND A.media_type=B.media_type
LEFT JOIN responses AS C ON C.event_date=B.event_date
                    AND C.pub_id=B.pub_id
                    AND C.media_type=B.media_type
LEFT JOIN pubs AS D ON B.pub_id=D.pub_id
