WITH serve AS (
    SELECT event_date AS date,
        dsp_id,
        ssp_id,
        pub_id,
        IFF(media_type = 'app', app_id, site_id) AS placement_id,
    --    site_name,
        IFF(TRIM(rg_blocked_reason) = '-', rg_blocked, rg_blocked_reason) AS filter_reason,
        SUM(impression_serve)::decimal AS impressions
    FROM r1_edw.rx.impression_serve_d_est
    WHERE ssp_id LIKE '%rmp%'
        AND event_date >= '{start_date}'
        AND event_date < '{end_date}'
    GROUP BY 1, 2, 3, 4, 5, 6 -- , 7
),
pixels AS (
    SELECT event_date AS date,
        dsp_id,
        ssp_id,
        pub_id,
        IFF(media_type = 'app', app_id, site_id) AS placement_id,
    --    site_name,
        IFF(TRIM(rg_blocked_reason) = '-', rg_blocked, rg_blocked_reason) AS filter_reason,
        SUM(impression_pixel)::decimal AS impressions
    FROM r1_edw.rx.impression_pixel_d_est
    WHERE ssp_id LIKE '%rmp%'
        AND event_date >= '{start_date}'
        AND event_date < '{end_date}'
    GROUP BY 1, 2, 3, 4, 5, 6 -- , 7
)
SELECT A.date,
    A.dsp_id,
    A.ssp_id,
    A.pub_id,
    A.placement_id,
    A.filter_reason,
    A.impressions AS impressions_serve,
    IFF(B.impressions IS NULL, 0, B.impressions)::decimal AS billable_impressions
FROM serve AS A
LEFT JOIN pixels AS B ON A.date = B.date
    AND A.dsp_id = B.dsp_id
    AND A.ssp_id = B.ssp_id
    AND A.pub_id = B.pub_id
    AND A.placement_id = B.placement_id
    AND A.filter_reason = B.filter_reason