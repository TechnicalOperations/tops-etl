SELECT event_date AS date,
    rx_dsp_name AS dsp_id,
    rx_ssp_name AS ssp_id,
    pub_id,
    IFF(rx_media_type = 'app', app_id, site_id) AS placement_id,
--    site_name,
    IFF(TRIM(rx_response_status) = '-', 'notFiltered', rx_response_status) AS filter_reason,
    SUM(dsp_bids)::decimal AS bids
FROM r1_edw.rx.bidresponse_d_est
WHERE ssp_id LIKE '%rmp%'
    AND event_date >= '{start_date}'
    AND event_date < '{end_date}'
GROUP BY 1, 2, 3, 4, 5, 6 -- , 7