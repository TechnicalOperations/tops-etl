WITH ssps AS (
  SELECT id AS ssp, name AS ssp_id
  FROM ods.rx.ssp
  WHERE name LIKE '%rmp%'
  GROUP BY 1, 2
),
reports AS (
    SELECT DATE_TRUNC(DAY, utc_time) AS date,
        B.ssp_id AS ssp_id,
        A.raw_json:request:pubID::string AS pub_id,
        IFF(
            A.raw_json:request:mediaType::string = 'app',
            A.raw_json:request:appID::string,
            A.raw_json:request:siteID::string
        ) AS placement_id,
        A.raw_json:request:domain::string AS site_name,
        A.raw_json:filter_report:source:rating::string AS source_rating,
        A.raw_json:request:checkset::string AS checkset,
        CASE WHEN A.raw_json:results:grapeshot:grapeshot_result:channels IS NULL
                OR ARRAY_SIZE(PARSE_JSON(A.raw_json:results:grapeshot:grapeshot_result:channels)) = 0
                THEN 'Grapeshot: No Results'
            WHEN A.raw_json:results:grapeshot:grapeshot_result:channels LIKE '%"gv_%'
                THEN 'Grapeshot Unsafe: ' || A.raw_json:results:grapeshot:grapeshot_result:channels::string
            ELSE 'Valid GS Results'
            END AS grapeshot,
        CASE WHEN A.raw_json:results:domains:blacklist IS NULL
                OR ARRAY_SIZE(PARSE_JSON(A.raw_json:results:domains:blacklist)) = 0
                THEN 'No Blacklist'
             ELSE 'Blacklist hit: ' || A.raw_json:results:domains:blacklist::string
             END AS blacklist,
        CASE WHEN A.raw_json:results:domains:whitelist IS NULL
                OR ARRAY_SIZE(PARSE_JSON(A.raw_json:results:domains:whitelist)) = 0
                THEN 'No Whitelist'
             ELSE 'Whitelist hit: ' || A.raw_json:results:domains:whitelist::string
             END AS whitelist,
        CASE WHEN A.raw_json:results:ip IS NULL
                OR ARRAY_SIZE(PARSE_JSON(A.raw_json:results:ip)) = 0
                THEN 'Valid IP'
             ELSE 'Invalid IP: ' || A.raw_json:results:ip::string
             END AS ip,
        COUNT(1) AS count
    FROM ods.rg.reports_rx_raw AS A
    INNER JOIN ssps AS B ON A.raw_json:request:sspID::string=B.ssp
    WHERE utc_time >= DATEADD(HOUR, -8, '{start_date}')
        AND utc_time < DATEADD(HOUR, 8, '{end_date}')
        AND A.raw_json:request:checkset::string LIKE '%BidRequest%'
    //    AND A.raw_json:filter_report:source:rating::string in ('low', 'unsafe')
        AND A.raw_json:request:forceReport::boolean
    //    AND A.raw_json:request:pubID::string IN ('{{pubs}}')
    //    AND A.raw_json:request:siteID::string IN ('{{sites}}')
    GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
)
SELECT DATE_TRUNC(DAY, CONVERT_TIMEZONE('America/New_York', date)::timestamp_ntz) AS date,
    ssp_id,
    pub_id,
    placement_id,
    site_name,
    source_rating,
    checkset,
    grapeshot,
    blacklist,
    whitelist,
    ip,
    count
FROM reports
WHERE date >= DATE_TRUNC(DAY, CONVERT_TIMEZONE('America/New_York', '{start_date}'::timestamp_ntz))
    AND date < DATE_TRUNC(DAY, CONVERT_TIMEZONE('America/New_York', '{end_date}'::timestamp_ntz))
