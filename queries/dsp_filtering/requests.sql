WITH fails AS (
    SELECT DATE_TRUNC(DAY, A.event_time_est) AS date,
        A.ssp_id,
        A.pub_id,
        IFF(A.media_type = 'app', A.app_id, A.site_id) AS placement_id,
--        A.site_name,
        B.key::string AS dsp_id,
        B.value::string AS filter_reason,
        SUM(A.sample_rate)::decimal AS requests
    FROM ods.rx.bidrequest AS A,
    LATERAL FLATTEN(INPUT=>PARSE_JSON(dsp_filter)) B
    WHERE A.ssp_id LIKE '%rmp%'
        AND A.event_time_est >= '{start_date}'
        AND A.event_time_est < '{end_date}'
        AND A.request_fail IN ('nodspbids', 'bidresponse', '-', 'nodsp')
    GROUP BY 1, 2, 3, 4, 5, 6 -- , 7
),
totals AS (
    SELECT event_date AS date,
        A.ssp_id,
        A.pub_id,
        IFF(A.media_type = 'app', A.app_id, A.site_id) AS placement_id,
--        A.site_name,
        B.value::string AS dsp_id,
        'notFiltered' AS filter_reason,
        SUM(A.requests)::decimal AS requests
    FROM r1_edw.rx.bidrequest_d_est AS A,
    LATERAL FLATTEN(INPUT=>PARSE_JSON(IFF(dsps = '-', '[]', dsps))) B
    WHERE A.ssp_id LIKE '%rmp%'
        AND A.event_date >= '{start_date}'
        AND A.event_date < '{end_date}'
        AND A.request_fail IN ('nodspbids', 'bidresponse', '-', 'nodsp')
    GROUP BY 1, 2, 3, 4, 5, 6 -- , 7
),
un AS (
    SELECT A.date,
        A.dsp_id,
        A.ssp_id,
        A.pub_id,
        A.placement_id,
    --    A.site_name,
        A.filter_reason,
        A.requests
    FROM fails AS A
    UNION
    SELECT B.date,
        B.dsp_id,
        B.ssp_id,
        B.pub_id,
        B.placement_id,
    --    B.site_name,
        B.filter_reason,
        B.requests
    FROM totals AS B
)
SELECT date,
    dsp_id,
    ssp_id,
    pub_id,
    placement_id,
--    A.site_name,
    filter_reason,
    SUM(requests) AS requests
FROM un
GROUP BY 1, 2, 3, 4, 5, 6 -- 7