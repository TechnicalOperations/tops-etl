SELECT ssp_id,
    pub_id,
    IFF(A.media_type = 'app', A.app_id, A.site_id) AS placement_id,
    imp_type,
    media_type,
    device,
    country_code
FROM r1_edw.rx.bidrequest_d_est
WHERE event_date >= '{start_date}'
    AND event_date < '{end_date}'
    AND pub_id IN ('{pubs}')
    AND placement_id IN ('{placements}')
GROUP BY 1, 2, 3, 4, 5, 6, 7