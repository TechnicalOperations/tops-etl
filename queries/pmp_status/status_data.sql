WITH imps AS (
    SELECT DATE_TRUNC('DAY', A.event_date) AS imp_date,
            A.rx_deal_id AS deal_id,
            -- A.dsp_seat AS seat,
            A.rx_ssp_name AS ssp_id,
            A.rx_dsp_name AS dsp_id,
            SUM(impression_win) AS impressions,
            SUM(revenue) AS revenue,
            SUM(revenue - cost) AS margin
    FROM r1_edw.rx.impression_d_est AS A
    WHERE A.event_date >= '{start_date}' AND A.event_date < '{end_date}'
        AND (A.rx_deal_id != '-') -- OR (A.dsp_seat = '1058' AND dsp_id = 'turn'))
    GROUP BY imp_date, A.rx_deal_id, A.rx_ssp_name, A.rx_dsp_name --, A.dsp_seat
),
bids AS (
    SELECT DATE_TRUNC('DAY', A.event_time_est) AS date,
            E.value::varchar AS deal_id,
            A.rx_ssp_name AS ssp_id,
            A.rx_dsp_name AS dsp_id,
            SUM(IFF(A.rx_request_id IS NOT NULL, A.rx_sample_rate::decimal, 0)) AS requests,
            SUM(IFF(CONTAINS(E.value, B.rx_deal_id) AND E.value IS NOT NULL, A.rx_sample_rate::decimal, 0)) AS bid_received,
            SUM(IFF(NOT(CONTAINS(E.value, B.rx_deal_id)) AND B.rx_deal_id IS NOT NULL, A.rx_sample_rate::decimal, 0)) AS lost_other
    FROM rx.dspbidrequest AS A
    LEFT JOIN rx.bidresponse AS B ON A.rx_request_id=B.rx_request_id
                            AND A.rx_dsp_name=B.rx_dsp_name,
    LATERAL FLATTEN(input=>PARSE_JSON(A.rx_deal_ids), outer=>TRUE) E
    WHERE A.event_time_est >= '{start_date}' AND A.event_time_est < '{end_date}'
        AND (B.event_time_est >= '{start_date}' OR B.event_time_est IS NULL)
        AND (B.event_time_est < '{end_date}' OR B.event_time_est IS NULL)
    GROUP BY date, E.value, A.rx_ssp_name, A.rx_dsp_name
)
SELECT IFF(A.date IS NULL, B.imp_date, A.date) AS date,
       A.deal_id,
       IFF(A.ssp_id IS NULL, B.ssp_id, A.ssp_id) AS ssp_id,
       IFF(A.dsp_id IS NULL, B.dsp_id, A.dsp_id) AS dsp_id,
       -- B.seat,
       A.requests,
       A.bid_received,
       A.lost_other,
       B.impressions,
       B.revenue,
       B.margin
FROM bids AS A
FULL OUTER JOIN imps AS B ON A.date=B.imp_date
                         AND (A.deal_id=B.deal_id OR (A.deal_id IS NULL AND B.deal_id='-'))
                         AND A.ssp_id=B.ssp_id
                         AND A.dsp_id=B.dsp_id
WHERE A.deal_id IS NOT NULL -- OR B.seat = '1058'