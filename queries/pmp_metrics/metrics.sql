WITH ivt_view AS (
  SELECT A.rx_deal_id AS deal_id,
      A.rx_media_type AS media_type,
      DATE_TRUNC(DAY, A.event_date) AS date,
      SUM(dsp_bids) AS bids
  FROM r1_edw.rx.bidresponse_d_est AS A
  WHERE A.event_date >= '{start_date}'
      AND A.event_date < '{end_date}'
      AND A.rx_deal_id IN ('{deal_ids}')
  GROUP BY 1, 2, 3
),
reqs AS (
  SELECT B.value::string AS deal_id,
      A.media_type,
      DATE_TRUNC(DAY, A.event_date) AS date,
      SUM(requests) AS requests
  FROM r1_edw.rx.bidrequest_d_est AS A,
  LATERAL FLATTEN(INPUT=>PARSE_JSON(IFF(deal_ids = '-', '[]', deal_ids))) B
  WHERE A.event_date >= '{start_date}'
      AND A.event_date < '{end_date}'
      AND B.value::string IN ('{deal_ids}')
      AND B.value::string != '-'
  GROUP BY 1, 2, 3
),
clicks AS (
  SELECT rx_deal_id AS deal_id,
    rx_media_type AS media_type,
    DATE_TRUNC(DAY, event_date) AS date,
    SUM(click_count) AS clicks
  FROM r1_edw.rx.click_d_est
  WHERE event_date >= '{start_date}'
      AND event_date < '{end_date}'
    AND rx_deal_id IN ('{deal_ids}')
  GROUP BY 1, 2, 3
),
imps AS (
  SELECT deal_id,
    DATE_TRUNC(DAY, event_date) AS date,
    media_type,
    SUM(ias_viewable_impression) * 100.0 / SUM(ias_total_impression) AS avg_viewability,
    SUM(ias_ivt_impression) * 100.0 / SUM(ias_total_impression) AS avg_ivt,
    SUM(impression_pixel) AS impressions,
    SUM(revenue) AS revenue,
    SUM(revenue) * 1000.0 / SUM(impression_pixel) AS ecpm
  FROM r1_edw.rx.impression_d_est
  WHERE event_date >= '{start_date}'
      AND event_date < '{end_date}'
    AND deal_id IN ('{deal_ids}')
    AND impression_pixel > 0
    AND ias_total_impression > 0
  GROUP BY 1, 2, 3
)
SELECT D.deal_id,
    D.date,
    D.requests,
    D.media_type,
    A.bids,
    C.ecpm,
    C.impressions,
    B.clicks,
    C.avg_viewability,
    C.avg_ivt
FROM reqs AS D
LEFT JOIN clicks AS B ON D.deal_id=B.deal_id
    AND D.date=B.date
    AND D.media_type=B.media_type
LEFT JOIN imps AS C ON D.deal_id=C.deal_id
    AND D.date=C.date
    AND D.media_type=C.media_type
LEFT JOIN ivt_view AS A ON A.deal_id=D.deal_id
    AND A.date=D.date
    AND A.media_type=D.media_type