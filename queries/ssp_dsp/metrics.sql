WITH reqs AS (
    SELECT DATE_TRUNC(HOUR, A.event_time_est) AS date,
        A.ssp_id,
        A.imp_type,
        A.media_type,
        A.device,
        B.value::string AS dsp_id,
        'notFiltered' AS filter_reason,
        SUM(sample_rate)::decimal AS requests
    FROM ods.rx.bidrequest AS A,
    LATERAL FLATTEN(input=>IFF(dsps IS NULL, '[]'::variant, dsps)) B
    WHERE event_time_est >= '2018-05-09'
        AND event_time_est < '2018-05-10'
    GROUP BY 1, 2, 3, 4, 5, 6, 7
    UNION
    SELECT DATE_TRUNC(HOUR, A.event_time_est) AS date,
        A.ssp_id,
        A.imp_type,
        A.media_type,
        A.device,
        B.key::string AS dsp_id,
        B.value::string AS filter_reason,
        SUM(A.sample_rate)::decimal AS requests
    FROM ods.rx.bidrequest AS A,
    LATERAL FLATTEN(input=>dsp_filter) B
    WHERE event_time_est >= '2018-05-09'
        AND event_time_est < '2018-05-10'
    GROUP BY 1, 2, 3, 4, 5, 6, 7
),
res AS (
    SELECT event_date AS date,
        ssp_id,
        imp_type,
        media_type,
        device,
        dsp_id,
        SUM(bids) AS responses
    FROM r1_edw.rx.bidresponse_d_est
    WHERE event_date >= '2018-05-09'
        AND event_date < '2018-05-10'
    GROUP BY 1, 2, 3, 4, 5, 6
)
SELECT A.date,
    A.ssp_id,
    A.imp_type,
    A.media_type,
    A.device,
    A.dsp_id,
    A.filter_reason,
    A.requests,
    IFF(B.responses IS NULL, 0, B.responses)::decimal AS responses
FROM reqs AS A
LEFT JOIN res AS B ON A.date=B.date
    AND A.ssp_id=B.ssp_id
    AND A.imp_type=B.imp_type
    AND A.media_type=B.media_type
    AND A.device=B.device
    AND A.dsp_id=B.dsp_id
    AND A.filter_reason='notFiltered'
WHERE A.dsp_id IS NOT NULL