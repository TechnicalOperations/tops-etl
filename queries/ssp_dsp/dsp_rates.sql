WITH reqs AS (
    SELECT A.event_date AS date,
        B.value::string AS dsp_id,
        SUM(A.requests) AS requests
    FROM r1_edw.rx.bidrequest_d_est AS A,
    LATERAL FLATTEN(INPUT=>PARSE_JSON(IFF(dsps = '-', '[]', dsps))) B
    WHERE A.event_date >= '2018-05-09'
        AND A.event_date < '2018-05-10'
    GROUP BY 1, 2
),
res AS (
    SELECT A.event_date AS date,
        A.dsp_id,
        SUM(A.bids) AS responses
    FROM r1_edw.rx.bidresponse_d_est AS A
    WHERE A.event_date >= '2018-05-09'
        AND A.event_date < '2018-05-10'
    GROUP BY 1, 2
)
SELECT A.date,
    A.dsp_id,
    A.requests,
    B.responses
FROM reqs AS A
LEFT JOIN res AS B ON A.date=B.date
    AND A.dsp_id=B.dsp_id