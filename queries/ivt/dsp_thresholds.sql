SELECT B.name, A.user_rating_provider, A.filter_user, A.threshold
FROM dashboard.dsp_user_rating_filter AS A
LEFT JOIN dashboard.dsp AS B ON A.dsp_id=B.id;