WITH reqs AS (
    SELECT event_date AS date,
        ssp_id,
        pub_id,
        IFF(media_type = 'app', app_id, site_id) AS placement_id,
        SUM(IFF(TRIM(ias_rate) = '-', NULL, ias_rate::decimal(15, 5)) * requests) / SUM(requests) AS ias_rate,
        SUM(IFF(TRIM(dv_rate) = '-', NULL, dv_rate::decimal(15, 5)) * requests) / SUM(requests) AS dv_rate,
        SUM(requests)::decimal AS requests
    FROM r1_edw.rx.bidrequest_d_est
    WHERE event_date >= '{start_date}'
        AND event_date < '{end_date}'
        AND ssp_id LIKE '%rmp%'
    GROUP BY 1, 2, 3, 4
),
imps AS (
    SELECT event_date AS date,
        ssp_id,
        pub_id,
        IFF(media_type = 'app', app_id, site_id) AS placement_id,
        SUM(impression_win)::decimal AS wins,
        SUM(impression_pixel)::decimal AS pixels
    FROM r1_edw.rx.impression_d_est
    WHERE event_date >= '{start_date}'
        AND event_date < '{end_date}'
        AND ssp_id LIKE '%rmp%'
    GROUP BY 1, 2, 3, 4
)
SELECT A.date,
    A.ssp_id,
    A.pub_id,
    A.placement_id,
    A.ias_rate,
    A.dv_rate,
    A.requests,
    IFF(B.wins IS NULL, 0, B.wins)::decimal AS wins,
    IFF(B.pixels IS NULL, 0, B.pixels)::decimal AS pixels
FROM reqs AS A
LEFT JOIN imps AS B ON A.date=B.date
    AND A.ssp_id=B.ssp_id
    AND A.pub_id=B.pub_id
    AND A.placement_id=B.placement_id