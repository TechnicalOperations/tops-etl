SELECT A.external_deal_id,
	A.description,
    D.type,
    GROUP_CONCAT(DISTINCT C.name SEPARATOR ',') AS dsps,
    GROUP_CONCAT(DISTINCT H.name SEPARATOR ',') AS ssps,
    GROUP_CONCAT(DISTINCT CONCAT_WS(';;;', F.name, E.viewability_percentage)) AS viewability_percentage,
    GROUP_CONCAT(DISTINCT CONCAT_WS(';;;', J.provider, J.score_threshold)) AS brand_safety,
    GROUP_CONCAT(DISTINCT CONCAT_WS(';;;', L.export_name, K.media_type, K.filter_type, K.group_id)) AS segment,
    A.bidfloor,
    A.filter_ssp,
    A.filter_country,
    A.filter_countrylist,
    A.filter_adsize,
    A.filter_adsize_list,
    A.filter_videosize,
    A.filter_videosize_list,
    A.filter_banner,
    A.filter_video,
    A.filter_native,
    A.filter_display AS filter_desktop,
    A.filter_mobile,
    A.filter_site,
    A.filter_app,
    CASE WHEN A.filter_ads_txt = 1 THEN 'has_r1'
		WHEN A.filter_ads_txt = 2 THEN 'has_r1_or_missing'
        ELSE null END AS ads_txt,
    A.wseat,
    A.filter_inviewpercent,
    A.filter_ctv
FROM dashboard.deal AS A
LEFT JOIN dashboard.deal_dsp AS B ON A.deal_id=B.deal_id
LEFT JOIN dashboard.dsp AS C ON B.dsp_id=C.id
LEFT JOIN dashboard.deal_type AS D ON A.type=D.id
LEFT JOIN dashboard.deal_viewability_filter AS E ON A.deal_id=E.deal_id
LEFT JOIN dashboard.viewability_provider AS F ON E.viewability_provider_id=F.id
LEFT JOIN dashboard.deal_ssp AS G ON A.deal_id=G.deal_id
LEFT JOIN dashboard.ssp AS H ON G.ssp_id=H.id
LEFT JOIN dashboard.deal_safety_filter AS I ON A.deal_id=I.deal_id
LEFT JOIN dashboard.safety_filter AS J ON I.safety_filter_id=J.id
LEFT JOIN dashboard.deal_segment AS K ON A.deal_id=K.deal_id
LEFT JOIN dashboard.segment AS L ON K.segment_id=L.segment_id
WHERE A.active = 1
    AND DATE_SUB(NOW(), INTERVAL 1 DAY) BETWEEN deal_start AND deal_end{0}
GROUP BY A.external_deal_id,
	A.description,
    D.type,
    A.bidfloor,
    A.filter_ssp,
    A.filter_country,
    A.filter_countrylist,
    A.filter_adsize,
    A.filter_adsize_list,
    A.filter_videosize,
    A.filter_videosize_list,
    A.filter_banner,
    A.filter_video,
    A.filter_native,
    A.filter_display,
    A.filter_mobile,
    A.filter_site,
    A.filter_app,
    ads_txt,
    A.wseat,
    A.filter_inviewpercent,
    A.filter_ctv