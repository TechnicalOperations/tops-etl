SELECT B.value::string AS deal_id,
    SUM(A.requests) AS requests
FROM r1_edw.rx.bidrequest_d_est AS A,
LATERAL FLATTEN(INPUT=>PARSE_JSON(deal_ids)) B
WHERE A.event_date >= '{start_date}'
    AND A.event_date < '{end_date}'
    AND A.deal_ids != '-'
    AND A.request_fail IN ('bidresponse-datastoreError', 'bidresponse', '-', 'nodspbids')
GROUP BY 1