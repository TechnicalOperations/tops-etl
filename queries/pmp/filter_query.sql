WITH adstxt_table AS (
  SELECT domain, MAX(has_r1_adstxt::decimal(1, 0))::string AS has_r1_adstxt
  FROM ods.tp.adstxt_crawler
  WHERE event_time >= '{start_date}'
    AND event_time < '{end_date}'
  GROUP BY 1
)
SELECT CASE WHEN B.has_r1_adstxt = '1' THEN 'has_r1'
        WHEN B.has_r1_adstxt IS NULL THEN 'missing'
        ELSE 'no_r1' END AS adstxt,
    CASE WHEN A.ad_size IN ('{adsizes}') THEN A.ad_size
        ELSE NULL END AS ad_size,
    IFF(video_w = '-', 'Unknown',
     IFF(TO_NUMERIC(video_w) <= 300, 'Small',
      IFF(TO_NUMERIC(video_w) <= 719, 'Medium',
       IFF(TO_NUMERIC(video_w) <= 2159, 'Large', 'Extra Large')))
    ) AS video_size,
    -- CASE {{floors}}
    --   ELSE 0.0 END AS bid_floor,
    ias_rate AS ias_rate,
    dv_rate AS dv_rate,
    IFF(country_code IN ('{countries}'), country_code, NULL) AS country,
    IFF(segments = '-',
        ARRAY_CONSTRUCT(),
        PARSE_JSON(segments)
       ) AS segments,
    CASE WHEN ias_viewability::string = '-' THEN NULL
        ELSE GREATEST(
            ZEROIFNULL(GET(PARSE_JSON(ias_viewability), 'adsize-country-domain')::decimal(15, 5)),
            ZEROIFNULL(GET(PARSE_JSON(ias_viewability), 'adsize-country-pubid-siteid-appid')::decimal(15, 5)),
            ZEROIFNULL(GET(PARSE_JSON(ias_viewability), 'pubid-adsize')::decimal(15, 5)),
            ZEROIFNULL(GET(PARSE_JSON(ias_viewability), 'score')::decimal(15, 5)),
            ZEROIFNULL(GET(PARSE_JSON(ias_viewability), 'sspid-pubid-siteid-appid')::decimal(15, 5))
        ) END AS viewability,
    media_type,
    imp_type,
    ssp_id,
    dsps,
    CASE WHEN TRIM(device) = '-' THEN 'unknown'
        ELSE device END AS device_type,
    SUM(A.requests) AS requests
FROM r1_edw.rx.bidrequest_d_est AS A
LEFT JOIN adstxt_table AS B ON A.site_domain=B.domain
WHERE A.event_date >= '{start_date}'
    AND A.event_date < '{end_date}'
    AND request_fail IN ('bidresponse-datastoreError', 'bidresponse', '-', 'nodspbids')
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 -- , 14