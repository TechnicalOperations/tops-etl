SELECT site_domain,
    site_name AS domain,
    IFF(has_adstxt IS NULL, 0, has_adstxt)::decimal AS has_adstxt,
    IFF(has_r1_adstxt IS NULL, 0, has_r1_adstxt)::decimal AS has_r1_adstxt,
    SUM(sample_rate)::decimal AS requests
FROM ods.rx.bidrequest
WHERE event_time_est >= '{start_date}'
    AND event_time_est < '{end_date}'
    AND media_type = 'site'
    AND has_adstxt::decimal = 1
GROUP BY 1, 2, 3 ,4