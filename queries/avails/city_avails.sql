SELECT DATE_TRUNC(DAY, A.event_time_est) AS date,
    CASE WHEN A.has_r1_adstxt::decimal = 1 THEN 'has_r1_adstxt'
        WHEN A.has_adstxt::decimal = 1 THEN 'has_adstxt'
        ELSE 'adstxt_missing' END AS adstxt,
    IFF(A.media_type = 'site', LOWER(A.site_name), A.app_id) AS placement_id,
    LOWER(A.ssp_request:user:geo:city::string) AS city,
    LOWER(A.ssp_request:user:geo:region::string) AS region,
    IFF(A.imp_type = 'video', A.ssp_request:imp[0]:video:protocols, NULL) AS protocols,
    IFF(A.imp_type = 'video', A.ssp_request:imp[0]:video:api, A.ssp_request:imp[0]:banner:api) AS api,
    A.ssp_id,
    A.imp_type,
    A.media_type,
    A.device,
    A.dsps,
    SUM(A.sample_rate)::decimal AS requests
FROM ods.rx.bidrequest AS A
WHERE A.event_time_est >= '{start_date}'
    AND A.event_time_est < '{end_date}'
    AND (
        A.request_fail IN ('bidresponse-datastoreError', 'bidresponse', 'nodspbids', '-')
        OR A.request_fail IS NULL
    )
    {cities}
    {protocols}
    {api}
    {dsps}
    {imp_type}
    {media_type}
    {device}
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12