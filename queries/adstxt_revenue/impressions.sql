SELECT event_date AS date, site_name AS domain, dsp_id AS dsp, dsp_seat AS seat, SUM(revenue) AS revenue, SUM(cost) AS cost
FROM r1_edw.rx.impression_d_est
WHERE event_date >= '{date}'
GROUP BY event_date, site_name, dsp_id, dsp_seat