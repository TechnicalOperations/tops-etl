SELECT DATE_TRUNC(DAY, A.event_time) AS date,
  A.domain,
  MAX(IFF(
    LOWER(B.value:seller::string) LIKE '%1rx%' OR LOWER(B.value:seller::string) LIKE '%rhythmone%',
    1, 0))::decimal AS has_r1,
  MAX(IFF((LOWER(B.value:seller::string) LIKE '%1rx%' OR LOWER(B.value:seller::string) LIKE '%rhythmone%')
    AND UPPER(B.value:relationship::string) = 'DIRECT', 1, 0))::decimal AS has_r1_direct
FROM ods.tp.adstxt_crawler AS A,
LATERAL FLATTEN(INPUT=>A.parsed_data) B
WHERE event_time >= '{date}'
GROUP BY 1, 2