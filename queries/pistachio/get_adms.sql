WITH crids AS (
  SELECT A.pub_id,
      A.crid,
      COUNT(1) AS total
  FROM ods.rx.impbeacon AS A
  WHERE A.event_time_est >= DATEADD(HOUR, -3, DATE_TRUNC(DAY, current_timestamp()))
      AND A.event_time_est < DATE_TRUNC(HOUR, current_timestamp())
      AND A.imp_type = 'banner'
      AND A.adm NOT LIKE '%"adm"%'
      AND A.ssp_id LIKE '%rmp%'
  GROUP BY 1, 2
  ORDER BY 3 DESC
  LIMIT 30
)
SELECT A.pub_id,
    A.crid,
    A.imp_type,
    SPLIT(A.ad_size, 'x')[0]::string AS width,
    SPLIT(A.ad_size, 'x')[1]::string AS height,
    ANY_VALUE(A.adm) AS adm,
    B.total
FROM ods.rx.impbeacon AS A
JOIN crids AS B ON A.crid=B.crid AND A.pub_id=B.pub_id
WHERE A.event_time_est >= DATEADD(HOUR, -3, DATE_TRUNC(DAY, current_timestamp()))
    AND A.event_time_est < DATE_TRUNC(HOUR, current_timestamp())
    AND A.imp_type = 'banner'
    AND A.adm NOT LIKE '%"adm"%'
    AND A.ssp_id LIKE '%rmp%'
GROUP BY 1, 2, 3, 4, 5, 7