SELECT A.uuid,
    A.crid,
    A.cat,
    A.attr,
    A.width,
    A.height,
    A.adm,
    B.har_content
FROM pistachio.data AS A
LEFT JOIN pistachio.log AS B ON A.uuid=B.uuid
WHERE A.uuid = '{uuid}'
