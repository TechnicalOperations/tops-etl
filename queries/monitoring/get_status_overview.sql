SELECT job_name, state, ticket, COUNT(DISTINCT uuid) AS count
FROM etl.status
WHERE state IN ('completed', 'error', 'hung/terminated')
    AND started >= DATE_SUB(UTC_TIMESTAMP(), INTERVAL {days} DAY)
GROUP BY job_name, state, ticket
UNION
SELECT job_name, state, ticket, COUNT(DISTINCT uuid) AS count
FROM etl.status
WHERE state IN ('processing', 'queue')
    AND started >= DATE_SUB(UTC_TIMESTAMP(), INTERVAL {days} DAY)
    AND uuid NOT IN (
        SELECT DISTINCT uuid
        FROM etl.status
        WHERE state IN ('completed', 'error', 'hung/terminated')
            AND started >= DATE_SUB(UTC_TIMESTAMP(), INTERVAL {days} DAY)
    )
GROUP BY job_name, state, ticket