CREATE TABLE status (
    id INT PRIMARY KEY AUTO_INCREMENT,
    uuid CHAR(36),
    job_name VARCHAR(255),
    ticket VARCHAR(15) DEFAULT 'No Ticket',
    initiator VARCHAR(15),
    state VARCHAR(20),
    queued DATETIME,
    started DATETIME,
    ended DATETIME,
    stack LONGTEXT,
    params MEDIUMTEXT,
    INDEX uuid_lu USING BTREE (uuid)
)