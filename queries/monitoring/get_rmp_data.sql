WITH req_res AS (
    SELECT event_time_hour AS date,
        ssp_id,
        pub_id,
        site_id,
        dsp_id,
        SUM(requests)::decimal AS requests,
        SUM(bids_received)::decimal AS bids
    FROM r1_edw.rx.bidreqres_h_utc
    WHERE event_time_hour >= '{start_date}'
        AND event_time_hour < '{end_date}'
        AND ssp_id LIKE '%rmp%'
    GROUP BY 1, 2, 3, 4, 5
),
imps AS (
    SELECT event_time_hour AS date,
        ssp_id,
        pub_id,
        site_id,
        dsp_id,
        SUM(impression_win)::decimal AS wins,
        SUM(impression_pixel)::decimal AS pixels,
        SUM(revenue) AS revenue,
        SUM(revenue - cost) AS margin
    FROM r1_edw.rx.impression_h_utc
    WHERE event_time_hour >= '{start_date}'
        AND event_time_hour < '{end_date}'
        AND ssp_id LIKE '%rmp%'
    GROUP BY 1, 2, 3, 4, 5
)
SELECT A.date,
    A.ssp_id,
    A.pub_id,
    A.site_id,
    A.dsp_id,
    A.requests,
    A.bids,
    B.wins,
    B.pixels,
    B.revenue,
    B.margin
FROM req_res AS A
LEFT JOIN imps AS B ON A.date=B.date
    AND A.ssp_id=B.ssp_id
    AND A.pub_id=B.pub_id
    AND A.site_id=B.site_id
    AND A.dsp_id=B.dsp_id