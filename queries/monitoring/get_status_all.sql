SELECT uuid,
    job_name,
    initiator,
    ticket,
    state,
    queued,
    started,
    ended,
    stack,
    params
FROM etl.status
WHERE started >= DATE_SUB(UTC_TIMESTAMP(), INTERVAL 1 DAY)
GROUP BY uuid,
    job_name,
    initiator,
    ticket,
    state,
    queued,
    started,
    ended,
    stack,
    params
