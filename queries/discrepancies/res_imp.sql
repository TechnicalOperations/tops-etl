WITH res AS (
  SELECT event_date AS date,
    ssp_id,
    pub_id,
    IFF(TRIM(site_id) = '-', app_id, site_id) AS plac_id,
    device,
    media_type,
    imp_type,
    SUM(bids) AS responses
  FROM r1_edw.rx.bidresponse_d_est
  WHERE event_date >= '{start_date}'
    AND event_date < '{end_date}'
    AND ssp_id LIKE '%rmp%'
    AND TRIM(filter_reason) = '-'
  GROUP BY 1, 2, 3, 4, 5, 6, 7
),
imp AS (
  SELECT event_date AS date,
    ssp_id,
    pub_id,
    IFF(TRIM(site_id) = '-', app_id, site_id) AS plac_id,
    device,
    media_type,
    imp_type,
    SUM(impression_win) AS wins,
    SUM(impression_pixel) AS pixels
  FROM r1_edw.rx.impression_d_est
  WHERE event_date >= '{start_date}'
    AND event_date < '{end_date}'
    AND ssp_id LIKE '%rmp%'
  GROUP BY 1, 2, 3, 4, 5, 6, 7
)
SELECT A.date,
    A.ssp_id,
    A.pub_id,
    A.plac_id,
    A.device,
    A.media_type,
    A.imp_type,
    A.responses::decimal AS responses,
    IFF(B.wins IS NULL, 0, B.wins)::decimal AS wins,
    IFF(B.pixels IS NULL, 0, B.pixels)::decimal AS pixels
FROM res AS A
LEFT JOIN imp AS B ON A.date=B.date
    AND A.pub_id=B.pub_id
    AND A.plac_id=B.plac_id
    AND A.device=B.device
    AND A.media_type=B.media_type
    AND A.imp_type=B.imp_type