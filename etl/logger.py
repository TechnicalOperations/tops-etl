import logging


ALL_LOGS = {}


def get_logger(name):
    """
    Returns the logger based on name
    :param name:
    :return:
    """
    global ALL_LOGS
    if name in ALL_LOGS:
        return ALL_LOGS[name]
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)
    console_formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s', '%Y-%m-%d %H:%M:%S')
    console_handler.setFormatter(console_formatter)
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(console_handler)
    ALL_LOGS[name] = logger
    return logger
