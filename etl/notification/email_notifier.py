import os
from report_mailer import mailer as email


AUTHENTICATION = os.path.expanduser('~') + '/.auth/.em'


def get_mailer_object(template, subs=None):
    """
    Convenience function for creating a mailer object
    :param template: Relative path to HTML template.  Starting point is tops-etl/email
    :param subs: Optional parameter passed to Mailer.  See Mailer constructor for more details
    :return:
    """
    if os.name == 'nt':
        path = os.path.dirname(os.path.realpath(__file__)).split('\\')
        path = '\\'.join(path[:len(path)-2]) + '\\email\\{0}.html'.format(template.replace('/', '\\'))
    else:
        path = os.path.dirname(os.path.realpath(__file__)).split('/')
        path = '/'.join(path[:len(path)-2])+'/email/{0}.html'.format(template)
    mailer = email.Mailer(AUTHENTICATION, path, subs={} if subs is None else subs)
    return mailer
