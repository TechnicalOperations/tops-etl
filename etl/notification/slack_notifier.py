import os
from slackclient import SlackClient


CHANNEL = 'G9F8BMMJ4'


def send_message(message, file_upload=None, file_name=None):
    """
    Message to be sent to tops-alerts slack channel
    :param message: Message string
    :param file_upload: File content to be uploaded
    :param file_name: Name of the file is file_upload is present
    :return: Return results of slack post
    """
    token = os.environ['SLACK_BOT_TOKEN']
    client = SlackClient(token)
    if file_upload is None or file_name is None:
        res = client.api_call(
            'chat.postMessage',
            channel=CHANNEL,
            text=message
        )
        return res
    res = client.api_call(
        'files.upload',
        channels=CHANNEL,
        initial_comment=message,
        content=file_upload,
        filename=file_name
    )
    return res


if __name__ == '__main__':
    send_message('Test')
