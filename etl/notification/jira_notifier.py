import os
import json
from etl.util import io
from io import BytesIO
from jira import JIRA


def auth():
    """
    Returns formatted authentication parameters for Jira
    :return:
    """
    with open(os.path.expanduser('~')+'/.auth/.jira', 'r') as f:
        auth_items = json.loads(f.read())
    return auth_items['username'], auth_items['password']


def create_ticket(
        summary,
        description,
        *excel_name,
        **attachments
):
    """
    Creates a Jira ticket
    :param summary: Title of the ticket
    :param description: More descriptive text of issue
    :param excel_name: Excel sheet to attach
    :param attachments: Dataframes to attach to the ticket
    :return: Absolutely None
    """
    jira = JIRA(
        server='https://rhythmone.atlassian.net',
        basic_auth=auth()
    )

    new_issue = jira.create_issue(
        project='TOPS',
        summary=summary,
        description=description,
        issuetype={'name': 'Task'}
    )

    if len(excel_name) == 0:
        jira.close()
        return

    data = BytesIO()
    io.to_excel(data, **attachments)
    jira.add_attachment(issue=new_issue, attachment=data, filename=excel_name[0])
    jira.close()
