import os
import gzip
import redis
import pandas
import pickle
from io import BytesIO
from datetime import datetime


def convert_datetime_string(datetime_str):
    """
    Convert string to datetime object.  Must be in the format '%Y-%m-%d %H:%M:%S'.
    Datetime string can be truncated (e.g. '2018-01-01' would still be valid)
    :param datetime_str:
    :return:
    """
    if '/' in datetime_str:
        formatter = '%m/%d/%Y'
    else:
        formatter = '%Y-%m-%d %H:%M:%S'
    idx = len(formatter)
    while idx > 0:
        try:
            return datetime.strptime(datetime_str, formatter[:idx])
        except ValueError as err:
            idx -= 1
    raise ValueError('Date value makes absolutely no sense when formatting it with {0}'.format('%Y-%m-%d %H:%M:%S'))


def get_query(query_name, path=None):
    """
    Retrieve query based on file name
    :param query_name: Relative path to query file
    :return: Full query
    """
    if path is None:
        if os.name == 'nt':
            path = os.path.dirname(os.path.realpath(__file__)).split('\\')
            path = '\\'.join(path[:len(path)-2]) + '\\queries\\{0}.sql'.format(query_name)
        else:
            path = os.path.dirname(os.path.realpath(__file__)).split('/')
            path = '/'.join(path[:len(path)-2])+'/queries/{0}.sql'.format(query_name)
    else:
        path = path + '{0}.sql'.format(query_name)
    with open(path, 'r') as f:
        return f.read()


def to_redis(expire, **kwargs):
    """
    Store data on redis.  Data is always stored in bytes and gzip compressed.
    pickle.dumps is used if data type is not bytes
    :param expire: Lifespan of key/value pair in seconds
    :param kwargs: Key/value pair for redis key/value pair
    :return:
    """
    r = redis.Redis(
        host='127.0.0.1',
        port=6379
    )
    for name, data in kwargs.items():
        try:
            if type(data) != bytes:
                data = gzip.compress(pickle.dumps(data))
            else:
                data = gzip.compress(data)
            name = name.split('/')
            if len(name) == 1:
                r.set(name[0], data)
            else:
                r.hset(name[0], name[1], data)
            r.expire(name[0], expire)
        except redis.exceptions.ConnectionError as err:
            print('A connection error has occurred.  Dumping data to disc')
            name = '_'.join(name)
            with open('{0}.gz'.format(name), 'wb') as f:
                f.write(data)


def to_redis_set(key, value):
    if type(value) != str:
        raise ValueError('Redis set values must be strings')
    r = redis.Redis(
        host='127.0.0.1',
        port=6379
    )
    r.sadd(key, value)
    r.expire(key, 3600)


def in_redis_set(key, value):
    if type(value) != str:
        raise ValueError('Redis set values must be strings')
    r = redis.Redis(
        host='127.0.0.1',
        port=6379
    )
    return r.sismember(key, value)


def delete_from_redis_set(key, value):
    if type(value) != str:
        raise ValueError('Redis set values must be strings')
    r = redis.Redis(
        host='127.0.0.1',
        port=6379
    )
    return r.srem(key, value)


def to_redis_queue(key, value):
    r = redis.Redis(
        host='127.0.0.1',
        port=6379
    )
    r.rpush(key, gzip.compress(pickle.dumps(value)))
    r.expire(key, 3600)


def pop_redis_queue(key):
    r = redis.Redis(
        host='127.0.0.1',
        port=6379
    )
    result = r.rpop(key)
    if result is None:
        return None
    return pickle.loads(gzip.decompress(result))


def get_redis(key):
    """
    Retrieve data from redis based on key
    :param key:
    :return:
    """
    r = redis.Redis(
        host='127.0.0.1',
        port=6379
    )
    key = key.split('/')
    if len(key) == 1:
        result = r.get(key[0])
    else:
        result = r.hget(key[0], key[1])
    try:
        result = result if result is None else gzip.decompress(result)
    except OSError:
        pass
    try:
        result = result if result is None else pickle.loads(result)
    except OSError:
        pass
    return result


def delete_redis(key):
    """
    Delete data from redis based on key
    :param key:
    :return:
    """
    r = redis.Redis(
        host='127.0.0.1',
        port=6379
    )
    key = key.split('/')
    if len(key) == 1:
        r.delete(key[0])
    else:
        r.hdel(key[0], key[1])


def get_all_redis_hash(hkey):
    """
    Returns all values in a hash as a dictionary
    :param key: Key for hash
    :return: Dictionary key/value pairing of everything in the hash
    """
    r = redis.Redis(
        host='127.0.0.1',
        port=6379
    )
    keys = r.hkeys(hkey)
    items = {}
    for key in keys:
        key = key.decode('utf-8')
        items[key] = get_redis('{0}/{1}'.format(hkey, key))
    return items


def to_excel(file_name, **kwargs):
    """
    Write results to Excel file
    :param file_name: File name (should include full path)
    :param kwargs: Key/value pairs translate to sheet name/pandas.DataFrame
    :return:
    """
    if len(kwargs) == 1 and type(list(kwargs.values())[0]) == bytes:
        data = list(kwargs.values())[0]
        if type(file_name) == str:
            with open(file_name, 'wb') as f:
                f.write(data)
        else:
            file_name.write(data)
        return
    if type(file_name) == str:
        writer = pandas.ExcelWriter(file_name.replace('~', os.path.expanduser('~')), engine='xlsxwriter')
    else:
        writer = pandas.ExcelWriter(file_name, engine='xlsxwriter')
    for name, data in kwargs.items():
        data.to_excel(writer, sheet_name=name, index=False)
    writer.save()
    writer.close()


def to_database(conn, db_name, **kwargs):
    """
    Inserts data into given database
    :param conn: Connection object to database (DbConnection.con if using DbConnection)
    :param db_name: Database/schema to insert into
    :param kwargs: Key/value pairs that translate to table/pandas.DataFrame
    :return:
    """
    insert = "INSERT INTO {table} ({columns}) VALUES ({values})"
    cursor = conn.cursor()
    for name, data in kwargs.items():
        if 'mysql' in str(conn.__class__).lower():
            conn.select_db(db_name)
            table = '{0}'.format(name)
        else:
            table = '{0}.{1}'.format(db_name, name)
        columns = list(data.columns)
        values = ['%s'] if 'mysql' in str(conn.__class__).lower() else ['?']
        values = ', '.join(values * len(data.columns))
        data = data.to_dict(orient='records')
        data = [
            tuple([i[column] for column in columns])
            for i in data
        ]
        columns = ', '.join(columns)
        cursor.executemany(
            insert.format(
                table=table,
                columns=columns,
                values=values
            ),
            data
        )
        conn.commit()
    cursor.close()
