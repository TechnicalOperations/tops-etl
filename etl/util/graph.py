from uuid import uuid4


class Node:

    def __init__(self, value, *key):
        self.value = value
        if len(key) > 0:
            self.uuid = key if len(key) > 1 else key[0]
        else:
            self.uuid = uuid4()
        self.connections = {}

    def connect_node(self, node):
        if not isinstance(node, Node):
            raise TypeError('Expected type Node')
        self.connections[node.uuid] = node

    def __hash__(self):
        return hash(self.uuid)


class Graph:

    def __init__(self):
        self.nodes = {}
        self.node_types = None

    def add_node(self, node, *connect_to, connect_all=False, connect_self=False):
        if self.node_types is None:
            self.node_types = type(node.value)
        if not isinstance(node.value, self.node_types):
            raise TypeError('Inconsistent value types on nodes')
        if connect_all:
            for uuid, graph_node in self.nodes.items():
                if not connect_self and uuid == node.uuid:
                    continue
                graph_node.connect_node(node)
        self.nodes[node.uuid] = node

    def min_to_zero(self):
        return self
