import pandas
import numpy as np
from datetime import datetime
from sklearn.neural_network import MLPRegressor
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split


def calc_stats(x, cols):
    out = pandas.Series(index=['avg', 'std', 'min', 'max', cols[-1]])
    out[cols[-1]] = x[cols[-1]].sum()
    out['avg'] = (x[cols[-2]] * x[cols[-1]]).sum() / out[cols[-1]]
    std = (x[cols[-2]] - out['avg']) ** 2
    out['std'] = np.sqrt(std.sum() / out[cols[-1]])
    out['min'] = np.min(x[cols[-2]])
    out['max'] = np.max(x[cols[-2]])
    return out


def weighted_stats(data, index, metric, count):
    if count is None:
        count = 'count'
        data[count] = 1
    if type(index) == list:
        cols = index + [metric, count]
    else:
        cols = [index, metric, count]
    data = data.copy()
    data = data[cols].groupby(index+[metric] if type(index) == list else [index, metric]).sum()
    l = len(index+[metric]) if type(index) == list else 2
    l = l - 1
    data = data.reset_index(level=l)
    data = data.groupby(level=[i for i in range(l)]).apply(lambda x: calc_stats(x, cols))
    data[count] = data[count].apply(int)
    return data


def _make_linear_regression(copy, x_values, y_values, scaler=None):
    if scaler is not None:
        scaler.fit(x_values, y_values)
        x_values = scaler.transform(x_values)
    copy['regression'] = 0.0
    regression = LinearRegression()
    regression.fit(x_values, y_values)
    copy['regression'] = regression.predict(x_values)[:, 0]
    copy['R2'] = regression.score(x_values, y_values)
    return copy, regression,


def _make_data_copy(data, x, y):
    copy = data.copy()
    if x not in copy.columns and (copy.index.name == x or x in copy.index.names):
        copy = copy.reset_index()
    if type(copy.iloc[0][x]) in [pandas.Timestamp, datetime]:
        copy[x] = copy[x].apply(lambda i: i.timestamp())
    else:
        try:
            copy[x] = copy[x] / 1
        except TypeError as err:
            raise TypeError('Data type {0} not supported in linear regression'.format(type(data.iloc[0][x])))
    if type(copy.iloc[0][y]) in [pandas.Timestamp, datetime]:
        copy[y] = copy[y].apply(lambda i: i.timestamp())
    else:
        try:
            copy[y] = copy[y] / 1
        except TypeError as err:
            raise TypeError('Data type {0} not supported in linear regression'.format(type(data.iloc[0][x])))
    return copy


def _make_array_x_y(copy, x, y):
    if type(x) == list:
        x_values = copy[x].values
    else:
        x_index = list(copy.columns).index(x)
        x_values = copy.values[:, x_index:x_index+1]
    if type(y) == list:
        y_values = copy[y].values()
    else:
        y_index = list(copy.columns).index(y)
        y_values = copy.values[:, y_index:y_index+1]
    return x_values, y_values,


def linear_regression(data, x, y):
    copy = _make_data_copy(data, x, y)
    x_values, y_values, = _make_array_x_y(copy, x, y)
    copy, regression, = _make_linear_regression(copy, x_values, y_values)
    data['regression'] = copy['regression'].values
    data['R2'] = copy['R2'].values
    return data


def slope_angle(data, x, y, standard_scaler=True):
    copy = _make_data_copy(data, x, y)
    x_values, y_values, = _make_array_x_y(copy, x, y)
    scaler = StandardScaler() if standard_scaler else None
    copy, regression, = _make_linear_regression(copy, x_values, y_values, scaler=scaler)
    slope = regression.coef_[0][0]
    slope = np.arctan(slope) * 180 / np.pi
    data['angle'] = slope
    return data


def classifier_model(data, x, y):
    scaler = StandardScaler()
    x_values, y_values = _make_array_x_y(data, x, y)
    scaler.fit(x_values, y_values)
    x_train, x_test, y_train, y_test = train_test_split(x_values, y_values)
    classifier = MLPClassifier()
    classifier.fit(x_train, y_train)
    return classifier
