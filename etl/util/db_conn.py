import os
import atexit
import pandas
import MySQLdb
from etl.util import io
from time import sleep
from datetime import datetime
from datetime import timedelta
from snowflake import connector


AUTH_FILES = {
    'rx_mysql': '~/.auth/.my.conf-rx'.replace('~', os.path.expanduser('~')),
    'tops_mysql': '~/.auth/.my.conf'.replace('~', os.path.expanduser('~')),
    'snowflake': '~/.snowsql/config'.replace('~', os.path.expanduser('~'))
}

SF_PARAM_MAP = {
    'accountname': 'account',
    'username': 'user',
    'password': 'password',
    'dbname': 'database',
    'rolename': 'role',
}


LIVE_CONNECTIONS = {}


if os.name == 'nt':
    AUTH_FILES = {k: v.replace('/', '\\') for k, v in AUTH_FILES.items()}


def connect(database):
    """
    Returns an SQL connection object based on database string
    :param database: Database to connect to based on AUTH_FILES keys
    :return:
    """
    global LIVE_CONNECTIONS
    if database in LIVE_CONNECTIONS:
        LIVE_CONNECTIONS[database]['count'] += 1
        return LIVE_CONNECTIONS[database]['con']
    auth_file = AUTH_FILES[database]
    if database == 'snowflake':
        with open(auth_file, 'r') as f:
            params = f.read()
        params = [i.split('=') for i in params.split('\n')]
        params = {SF_PARAM_MAP[i[0]]: i[1] for i in params if i[0] in SF_PARAM_MAP}
        con = connector.connect(**params)
    elif 'mysql' in database:
        con = MySQLdb.connect(read_default_file=auth_file)
    else:
        raise ValueError('Database option \'{0}\' does not exist'.format(database))
    LIVE_CONNECTIONS[database] = {
        'con': con,
        'count': 1
    }
    return con


def close_connections(*databases):
    """
    Handle to close all live connections on script termination
    :return:
    """
    global LIVE_CONNECTIONS
    if len(databases) > 0:
        for name in databases:
            if LIVE_CONNECTIONS[name]['count'] == 1:
                LIVE_CONNECTIONS[name]['con'].close()
                del LIVE_CONNECTIONS[name]
            else:
                LIVE_CONNECTIONS[name]['count'] -= 1
        return
    for name, params in LIVE_CONNECTIONS.items():
        try:
            params['con'].close()
        except Exception as err:
            pass
    LIVE_CONNECTIONS = {}


atexit.register(close_connections)


class DbConnection:
    """
    DB connection container class.
    This class is mostly for convenience on select statements
    """

    INSERT_STR = """
    INSERT INTO {table}
    ({columns})
    VALUES
    ({values})
    """

    def __init__(self, database):
        self.database = database

    def insert_frame(self, db_name, **data):
        if self.database == 'snowflake':
            raise ValueError('Inserting into snowflake not permitted')
        conn = connect(self.database)
        io.to_database(conn, db_name, **data)
        close_connections(self.database)

    def get_frame(
            self,
            query,
            redis=True,
            expire=129600,
            force_query=False,
            **query_format
    ):
        """
        Returns a pandas DataFrame based on the query executed
        :param query: Relative path to query file (exclude file extension '.sql') (If file doesn't exist,
            assume query itself is passed)
        :param redis: Boolean on whether to store results in redis (query will be used as the key)
        :param expire: Seconds on how long redis key/value pairing should exist
        :param force_query: Boolean on whether to force query to execute even if redis key/value exists
        :param query_format: Additional key/value pairs for string formatting of actual query (optional)
        :return: Pandas DataFrame
        """
        redis_key = query
        block_key = None
        try:
            query = io.get_query(query).format(**query_format)
            data = io.get_redis(redis_key) if redis else None
            if self.database == 'snowflake' and data is None:
                block_key = redis_key.replace('/', '_')
                block_key = 'sf_block/{0}'.format(block_key)
                block = io.get_redis(block_key)
                if block is None:
                    block = False
                    io.to_redis(3600, **{block_key: block})
                while block:
                    sleep(1)
                    block = io.get_redis('sf_block/{0}'.format(block_key))
                data = io.get_redis(redis_key) if redis else None
        except (FileNotFoundError, OSError):
            redis = False
            data = None
        if data is None or force_query:
            if self.database == 'snowflake' and redis:
                io.to_redis(3600, **{block_key: True})
            try:
                conn = connect(self.database)
                data = pandas.read_sql_query(query, conn)
            except (Exception, KeyboardInterrupt) as err:
                close_connections(self.database)
                if self.database == 'snowflake' and redis:
                    io.to_redis(3600, **{block_key: False})
                raise err
            close_connections(self.database)
            if self.database == 'snowflake' and redis:
                io.to_redis(3600, **{block_key: False})
            data = data.rename(columns={i: i.lower() for i in data.columns})
            if redis:
                io.to_redis(expire, **{redis_key: data})
        return data

    def get_frame_date_append(
            self,
            query,
            date_col,
            start,
            end,
            expire=129600,
            inc=timedelta(days=1),
            retention=timedelta(days=31),
            **query_format
    ):
        try:
            io.get_query(query).format(**query_format)
        except (FileNotFoundError, OSError):
            raise ValueError('Query must be key (sql file name) for redis cache to use this function')
        if type(start) == str:
            start = io.convert_datetime_string(start)
        if type(end) == str:
            end = io.convert_datetime_string(end)
        data = self.get_frame(
            query,
            **query_format
        )
        data[date_col] = data[date_col].apply(
            lambda x: datetime(year=x.year, month=x.month, day=x.day, hour=getattr(x, 'hour', 0))
        )
        if data[date_col].min() <= start and data[date_col].max() >= end - inc:
            return data[(data[date_col] >= start) & (data[date_col] < end)].reset_index(drop=True)
        cache = data
        data = self.get_frame(
            query,
            force_query=True,
            redis=False,
            **query_format
        )
        data[date_col] = data[date_col].apply(
            lambda x: datetime(year=x.year, month=x.month, day=x.day, hour=getattr(x, 'hour', 0))
        )
        data = data[~data[date_col].isin(set(cache[date_col]))].reset_index(drop=True)
        data = data.append(cache).reset_index(drop=True)
        latest = data[date_col].max()
        data = data[(data[date_col] >= latest - retention)].reset_index(drop=True)
        io.to_redis(expire, **{query: data})
        return data[(data[date_col] >= start) & (data[date_col] < end)].reset_index(drop=True)
