import json
import flask
from flask import request
from etl.util import io
from etl.util import db_conn
from etl import monitoring
from etl.http_nodes import main_app
from etl.job_processor import run_job
from datetime import datetime
from datetime import timedelta
from jobs.main import get_all_scripts_names
from jobs.main import get_all_job_descriptions


all_scripts = get_all_scripts_names()
# The below app is a carry over from main_app.  This is done this way to provide the ability to use multiple
#  modules for route definitions
ui_app = main_app.ui_app


@ui_app.route('/submit/', methods=['POST'])
def submit_job():
    """
    Initiates a job/report.  The post request data is a JSON with the following format:
    {'job_name': <job_settings_script_name>, 'parameters': {<job_specific_parameters>}}
    :return:
    """
    data = flask.request.get_json()
    # The line below is how to set the job to run while simultaneously returning a response back to the client
    validator = all_scripts[data['job_name']].param_validator(**data['parameters'])
    if not validator[0]:
        return flask.jsonify(validator[1])
    status = monitoring.Status(data['job_name'], request.remote_addr, **validator[1])
    run_job.delay(status, **validator[1])
    return flask.jsonify({
        'status': 'success',
        'message': 'Successfully submitted request for job {0}'.format(data['job_name'])
    })


@ui_app.route('/reports/<regex("all|data_tools|crawlers"):tools>/', methods=['POST'])
def get_job_descriptions(tools):
    """
    Returns field descriptions for each job.  See jobs/settings.py under FIELD_DESCRIPTORS for more details
    :return:
    """
    descriptions = get_all_job_descriptions()
    descriptions = [
        i for i in descriptions
        if i['is_public'] and (tools == 'all' or tools == i['type'])
    ]
    for item in descriptions:
        item['fields'] = [
            {
                'name': 'ticket',
                'type': 'str',
                'descriptor': 'Ticket ID to associate this script with',
                'required': False
            }
        ] + item['fields']
    return flask.jsonify(descriptions)


@ui_app.route('/monitoring/<regex("day|week|month"):time_range>/', methods=['POST'])
def get_statuses(time_range):
    query = io.get_query('monitoring/get_status_overview')
    if time_range == 'day':
        days = 1
    elif time_range == 'week':
        days = 7
    elif time_range == 'month':
        days = 31
    else:
        days = 1
    query = query.format(days=days)
    conn = db_conn.DbConnection('tops_mysql')
    status = conn.get_frame(query, redis=False)
    status = status.to_dict(orient='records')
    status += [
        dict(v) for k, v in monitoring.Status.get_all_current().items()
        if datetime.utcnow() - v._started <= timedelta(days=days)
    ]
    status += [
        dict(v) for k, v in monitoring.Status.get_all_complete().items()
        if datetime.utcnow() - v._started <= timedelta(days=days)
    ]
    descriptions = get_all_job_descriptions()
    descriptions = {i['name']: i['type'] for i in descriptions}
    for s in status:
        s['job_type'] = descriptions.get(s['job_name'], None)
        s['params'] = json.loads(s['params'])
        if 'notifier' in s['params']:
            if isinstance(s['params']['notifier'], dict):
                s['params']['to'] = s['params']['notifier'].get('to', None)
                s['params']['cc'] = s['params']['notifier'].get('cc', [])
            else:
                s['params']['to'] = None
                s['params']['cc'] = []
            del s['params']['notifier']
        s['params'] = json.dumps(s['params'])
    return flask.jsonify(status)


@ui_app.route('/monitoring/all/', methods=['POST'])
def get_all_statuses():
    query = io.get_query('monitoring/get_status_all')
    conn = db_conn.DbConnection('tops_mysql')
    status = conn.get_frame(query, redis=False)
    def try_convert(x):
        try:
            return x.strftime('%Y-%m-%d %H:%M:%S')
        except:
            return None
    for date in ['queued', 'started', 'ended']:
        status[date] = status[date].apply(try_convert)
    status = status.to_dict(orient='records')
    status += [dict(v) for k, v in monitoring.Status.get_all_current().items()]
    status += [dict(v) for k, v in monitoring.Status.get_all_complete().items()]
    descriptions = get_all_job_descriptions()
    descriptions = {i['name']: i['type'] for i in descriptions}
    for s in status:
        s['job_type'] = descriptions.get(s['job_name'], None)
        s['params'] = json.loads(s['params'])
        if 'notifier' in s['params']:
            if isinstance(s['params']['notifier'], dict):
                s['params']['to'] = s['params']['notifier'].get('to', None)
                s['params']['cc'] = s['params']['notifier'].get('cc', [])
            else:
                s['params']['to'] = None
                s['params']['cc'] = []
            del s['params']['notifier']
        s['params'] = json.dumps(s['params'])
    return flask.jsonify(status)
