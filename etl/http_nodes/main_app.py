import flask
from flask import Response
from werkzeug.routing import BaseConverter
from etl.http_nodes.file_loader import file_loader


ui_app = flask.Flask('job_ui')


class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]


ui_app.url_map.converters['regex'] = RegexConverter


@ui_app.route('/<regex("img|html|css|js"):f_type>/<space>/<file_name>', methods=['GET'])
def get_static(f_type, space, file_name):
    """
    Flask app route for retrieving files from the file system (css/html/js/img)
    The arguments are the same ones used in file_loader
    :param f_type: File type (css/html/js/img)
    :param space: Subdirectory to look into based on f_type (css/<space>)
    :param file_name: The name of the file to be loaded, including the extension (css/<space>/<name>.css)
    :return: Response to request
    """
    try:
        content, mimetype, = file_loader(f_type, space, file_name)
        return Response(content, mimetype=mimetype)
    except FileNotFoundError:
        flask.abort(404)


@ui_app.route('/<regex("[a-zA-Z\-_]*\.?h?t?m?l?"):file_name>', methods=['GET'])
def submit_form(file_name):
    """
    Convenience route for get_static with route 'html/submit/index.hmtl'
    :return: Response to request
    """
    file_name = 'index.html' if file_name is None or file_name == '' else file_name
    content, mimetype, = file_loader('html', 'index', file_name)
    return Response(content, mimetype=mimetype)
