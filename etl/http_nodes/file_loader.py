import os


LOADED_FILES = {}


def file_loader(f_type, space, file_name):
    """
    Loads a static file into memory and returns the file content and mimetype of the file
    :param f_type: File type (css/html/js/img)
    :param space: Subdirectory to look into based on f_type (css/<space>)
    :param file_name: The name of the file to be loaded, including the extension (css/<space>/<name>.css)
    :return:
    """
    key = (f_type, space, file_name,)
    if key in LOADED_FILES:
        return LOADED_FILES[key]['content']
    if os.name == 'nt':
        path = os.path.dirname(os.path.realpath(__file__)).split('\\')
        path = '\\'.join(path[:len(path) - 2]) + '\\http\\{0}\\{1}\\{2}'.format(f_type, space, file_name)
    else:
        path = os.path.dirname(os.path.realpath(__file__)).split('/')
        path = '/'.join(path[:len(path) - 2]) + '/http/{0}/{1}/{2}'.format(f_type, space, file_name)
    with open(path, 'rb') as f:
        content = f.read()
    content = content.decode('utf-8') if f_type in ['html', 'css', 'js'] else content
    if f_type == 'html':
        mimetype = 'text/html'
    elif f_type == 'css':
        mimetype = 'text/css'
    elif f_type == 'js':
        mimetype = 'application/javascript'
    else:
        mimetype = 'image/{0}'.format(file_name.split('.')[1])
    return content, mimetype,


if __name__ == '__main__':
    print(file_loader('html', 'test', 'test.html'))
