from etl.http_nodes import main_app
from etl.http_nodes import post_requests


# It's necessary to import ALL route function definitions here for the route to be applied to the app
# A simple import as shown above is all that's necessary

complete_app = main_app.ui_app

if __name__ == '__main__':
    import socket
    main_app.ui_app.run(
        host=socket.gethostbyname(socket.gethostname()),
        port=8080
    )
