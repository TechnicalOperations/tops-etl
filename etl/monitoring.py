import enum
import json
import uuid
import pandas
from etl.util import io
from copy import deepcopy
from datetime import datetime


CACHE_TIME = 31 * 24 * 60 * 60  # 31 days


class State(enum.Enum):
    QUEUE = 'queue'
    PROCESSING = 'processing'
    ERROR = 'error'
    COMPLETED = 'completed'
    HUNG = 'hung/terminated'


class Status:

    def __init__(self, job_name, initiator, **params):
        self._job_name = job_name
        self._initiator = initiator
        self._uuid = str(uuid.uuid1())
        self._state = State.QUEUE
        self._queued = datetime.utcnow()
        self._started = None
        self._ended = None
        self._stack_trace = None
        self._ticket = None
        self._params = deepcopy(params)
        self._update_redis()
    
    @property
    def job_name(self):
        return self._job_name

    @property
    def initiator(self):
        return self._initiator

    def _update_redis(self):
        if self._state == State.QUEUE or self._state == State.PROCESSING:
            io.to_redis(
                CACHE_TIME,
                **{'current/{0}'.format(self._uuid): self}
            )
        else:
            io.delete_redis('current/{0}'.format(self._uuid))
            io.to_redis(
                CACHE_TIME,
                **{'complete/{0}'.format(self._uuid): self}
            )

    @staticmethod
    def get_all_current():
        return io.get_all_redis_hash('current')

    @staticmethod
    def get_all_complete():
        return io.get_all_redis_hash('complete')

    @staticmethod
    def flush_current():
        io.delete_redis('current')

    @staticmethod
    def flush_complete():
        io.delete_redis('complete')

    @staticmethod
    def all_complete_to_frame():
        complete = Status.get_all_complete()
        all_items = [dict(i) for i in complete]
        all_items = pandas.DataFrame(all_items)
        return all_items

    def start(self):
        self._started = datetime.utcnow()
        self._state = State.PROCESSING
        self._update_redis()

    def fail(self, trace):
        self._state = State.ERROR
        self._stack_trace = trace
        self._ended = datetime.utcnow()
        self._update_redis()

    def success(self):
        self._ended = datetime.utcnow()
        self._state = State.COMPLETED
        self._update_redis()

    def __iter__(self):
        params = {
            k: v.strftime('%Y-%m-%d %H:%M:%S') if type(v) == datetime else v
            for k, v in self._params.items()
        }
        values = {
            'job_name': self._job_name,
            'initiator': self._initiator,
            'uuid': self._uuid,
            'ticket': self._ticket,
            'state': self._state.value,
            'queued': None if self._queued is None else datetime.strftime(self._queued, '%Y-%m-%d %H:%M:%S'),
            'started': None if self._started is None else datetime.strftime(self._started, '%Y-%m-%d %H:%M:%S'),
            'ended': None if self._ended is None else datetime.strftime(self._ended, '%Y-%m-%d %H:%M:%S'),
            'stack': self._stack_trace,
            'params': json.dumps(params)
        }
        for k, v in values.items():
            yield k, v,
