import os
import multiprocessing
from celery import Celery
from jobs.main import get_all_scripts_names


app = Celery('job_processor', broker='redis://127.0.0.1')
app.config_from_object('etl.celeryconfig')
all_scripts = get_all_scripts_names()


if os.name == 'nt':  # This was created to accommodate the lack of Celery in Windows
    class run_job:

        @staticmethod
        def delay(status, **kwargs):
            job_main = all_scripts[status.job_name]
            process = multiprocessing.Process(
                target=job_main.run,
                args=(status,),
                kwargs=kwargs
            )
            process.start()
else:
    @app.task
    def run_job(status, **kwargs):
        """
        Run the job_name specified.  Appropriate notification types should be specified in the job itself, called within run.
        :param status: Instance of the class etl.monitoring.Status
        :param kwargs: Additional kwargs to be passed to job run(**kwargs) function
        :return: Absolutely nothing
        """
        job_main = all_scripts[status.job_name]
        try:
            job_main.run(status, **kwargs)
        except Exception as err:
            pass
